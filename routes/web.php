<?php

// guest routes
Auth::routes(['verify' => true]);

Route::get('/register/', ['uses' => '\App\Http\Controllers\Auth\RegisterController@showRegistrationForm']);
Route::get('/register/{role}', ['uses' => '\App\Http\Controllers\Auth\RegisterController@index']);
Route::post('/register/{role}', ['uses' => '\App\Http\Controllers\Auth\RegisterController@register']);

Route::group(['namespace' => 'App'], function () {
    Route::get('/', ['uses' => 'HomeController@index', 'as' => 'app.home.index']);
    Route::get('/contact-us', ['uses' => 'HomeController@contact', 'as' => 'app.home.contact']);
    Route::get('/about', ['uses' => 'HomeController@about', 'as' => 'app.home.about']);
    Route::get('/terms', ['uses' => 'HomeController@term', 'as' => 'app.home.term']);
    Route::get('/privacy', ['uses' => 'HomeController@privacy', 'as' => 'app.home.privacy']);
});

Route::group(['middleware' => ['role:admin'], 'namespace' => 'Admin', 'prefix' => 'admin'], function () {
    Route::get('/', ['uses' => 'DashboardController@index', 'as' => 'admin.dashboard.index']);
    Route::post('/user/list', ['uses' => 'UserController@list', 'as' => 'admin']);
    Route::resource('/user', 'UserController', ['as' => 'admin']);
    Route::get('/user/{user}/orders', ['uses' => 'UserController@orders', 'as' => 'admin.user.order']);
    Route::get('/user/{user}/balance', ['uses' => 'UserController@balances', 'as' => 'admin.user.balance']);
    Route::get('/user/{user}/shipment', ['uses' => 'UserController@shipments', 'as' => 'admin.user.shipment']);
    Route::get('/user/{user}/address', ['uses' => 'UserController@addresses', 'as' => 'admin.user.address']);

    Route::resource('/order', 'OrderController', ['as' => 'admin']);
    Route::post('/order/getOrdersByUser', ['uses' => 'OrderController@getOrdersByUser', 'as' => 'admin.order.user']);
    Route::resource('/shipment', 'ShipmentController', ['as' => 'admin']);
    Route::resource('/shipment.status', 'ShipmentStatusController', ['as' => 'admin']);
    Route::resource('/shipment.service', 'ShipmentServiceController', ['as' => 'admin']);
    Route::resource('/shipment-schedule', 'ShipmentScheduleController', ['as' => 'admin']);
    Route::resource('/shipment-provider', 'ShipmentProviderController', ['as' => 'admin']);

    Route::resource('/bank', 'BankController', ['as' => 'admin']);
    Route::post('/bank/getOrdersByUser', ['uses' => 'BankController@getBanksByUser', 'as' => 'admin.bank.user']);

    Route::resource('/bank-type', 'BankTypeController', ['as' => 'admin']);
    Route::resource('/coupon', 'CouponController', ['as' => 'admin']);
    Route::resource('/package', 'PackageController', ['as' => 'admin']);
    Route::resource('/report', 'ReportController', ['as' => 'admin']);
    Route::resource('/transfer', 'TransferController', ['as' => 'admin']);
    Route::resource('/address', 'AddressController', ['as' => 'admin']);
    Route::resource('/app-setting', 'AppSettingController', ['as' => 'admin']);
    Route::resource('/slider', 'SliderController', ['as' => 'admin']);
});

Route::group(['namespace' => 'App', 'middleware' => ['role:admin|trader|customer', 'verified', 'packageIsSelected']], function () {
    Route::get('/user/setting/', ['uses' => 'UserController@setting', 'as' => 'app.user.setting']);
    Route::get('/user/transfer-period/', ['uses' => 'UserController@duration', 'as' => 'app.user.duration']);
    Route::post('/user/transfer-period/', ['uses' => 'UserController@post_duration', 'as' => 'app.user.duration']);
    Route::resource('/user', 'UserController', ['as' => 'app']);
    
    Route::resource('/notification', 'NotificationController', ['as' => 'app']);


    Route::get('/shipment/filter/{addressType?}/{shipmentStatus?}', ['uses' => 'ShipmentController@filter', 'as' => 'app.shipment.track']);
    Route::resource('/shipment', 'ShipmentController', ['as' => 'app']);
    Route::get('/shipment-track/', ['uses' => 'ShipmentController@get_track', 'as' => 'app.shipment.track']);
    Route::post('/shipment-track/', ['uses' => 'ShipmentController@post_track', 'as' => 'app.shipment.track']);

    Route::get('/order/confirm-order',  ['uses' => 'OrderController@get_confirm', 'as' => 'app.order.confirm']);
    Route::post('/order/confirm-order',  ['uses' => 'OrderController@post_confirm', 'as' => 'app.order.confirm']);
    Route::resource('/order', 'OrderController', ['as' => 'app']);

    Route::resource('/coupon', 'CouponController', ['as' => 'app']);
    Route::resource('/package', 'PackageController', ['as' => 'app']);
    Route::resource('/report', 'ReportController', ['as' => 'app']);
    Route::resource('/transfer', 'TransferController', ['as' => 'app']);
    Route::resource('/address', 'AddressController', ['as' => 'app']);
    Route::get('/address-list/{address_type}', ['uses' => 'AddressController@list_by_type', 'as' => 'app.address.list']);

    Route::resource('/bank', 'BankController', ['as' => 'app']);
});