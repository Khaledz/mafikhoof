<?php

Route::post('login', 'Api\Auth\AuthController@login');
Route::post('register/member', 'Api\Auth\AuthController@register_member');
Route::post('register/beneficiary', 'Api\Auth\AuthController@register_beneficiary');

// Route::get('/countries', 'Api\ListAllController@countries');
// Route::get('/categories', 'Api\ListAllController@categories');
// Route::get('/genders', 'Api\ListAllController@genders');
// Route::get('/rates', 'Api\ListAllController@rates');
// Route::get('/accents', 'Api\ListAllController@accents');
// Route::get('/social_medias', 'Api\ListAllController@social_medias');
// Route::get('/price_types', 'Api\ListAllController@price_types');
// Route::get('/hair_colors', 'Api\ListAllController@hair_colors');
// Route::get('/eye_colors', 'Api\ListAllController@eye_colors');
// Route::get('/menu', 'Api\ListAllController@menu');
// Route::get('/skin_colors', 'Api\ListAllController@skin_colors');
// Route::get('/payment_methods', 'Api\ListAllController@payment_methods');
// Route::get('/payment_statuses', 'Api\ListAllController@payment_statuses');
// Route::get('/app_setting', 'Api\ListAllController@app_setting');
// Route::get('/roles', 'Api\ListAllController@roles');

Route::group(['middleware' => 'auth:api'], function() {
    Route::get('/user/setting/', ['uses' => 'UserController@setting', 'as' => 'app.user.setting']);
    Route::get('/user/transfer-period/', ['uses' => 'UserController@duration', 'as' => 'app.user.duration']);
    Route::post('/user/transfer-period/', ['uses' => 'UserController@post_duration', 'as' => 'app.user.duration']);
    Route::resource('/user', 'UserController', ['as' => 'app']);
    
    Route::resource('/notification', 'NotificationController', ['as' => 'app']);


    Route::get('/shipment/filter/{addressType?}/{shipmentStatus?}', ['uses' => 'ShipmentController@filter', 'as' => 'app.shipment.track']);
    Route::resource('/shipment', 'ShipmentController', ['as' => 'app']);
    Route::get('/shipment-track/', ['uses' => 'ShipmentController@get_track', 'as' => 'app.shipment.track']);
    Route::post('/shipment-track/', ['uses' => 'ShipmentController@post_track', 'as' => 'app.shipment.track']);

    Route::get('/order/confirm-order',  ['uses' => 'OrderController@get_confirm', 'as' => 'app.order.confirm']);
    Route::post('/order/confirm-order',  ['uses' => 'OrderController@post_confirm', 'as' => 'app.order.confirm']);
    Route::resource('/order', 'OrderController', ['as' => 'app']);

    Route::resource('/coupon', 'CouponController', ['as' => 'app']);
    Route::resource('/package', 'PackageController', ['as' => 'app']);
    Route::resource('/report', 'ReportController', ['as' => 'app']);
    Route::resource('/transfer', 'TransferController', ['as' => 'app']);
    Route::resource('/address', 'AddressController', ['as' => 'app']);
    Route::get('/address-list/{address_type}', ['uses' => 'AddressController@list_by_type', 'as' => 'app.address.list']);

    Route::resource('/bank', 'BankController', ['as' => 'app']);

//     Route::get('logout', 'Api\Auth\AuthController@logout');
//     Route::get('user', 'Api\Auth\AuthController@user');
//     Route::resource('category', 'Api\CategoryController');
//     Route::post('category/requirements', 'Api\CategoryController@getRequirements');
//     Route::post('category/filter', 'Api\CategoryController@filter');
//     Route::get('/getHomeCategories', 'Api\HomeController@index');

//     Route::resource('order', 'Api\OrderController');
//     Route::resource('portofolio', 'Api\PortofolioController');
//     Route::get('/dashboard', 'Api\UserController@dashboard');
//     Route::post('/setting', 'Api\UserController@post_setting');
//     Route::get('/user/payments', 'Api\UserController@payments');
//     Route::get('/user/orders', 'Api\UserController@orders');
//     Route::get('/user/portofolios', 'Api\UserController@portofolios');
//     Route::post('/user/set-category', 'Api\UserController@set_category_post');
//     Route::resource('user', 'Api\UserController');
});