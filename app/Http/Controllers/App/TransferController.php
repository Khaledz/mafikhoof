<?php

namespace App\Http\Controllers\App;

use App\Models\Transfer;
use App\Models\TransferDuration;
use App\Models\User;
use App\Models\Order;
use App\Models\Bank;
use App\Models\Shipment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();

        // get orders that not deliver
        $orderStatusRules = [
            1, 2
        ];
        
        $sumItemsNotDeliver = Shipment::whereHas('receiver', function($query) use ($user){
            return $query->where('user_id', $user->id);
        })->orWhereHas('sender', function($query) use ($user){
            return $query->where('user_id', $user->id);
        })->whereHas('order', function($query) use($orderStatusRules){
            return $query->whereIn('order_status_id', $orderStatusRules);
        })->sum('item_cost');

        $sumLastTwoTransfers = Transfer::where('user_id', auth()->user()->id)->latest()->take(2)->get()->sum('amount');
        // get transfers for yesterday
        $transfersForYesterday = Transfer::whereDate('created_at', '=', \Carbon\Carbon::yesterday()->format('Y-m-d'))
        ->where('user_id', $user->id)->get();

        return view('app.transfer.index', compact('sumItemsNotDeliver', 'sumLastTwoTransfers', 'transfersForYesterday'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // add balance from user to his account
        $banks = Bank::with('type')->get()->pluck('type.name', 'id');

        return view('app.transfer.create', compact('banks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, 
            ['bank_id' => 'required', 'amount' => 'numeric|min:50']
        );
        $transfer = Transfer::create(array_merge($request->all(), ['user_id' => auth()->user()->id, 'activate' => 0, 'transfer_type_id' => 2]));
        
        return redirect(route('app.user.show', auth()->user()))->with('message', 'تم حفظ المعلومات بنجاح!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transfer  $transfer
     * @return \Illuminate\Http\Response
     */
    public function show(Transfer $transfer)
    {
        return view('admin.transfer.show', compact('transfer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transfer  $transfer
     * @return \Illuminate\Http\Response
     */
    public function edit(Transfer $transfer)
    {
        $users = User::pluck('name', 'id');
        $functions = TransferFunction::pluck('name', 'id');

        return view('admin.transfer.edit', compact('users', 'functions','transfer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transfer  $transfer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transfer $transfer)
    {
        $transfer->update($request->except('functions'));
        $transfer->functions()->sync($request->functions);

        return redirect(route('admin.transfer.index'))->with('message', 'تم تحديث المعلومات بنجاح!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transfer  $transfer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transfer $transfer)
    {
        $transfer->delete();

        return redirect(route('admin.transfer.index'))->with('message', 'تم حذف المعلومات بنجاح!');
    }
}
