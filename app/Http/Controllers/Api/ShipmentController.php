<?php

namespace App\Http\Controllers\Api;

use App\Models\Shipment;
use App\Models\AddressType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShipmentController extends Controller
{
    /**
     * Filter a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // EX: address: sender, Status = new, not yet, done
    // shipmentStatus = new(order status is 1), not delivered(order status 1 or 2),
    // done (order status is 3 or 4)
    public function filter($addressType = null, $shipmentStatus = null)
    {
        $user = auth()->user();

        $shipments = collect();

        if($addressType == null or $addressType == 'all')
        {
            $shipments = Shipment::whereHas('receiver', function($query) use ($user){
                return $query->where('user_id', $user->id);
            })->orWhereHas('sender', function($query) use ($user){
                return $query->where('user_id', $user->id);
            });
        }
        else
        {
            // make sure the address type is lised in our DB;
            $addressTypeDB = AddressType::where('code_name', $addressType)->first();

            if(! is_null($addressTypeDB))
            {
                $shipments = Shipment::whereHas($addressTypeDB->code_name, function($query) use ($user){
                    return $query->where('user_id', $user->id);
                });
            }
        }

        if(! is_null($shipmentStatus))
        {
            $shipmentStatusRules = [
                'new' => [1],
                'not_delivered' => [1, 2],
                'expired' => [4]
            ];
            
            if(array_key_exists($shipmentStatus, $shipmentStatusRules))
            {
                $ruleIds = $shipmentStatusRules[$shipmentStatus];
            }
            if(isset($addressTypeDB) && ! is_null($addressTypeDB))
            {
                $shipments->whereHas($addressTypeDB->code_name, function($query) use ($user){
                    return $query->where('user_id', $user->id);
                })->whereHas('order', function($query) use($ruleIds){
                    return $query->whereIn('order_status_id', $ruleIds);
                });
            }else{
                $shipments->whereHas('order', function($query) use($ruleIds){
                    return $query->whereIn('order_status_id', $ruleIds);
                });
            }
        }

        return response()->json($shipments->get());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();

        // get all shipments that's new by its order status
        $shipments = Shipment::whereHas('receiver', function($query) use ($user){
            return $query->where('user_id', $user->id);
        })->orWhereHas('sender', function($query) use ($user){
            return $query->where('user_id', $user->id);
        })->whereHas('order', function($query){
            return $query->where('order_status_id', 1);
        })->get();

        return view('app.shipment.index', compact('shipments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Shipment  $shipment
     * @return \Illuminate\Http\Response
     */
    public function show(Shipment $shipment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Shipment  $shipment
     * @return \Illuminate\Http\Response
     */
    public function edit(Shipment $shipment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Shipment  $shipment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shipment $shipment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shipment  $shipment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shipment $shipment)
    {
        //
    }

    public function get_track()
    {
        return view('app.shipment.track');
    }

    public function post_track(Request $request)
    {
        $this->validate($request, ['tracking_number' => 'required']);

        $shipment = Shipment::where('tracking_number', $request->tracking_number)->first();
        
        return view('app.shipment.trackWithValue', compact('shipment'));

    }
}
