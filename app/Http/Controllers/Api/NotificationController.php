<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notifications = auth()->user()->unreadNotifications()->get();
        $notifications->markAsRead();

        return view('app.user.notification', compact('notifications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('app.coupon.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['code' => 'required']);

        $coupon = Coupon::where('code', $request->code)
                        ->where('expire_on', '>', \Carbon\Carbon::now()->format('d/m/Y'))->first();

        if(is_null($coupon))
        {
            return back()->with('message', 'هذه القسيمة غير صالحة.');
        }

        $user = auth()->user();
        
        // if user already used the coupon?
        if($user->coupons->contains($coupon))
        {
            return back()->with('message', 'لقد قمت بتفعيل هذا الكوبون مسبقاً.');
        }

        $user->coupons()->attach($coupon);

        return back()->with('message', 'تم تفعيل القسيمة بنجاح! ستحصل على خصم قدره ' . $coupon->discount_percentage .'%');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function show(Coupon $coupon)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function edit(Coupon $coupon)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coupon $coupon)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coupon $coupon)
    {
        //
    }
}
