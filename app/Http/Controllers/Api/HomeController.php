<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Slider;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $sliders = Slider::latest()->get();

        return view('app.home.index', compact('sliders'));
    }

    public function contact()
    {
        return view('app.home.contact');
    }

    public function privacy()
    {
        return view('app.home.privacy');
    }

    public function term()
    {
        return view('app.home.term');
    }

    public function about()
    {
        return view('app.home.about');
    }
}
