<?php

namespace App\Http\Controllers\Api;

use App\Models\Coupon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // DELETE any coupoun that expired
        // $coupon = Coupon::whereDate('expire_on', '>', Carbon::now()->format('d/m/Y'))->get();
        // dd($coupon);
        
        $user = auth()->user();
        $coupons = Coupon::with('users')->whereHas('users', function($query) use($user){
            return $query->where('user_id', $user->id);
        })->get();

        return view('app.coupon.create', compact('coupons'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['code' => 'required']);
// dd("21/03/2019" > \Carbon\Carbon::now()->format('d/m/Y'));
        $coupon = Coupon::whereDate('expire_on', '>=', \Carbon\Carbon::now())->where('code', $request->code)
        ->first();

        if(is_null($coupon))
        {
            return back()->with('message', 'هذه القسيمة غير صالحة.');
        }

        $user = auth()->user();
        
        // if user has already a coupon?
        if($user->coupons->count() > 0)
        {
            return back()->with('message', 'تستطيع تفعيل كوبون واحد فقط.');
        }

        // if user already used the coupon?
        if($user->coupons->contains($coupon))
        {
            return back()->with('message', 'لقد قمت بتفعيل هذا الكوبون مسبقاً.');
        }

        $user->coupons()->attach($coupon);

        return back()->with('message', 'تم تفعيل القسيمة بنجاح! ستحصل على خصم قدره ' . $coupon->discount_percentage .'%');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function show(Coupon $coupon)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function edit(Coupon $coupon)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coupon $coupon)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coupon $coupon)
    {
        auth()->user()->coupons()->detach($coupon);

        return back()->with('message', 'تم حذف القسيمة بنجاح.');
    }
}
