<?php

namespace App\Http\Controllers\Api;

use App\Models\Order;
use App\Models\Address;
use App\Models\ShipmentProviderSchedule;
use App\Models\Day;
use App\Models\Country;
use App\Models\Shipment;
use App\Models\ShipmentService;
use App\Models\ShipmentMoneyReceiver;
use App\Models\ShipmentMethod;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::where('user_id', auth()->user()->id)->get();

        return view('app.order.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // var_dump(\Carbon\Carbon::parse('12:30 AM')->format('H:i'));
        // var_dump(\Carbon\Carbon::now()->format("H:i"));
        // dd(\Carbon\Carbon::parse('2:10 AM')->format('H:i') > \Carbon\Carbon::now()->format("H:i"));
        $day_id = \Carbon\Carbon::now()->dayOfWeek;
        // if Sunday
        if($day_id == 0)
        {
            $day_id = 7;
        }

        $schedulesForToday = ShipmentProviderSchedule::
        whereHas('days', function($query) use ($day_id){
            return $query->where('day_id', $day_id);
        })->with(['days' => function($q) use ($day_id){
            return $q->where('day_id',  $day_id);
        }])->where('from', '>', \Carbon\Carbon::now()->format("H:i"))
        ->where('enable', 1)->get();

        $schedulesForTomorrow = ShipmentProviderSchedule::whereHas('days', function($query) use ($day_id){
            return $query->where('day_id', $day_id + 1);
        })->with(['days' => function($q) use ($day_id){
            return $q->where('day_id',  $day_id + 1);
        }])->where('enable', 1)->get();

        $mains = Address::where('address_type_id', 1)->where('user_id', auth()->user()->id)->get();
        $senders = Address::where('address_type_id', 2)->where('user_id', auth()->user()->id)->get();
        $receivers = Address::where('address_type_id', 3)->where('user_id', auth()->user()->id)->get();
        $services = ShipmentService::get();
        $methods = ShipmentMethod::latest()->pluck('name', 'id');

        return view('app.order.create', compact('methods','services','schedulesForTomorrow','mains','senders', 'receivers', 'schedulesForToday'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'schedule_id' => 'required',
            'item_cost' => 'required',
            // 'sender_address_id' => 'not_in:' . $request->sender_address_id,
            // 'receiver_address_id' => 'not_in:' . $request->receiver_address_id,
            'sender_name' => 'required_with:saveSenderAddress,true',
            'receiver_name' => 'required_with:saveReceiverAddress,true',
            'item_cost' => 'required_if:shipment_method_id,1',
        ]);

        // create a new sender address
        if(is_null($request->sender_address_id))
        {
            $sender_address = new Address();
            $sender_address->name = $request->sender_name;
            $sender_address->country_id = $request->sender_country_id;
            $sender_address->address_type_id = 2;
            $sender_address->city = $request->sender_city;
            $sender_address->area = $request->sender_area;
            $sender_address->phone = $request->sender_phone;
            $sender_address->user_id = auth()->user()->id;
            $sender_address->save();
        }
        // if sender_address_id exists, means he/she select an address
        else if(! is_null($request->sender_address_id))
        {
            $sender_address = Address::find($request->sender_address_id);
        }
        else
        {
            
        }
        
        // create a new receiver address
        if(is_null($request->receiver_address_id))
        {
            $receiver_address = new Address();
            $receiver_address->name = $request->receiver_name;
            $receiver_address->country_id = $request->receiver_country_id;
            $receiver_address->address_type_id = 3;
            $receiver_address->city = $request->receiver_city;
            $receiver_address->area = $request->receiver_area;
            $receiver_address->phone = $request->receiver_phone;
            $receiver_address->user_id = auth()->user()->id;
            $receiver_address->save();
        }
        // if receiver_address_id exists, means he/she select an address
        else if(! is_null($request->receiver_address_id))
        {
            $receiver_address = Address::find($request->receiver_address_id);
        }

        $order = Order::create(['user_id' => auth()->user()->id, 'order_status_id' => 1]);

        $shipment = new Shipment();
        $shipment->tracking_number = 'WQW1232';
        $shipment->order_id = $order->id;
        $shipment->sender_address = $sender_address->id;
        $shipment->receiver_address = $receiver_address->id;
        $shipment->item_cost = $request->item_cost;
        $shipment->date = \Carbon\Carbon::today();
        $shipment->shipment_method_id = $request->shipment_method_id;
        $shipment->shipment_provider_schedule_id = $request->schedule_id;
        $shipment->shipment_money_receiver_id = 1;
        $shipment->save();

        $status = new \App\Models\ShipmentStatus();
        $status->name = 'جاري إستلام الشحنة من المرسل';
        $status->shipment_id = $shipment->id;
        $status->save();

        // if select service
        $shipment->services()->attach($request->services);

        session(['user_' . auth()->user()->id. '_order' => $order]);

        // update user balance
        // if(! is_null($request->item_cost))
        // {
        //     $user->balance()->update([]);
        // }
        if(auth()->user()->policies > 0)
        {
            auth()->user()->update(['policies' => auth()->user()->policies - 1]);
        }
        

        return redirect(route('app.order.confirm'))->with('message', 'من فضلك قم بمراجعة الطلب وتأكيده');
    }
    
    public function get_confirm()
    {
        $order = session('user_' . auth()->user()->id . '_order');
        
        $money_receivers = ShipmentMoneyReceiver::get();
        $sender_address = $order->shipment->sender;
        $receiver_address = $order->shipment->receiver;
        $shipment = $order->shipment;
        // show balance of sender if he choose to deduct money from his balanace.
        $balance = \App\Models\Balance::where('user_id', auth()->user()->id)->first();

        return view('app.order.confirm', compact('balance','shipment', 'sender_address', 'receiver_address', 'money_receivers'));
    }

    public function post_confirm(Request $request)
    {
        $this->validate($request, [
            'money_receiver' => 'required',
        ]);
        // if he want to deduct from balanace and its balance lower than total cost. show error.
        $balance = \App\Models\Balance::where('user_id', auth()->user()->id)->first();
        $appSetting = \App\Models\AppSetting::first();
        if($request->money_receiver == 3 && $appSetting->delivery_cost > $balance->balance)
        {
            return back()->withErrors('رصيدك الحالي لا يكفي لتحمل تكلفة التوصيل');
        }

        $order = session('user_' . auth()->user()->id . '_order');
        $order->shipment->update(['shipment_money_receiver_id' => $request->money_receiver]);
        // add the balance.
        
        $this->updateBalance($request, $order);

        // remove the session
        session()->forget('user_' . auth()->user()->id . '_order');

        return redirect(route('app.user.show', auth()->user()))->with('message', 'تم إنشاء طلبك رقم (' . $order->id .'#) بنجاح!.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }

    // if item_cost is null
        // if sender
        // if delivery money set to pay by sender, then paytowebsite is 35, websitepaytouser 0, balance -35
        // ******************************* receiver, then paytowebsite is 0, 0, 0

        // if receiver
        // pay by sender, paytowebsite 0, websitepaytouser 0, balance 0
        // pay by receiver, paytowebsite 35, websitepaytouser 0, balance -35
        // ************* //

    // if item_cost not null
        // if sender
        // if delivery money set to pay by sender, then paytowebsite is 35, websitepaytouser 150, balance 115
        // ******************************* receiver, then paytowebsite is 0, 0, 0

        // if receiver
        // pay by sender, paytowebsite 0, websitepaytouser 0, balance 0
        // pay by receiver, paytowebsite 35, websitepaytouser 0, balance -35
    private function updateBalance($request, $order)
    {
        $delivery_cost = \App\Models\AppSetting::first()->getDeliveryCostForUser(auth()->user());
        $total = session('total'); // total cost
        $totalWithoutItemCost = session('totalWithoutItemCost'); //deliver_cost without;
        $transfer = new \App\Models\Transfer();

        // take out one of policy
        if(is_null($order->shipment->item_cost))
        {
            // if order requested by sender
            if($order->shipment->sender->user->id == auth()->user()->id)
            {
                // if sender pay delviery
                if($request->money_receiver == 1)
                {
                    // $balance = auth()->user()->balance;
                    // // $balance->paytowebsite = $delivery_cost;
                    // // $balance->paytouser = 0;
                    // // $balance->balance += (-$delivery_cost);
                    // $balance->balance += -$delivery_cost;
                    // $balance->save();
                    $transfer->transfer_type_id = 2;
                    $transfer->amount = $totalWithoutItemCost;
                    $transfer->user_id = auth()->user()->id;
                    $transfer->activate = 1;
                    $transfer->save();
                }
                // receiver
                else if($request->money_receiver == 2)
                {
                    //do nothing.
                    // $balance = auth()->user()->balance;
                    // $balance->paytowebsite = 0;
                    // $balance->paytouser = 0;
                    // $balance->balance += 0;
                    // $balance->balance = $balance->paytowebsite - $balance->paytouser;

                    // $balance->save();
                }
                else if($request->money_receiver == 3)
                {
                    // $balance = auth()->user()->balance;
                    // // $balance->paytowebsite = $delivery_cost;
                    // // $balance->paytouser = 0;
                    // $balance->balance += -$delivery_cost;
                    // $balance->save();
                    $transfer->transfer_type_id = 2;
                    $transfer->amount = $totalWithoutItemCost;
                    $transfer->user_id = auth()->user()->id;
                    $transfer->activate = 1;
                    $transfer->save();
                }
            }
            // receiver side
            else if($order->shipment->receiver->user->id == auth()->user()->id)
            {
                // if sender pay delviery
                if($request->money_receiver == 1)
                {
                    // $balance = auth()->user()->balance;
                    // // $balance->paytowebsite = 0;
                    // // $balance->paytouser = 0;
                    // // $balance->balance += 0;
                    // $balance->save();
                }
                // receiver pay delivery
                else if($request->money_receiver == 2)
                {
                    // $balance = auth()->user()->balance;
                    // // $balance->paytowebsite = $total;
                    // // $balance->paytouser = 0;
                    // $balance->balance += (-$total);
                    // $balance->save();
                    $transfer->transfer_type_id = 2;
                    $transfer->amount = $totalWithoutItemCost;
                    $transfer->user_id = auth()->user()->id;
                    $transfer->activate = 1;
                    $transfer->save();
                }
                else if($request->money_receiver == 3)
                {
                    // $balance = auth()->user()->balance;
                    // // $balance->paytowebsite = $total;
                    // // $balance->paytouser = 0;
                    // $balance->balance += (-$total);
                    // $balance->save();
                    $transfer->transfer_type_id = 2;
                    $transfer->amount = $totalWithoutItemCost;
                    $transfer->user_id = auth()->user()->id;
                    $transfer->activate = 1;
                    $transfer->save();
                }
            }
        }
        else if(! is_null($order->shipment->item_cost))
        {
            // if order requested by sender
            if($order->shipment->sender->user->id == auth()->user()->id)
            {
                // if sender pay delviery
                if($request->money_receiver == 1)
                {
                    // $balance = auth()->user()->balance;
                    // // $balance->paytowebsite = $total;
                    // // $balance->paytouser = $order->shipment->item_cost;
                    // $balance->balance += $order->shipment->item_cost + (-$total);
                    // $balance->save();
                    $transfer->transfer_type_id = 2;
                    $transfer->amount = $order->shipment->item_cost + (-$totalWithoutItemCost);
                    $transfer->user_id = auth()->user()->id;
                    $transfer->activate = 1;
                    $transfer->save();
                }
                else if($request->money_receiver == 2)
                {
                    // $balance = auth()->user()->balance;
                    // // $balance->paytowebsite = 0;
                    // // $balance->paytouser = $order->shipment->item_cost;
                    // $balance->balance += $order->shipment->item_cost;
                    // $balance->save();

                    $transfer->transfer_type_id = 2;
                    $transfer->amount = $order->shipment->item_cost;
                    $transfer->user_id = auth()->user()->id;
                    $transfer->activate = 1;
                    $transfer->save();
                }
                else if($request->money_receiver == 3)
                {
                    // $balance = auth()->user()->balance;
                    // // $balance->paytowebsite = $total;
                    // // $balance->paytouser = $order->shipment->item_cost;
                    // $balance->balance += $order->shipment->item_cost + (-$total);
                    // $balance->save();
                    $transfer->transfer_type_id = 2;
                    $transfer->amount = $order->shipment->item_cost + (-$totalWithoutItemCost);
                    $transfer->user_id = auth()->user()->id;
                    $transfer->activate = 1;
                    $transfer->save();
                }
            }
            // receiver side
            else if($order->shipment->receiver->user->id == auth()->user()->id)
            {
                // if sender pay delviery
                if($request->money_receiver == 1)
                {
                    // $balance = auth()->user()->balance;
                    // // $balance->paytowebsite = 0;
                    // // $balance->paytouser = $order->shipment->item_cost;
                    // $balance->balance += $order->shipment->item_cost;
                    // $balance->save();
                    $transfer->transfer_type_id = 2;
                    $transfer->amount = $order->shipment->item_cost;
                    $transfer->user_id = auth()->user()->id;
                    $transfer->activate = 1;
                    $transfer->save();
                }
                // receiver pay delivery
                else if($request->money_receiver == 2)
                {
                    // $balance = auth()->user()->balance;
                    // // $balance->paytowebsite = $total;
                    // // $balance->paytouser = $order->shipment->item_cost;
                    // $balance->balance += $order->shipment->item_cost + (-$total);
                    // $balance->save();
                    $transfer->transfer_type_id = 2;
                    $transfer->amount = $order->shipment->item_cost + (-$totalWithoutItemCost);
                    $transfer->user_id = auth()->user()->id;
                    $transfer->activate = 1;
                    $transfer->save();
                }
                else if($request->money_receiver == 3)
                {
                    // $balance = auth()->user()->balance;
                    // // $balance->paytowebsite = $total;
                    // // $balance->paytouser = $order->shipment->item_cost;
                    // $balance->balance += $order->shipment->item_cost + (-$total);
                    // $balance->save();
                    $transfer->transfer_type_id = 2;
                    $transfer->amount = $order->shipment->item_cost + (-$totalWithoutItemCost);
                    $transfer->user_id = auth()->user()->id;
                    $transfer->activate = 1;
                    $transfer->save();
                }
            }
        }
    }
}
