<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Spatie\Permission\Models\Role;
use App\Models\TransferDuration;
use App\Models\Package;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $users = User::orderBy('id', 'desc')->get();

        // return view('app.user.show', auth()->user(), compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $roles = Role::pluck('display_name', 'id');

        // return view('app.user.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, 
            ['email' => 'email|unique:users']
        );

        $user = User::create($request->except(['role_id']));
        
        $user->assignRole($request->role_id);
        
        return redirect(route('app.user.show', auth()->user()))->with('message', 'تم حفظ المعلومات بنجاح!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('app.user.show', auth()->user(), compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $roles = Role::pluck('display_name', 'id');

        return view('app.user.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->validate($request, 
            ['email' => 'email|unique:users,email,'.$user->id]
        );

        $user->update($request->except(['password']));

        if(! is_null($request->password))
        {
            $user->password = bcrypt($request->password);
            $user->save();
        }
        
        return redirect(route('app.user.show', auth()->user()))->with('message', 'تم حفظ المعلومات بنجاح!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        
        return redirect(route('app.user.show', auth()->user()))->with('message', 'تم حذف المعلومات بنجاح!');
    }

    public function duration()
    {
        $durations = TransferDuration::pluck('name', 'id');

        return view('app.transfer.duration', compact('durations'));
    }

    public function post_duration(Request $request)
    {
        $duration = TransferDuration::findOrFail($request->transfer_duration_id);
        auth()->user()->update(['transfer_duration_id' => $duration->id]);

        return redirect(route('app.user.show', auth()->user(), auth()->user()))->with('message', 'تم تحديث بياناتك بنجاح!');
    }

    public function setting()
    {
        $user = auth()->user();
        $packages = Package::pluck('name', 'id');

        return view('app.user.setting', compact('user', 'packages'));
    }
}
