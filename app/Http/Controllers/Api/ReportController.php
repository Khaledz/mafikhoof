<?php

namespace App\Http\Controllers\Api;

use App\Models\Report;
use App\Models\Shipment;
use App\Models\Transfer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('app.report.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->date_from);
        $user = auth()->user();
        // dd(\Carbon\Carbon::parse('2019-02-22')->format('Y-m-d') > \Carbon\Carbon::now()->format('Y-m-d'));
        $this->validate($request, 
            [
                'date_from'  => 'required',
                'date_to'  => 'required' ,
                'functions' => 'required',
                'email' => 'required|email'
            ]
        );
        
        // save the record in the DB.
        $report = Report::create(array_merge($request->except('functions'), ['user_id' => auth()->user()->id]));
        $report->functions()->sync($request->functions);

        // generate the report data.
        $data = collect();
        $user = auth()->user();
        $shipmentStatusRules = [1,2];

        foreach($request->functions as $function_id)
        {
            if($function_id == 1)
            {
                $data->shipments = Shipment::with('status')->where(function($query) use($request){
                    $query->whereDate('created_at', '>=', \Carbon\Carbon::parse($request->date_from))
                    ->whereDate('created_at', '<=', \Carbon\Carbon::parse($request->date_to));
                })->whereHas('receiver', function($query) use ($user){
                    return $query->where('user_id', $user->id);
                })->WhereHas('sender', function($query) use ($user){
                    return $query->where('user_id', $user->id);
                })->get();
            }
            else if($function_id == 2)
            {
                $data->billed = Shipment::with('status')->where(function($query) use($request){
                    $query->whereDate('created_at', '>=', \Carbon\Carbon::parse($request->date_from))
                    ->whereDate('created_at', '<=', \Carbon\Carbon::parse($request->date_to));
                })->whereHas('receiver', function($query) use ($user){
                    return $query->where('user_id', $user->id);
                })->WhereHas('sender', function($query) use ($user){
                    return $query->where('user_id', $user->id);
                })->whereHas('order', function($query) use($shipmentStatusRules){
                    return $query->whereIn('order_status_id', $shipmentStatusRules);
                })
                ->get();
            }
            else if($function_id == 3)
            {
                // get orders that not deliver
                $orderStatusRules = [
                    1, 2
                ];
                $data->sumItemsNotDeliver = Shipment::with('status')->where(function($query) use($request){
                    $query->whereDate('created_at', '>=', \Carbon\Carbon::parse($request->date_from))
                    ->whereDate('created_at', '<=', \Carbon\Carbon::parse($request->date_to));
                })->whereHas('receiver', function($query) use ($user){
                    return $query->where('user_id', $user->id);
                })->orWhereHas('sender', function($query) use ($user){
                    return $query->where('user_id', $user->id);
                })->whereHas('order', function($query) use($orderStatusRules){
                    return $query->whereIn('order_status_id', $orderStatusRules);
                })->sum('item_cost');

                $data->sumLastTwoTransfers = Transfer::where('user_id', auth()->user()->id)->latest()->take(2)
                ->whereDate('created_at', '>=', \Carbon\Carbon::parse($request->date_from)->format('Y-m-d'))
                ->whereDate('created_at', '<=', \Carbon\Carbon::parse($request->date_to)->format('Y-m-d'))
                ->get()->sum('amount');
            }
            else if($function_id == 4)
            {
                $data->transfers = Transfer::where('user_id', auth()->user()->id)
                ->whereDate('created_at', '>=', \Carbon\Carbon::parse($request->date_from)->format('Y-m-d'))
                ->whereDate('created_at', '<=', \Carbon\Carbon::parse($request->date_to)->format('Y-m-d'))
                ->latest()->take(10)->get();
            }
        }
        $data->report = $report;

        // send email here
        \Mail::to(auth()->user())->send(new \App\Mail\MailReport($data));
        
        return redirect(route('app.report.index'))->with('message', 'تم إرسال التقرير إلى البريد اﻹلكتروني');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function show(Report $report)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function edit(Report $report)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Report $report)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function destroy(Report $report)
    {
        //
    }
}
