<?php

namespace App\Http\Controllers\Admin;

use App\Models\Shipment;
use App\Models\Address;
use App\Models\ShipmentProviderSchedule;
use App\Models\ShipmentMoneyReceiver;
use App\Models\ShipmentMethod;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ShipmentStatus;
use App\Models\ShipmentService;

class ShipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shipments = Shipment::latest()->get();

        return view('admin.shipment.index', compact('shipments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.shipment.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, 
            ['email' => 'email|unique:users']
        );

        $user = User::create($request->except(['role_id']));
        
        $user->assignRole($request->role_id);
        
        return redirect(route('admin.user.index'))->with('message', 'تم حفظ المعلومات بنجاح!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Shipment  $shipment
     * @return \Illuminate\Http\Response
     */
    public function show(Shipment $shipment)
    {
        return view('admin.shipment.show', compact('shipment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Shipment  $shipment
     * @return \Illuminate\Http\Response
     */
    public function edit(Shipment $shipment)
    {
        $addresses = Address::where('user_id', $shipment->order->user_id)->pluck('name', 'id');
        $money_receivers = ShipmentMoneyReceiver::pluck('name', 'id');
        $shipment_provider_schedules = ShipmentProviderSchedule::selectRaw('CONCAT(shipment_providers.name, " (من ",`from`," إلى ",`to`, ") ") as text, shipment_provider_schedules.id')
        ->join('shipment_providers', 'shipment_providers.id', '=', 'shipment_provider_schedules.shipment_provider_id')
        ->pluck('text', 'id');
        $services = ShipmentService::get();
        $methods = ShipmentMethod::pluck('name', 'id');
        
        return view('admin.shipment.edit', compact('methods', 'services','shipment', 'addresses', 'money_receivers', 'shipment_provider_schedules'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Shipment  $shipment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shipment $shipment)
    {
        $old_shipment_money_receiver_id = $shipment->shipment_money_receiver_id;

        $shipment->update($request->except('shipment_status_name'));
        // $shipment_status = new ShipmentStatus(['name' => $request->shipment_status_name]);
        // $shipment->status()->save($shipment_status);
        $balance = $shipment->order->user->balance;

        // if pay f delivery changed?
        // if($request->shipment_money_receiver_id == 1 &&
        // $old_shipment_money_receiver_id != $request->shipment_money_receiver_id)
        // {
        //     $balance->paytowebsite += (\App\Models\AppSetting::first()->getDeliveryCostForUser(auth()->user()));
        //     $balance->paytouser += -(\App\Models\AppSetting::first()->getDeliveryCostForUser(auth()->user()));
        //     $balance->balance += -(\App\Models\AppSetting::first()->getDeliveryCostForUser(auth()->user()));
        //     $balance->save();
        // }
        // else if($request->shipment_money_receiver_id == 2 &&
        // $old_shipment_money_receiver_id != $request->shipment_money_receiver_id)
        // {
        //     $balance->paytouser += (\App\Models\AppSetting::first()->getDeliveryCostForUser(auth()->user()));
        //     $balance->balance += (\App\Models\AppSetting::first()->getDeliveryCostForUser(auth()->user()));
        //     $balance->save();
        // }
        // else if($request->shipment_money_receiver_id == 3 &&
        // $old_shipment_money_receiver_id != $request->shipment_money_receiver_id
        // && $balance->balance > \App\Models\AppSetting::first()->getDeliveryCostForUser(auth()->user()))
        // {
            
        //     $balance->paytowebsite += (\App\Models\AppSetting::first()->getDeliveryCostForUser(auth()->user()));
        //     $balance->paytouser += -(\App\Models\AppSetting::first()->getDeliveryCostForUser(auth()->user()));
        //     $balance->balance += -(\App\Models\AppSetting::first()->getDeliveryCostForUser(auth()->user()));
        //     $balance->save();
        // }

        return back()->with('message', 'تم حفظ المعلومات بنجاح!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shipment  $shipment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shipment $shipment)
    {
        return back()->with('message', 'لا يمكنك حذف تفاصيل الشحنة لهذا الطلب!');

        $shipment->delete();

        return back()->with('message', 'تم حذف المعلومات بنجاح!');
    }
}
