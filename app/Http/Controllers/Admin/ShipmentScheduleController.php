<?php

namespace App\Http\Controllers\Admin;

use App\Models\ShipmentProvider;
use App\Models\ShipmentProviderSchedule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Shipment;

class ShipmentScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schedules = ShipmentProviderSchedule::orderBy('id', 'desc')->get();

        return view('admin.shipment_schedule.index', compact('schedules'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $providers = ShipmentProvider::pluck('name', 'id');

        return view('admin.shipment_schedule.create', compact('providers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        // $this->validate($request, 
        //     ['email' => 'email|unique:users']
        // );

        $schedule = ShipmentProviderSchedule::create($request->except('days'));
        $schedule->days()->attach($request->days);
        return redirect(route('admin.shipment-schedule.index'))->with('message', 'تم حفظ المعلومات بنجاح!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Shipment  $shipment
     * @return \Illuminate\Http\Response
     */
    public function show(Shipment $shipment)
    {
        // return view('admin.shipment.show', compact('shipment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Shipment  $shipment
     * @return \Illuminate\Http\Response
     */
    public function edit(ShipmentProviderSchedule $schedule)
    {
        $providers = ShipmentProvider::pluck('name', 'id');

        return view('admin.shipment_schedule.edit', compact('schedule', 'providers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Shipment  $shipment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ShipmentProviderSchedule $schedule)
    {
        $schedule->update($request->except('days'));
        $schedule->days()->sync($request->days);

        return redirect(route('admin.shipment-schedule.index'))->with('message', 'تم حفظ المعلومات بنجاح!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shipment  $shipment
     * @return \Illuminate\Http\Response
     */
    public function destroy(ShipmentProviderSchedule $schedule)
    {
        return redirect(route('admin.shipment-schedule.index'))->with('message', 'بإمكانك تعطيل هذا الوقت ﻷنه مرتبط بطلبات مسبقة.');
        $schedule->delete();
        $schedule->days()->detach();

        return redirect(route('admin.shipment-schedule.index'))->with('message', 'تم حذف المعلومات بنجاح!');
    }
}
