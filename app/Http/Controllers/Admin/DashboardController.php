<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use App\Models\User;
use App\Models\Package;
use App\Models\Shipment;
use App\Models\Transfer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::with('shipment.receiver')->latest()->limit(5)->get();
        $users = User::latest()->limit(5)->get();
        $shipments = Shipment::latest()->limit(5)->get();
        $transfers = Transfer::latest()->limit(5)->get();

        $packages = Package::get();
        $users_count = User::count();
        $pendingOrders = Order::whereIn('order_status_id', [1,2])->count();
        // $shipmentOrders = Order::where('order_status_id', 2)->count();
        // $pendingTransfer = Order::doesntHave('transfer')->where('order_status_id', 3)->count();
        $shipmentOrders = \App\Models\Shipment::whereHas('order', function($query) {
            return $query->whereIn('order_status_id', [1,2]);
        })->get()->sum('item_cost');

        $pendingTransfer = \App\Models\Balance::get()->sum('balance');


        return view('admin.dashboard.index', compact('pendingTransfer','pendingOrders', 'shipmentOrders','shipments', 'transfers', 'packages', 'orders', 'users', 'users_count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function show(Address $address)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function edit(Address $address)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Address $address)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function destroy(Address $address)
    {
        //
    }
}
