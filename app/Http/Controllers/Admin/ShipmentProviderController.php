<?php

namespace App\Http\Controllers\Admin;

use App\Models\ShipmentProvider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShipmentProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $providers = ShipmentProvider::orderBy('id', 'desc')->get();

        return view('admin.shipment_provider.index', compact('providers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.shipment_provider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, 
            ['name' => 'required']
        );

        ShipmentProvider::create($request->all());
        
        return redirect(route('admin.shipment-provider.index'))->with('message', 'تم حفظ المعلومات بنجاح!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ShipmentProvider  $provider
     * @return \Illuminate\Http\Response
     */
    public function show(ShipmentProvider $provider)
    {
        return view('admin.shipment_provider.show', compact('provider'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ShipmentProvider  $provider
     * @return \Illuminate\Http\Response
     */
    public function edit(ShipmentProvider $provider)
    {
        return view('admin.shipment_provider.edit', compact('provider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ShipmentProvider  $provider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ShipmentProvider $provider)
    {
        $this->validate($request, 
            ['name' => 'required']
        );
        
        $provider->update($request->all());

        return redirect(route('admin.shipment-provider.index'))->with('message', 'تم حفظ المعلومات بنجاح!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ShipmentProvider  $provider
     * @return \Illuminate\Http\Response
     */
    public function destroy(ShipmentProvider $provider)
    {
        $provider->delete();

        return redirect(route('admin.shipment-provider.index'))->with('message', 'تم حذف المعلومات بنجاح!');
    }
}
