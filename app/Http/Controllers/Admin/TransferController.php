<?php

namespace App\Http\Controllers\Admin;

use App\Models\Transfer;
use App\Models\User;
use App\Models\Order;
use App\Models\Bank;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transfers = Transfer::latest()->get();

        return view('admin.transfer.index', compact('transfers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::pluck('name', 'id');
        $orders = Order::pluck('id', 'id');
        $banks = Bank::pluck('id', 'id');
        $types = \App\Models\TransferType::pluck('name', 'id');

        return view('admin.transfer.create', compact('users', 'orders', 'banks', 'types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request, 
        //     ['email' => 'email|unique:users']
        // );

        $transfer = Transfer::create($request->all());
        
        // We need to notify the user.
        User::find($request->user_id)->notify(new \App\Notifications\NewTransfer($transfer));
        
        return redirect(route('admin.transfer.index'))->with('message', 'تم حفظ المعلومات بنجاح!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transfer  $transfer
     * @return \Illuminate\Http\Response
     */
    public function show(Transfer $transfer)
    {
        return view('admin.transfer.show', compact('transfer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transfer  $transfer
     * @return \Illuminate\Http\Response
     */
    public function edit(Transfer $transfer)
    {
        $users = User::pluck('name', 'id');
        $types = \App\Models\TransferType::pluck('name', 'id');


        return view('admin.transfer.edit', compact('users','transfer', 'types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transfer  $transfer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transfer $transfer)
    {
        $transfer->update($request->all());
        

        return redirect(route('admin.transfer.index'))->with('message', 'تم تحديث المعلومات بنجاح!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transfer  $transfer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transfer $transfer)
    {
        $transfer->delete();

        return redirect(route('admin.transfer.index'))->with('message', 'تم حذف المعلومات بنجاح!');
    }
}
