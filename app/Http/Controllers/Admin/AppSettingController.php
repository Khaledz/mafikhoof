<?php

namespace App\Http\Controllers\Admin;

use App\Models\AppSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AppSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = AppSetting::latest()->get();

        return view('admin.app_setting.index', compact('settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AppSetting  $address
     * @return \Illuminate\Http\Response
     */
    public function show(AppSetting $address)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AppSetting  $address
     * @return \Illuminate\Http\Response
     */
    public function edit(AppSetting $setting)
    {
        return view('admin.app_setting.edit', compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AppSetting  $address
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AppSetting $setting)
    {
        $setting->update($request->all());

        return back()->with('message', 'تم حفظ المعلومات بنجاح!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppSetting  $address
     * @return \Illuminate\Http\Response
     */
    public function destroy(AppSetting $address)
    {
        $address->delete();

        return back()->with('message', 'تم حذف المعلومات بنجاح!');
    }
}
