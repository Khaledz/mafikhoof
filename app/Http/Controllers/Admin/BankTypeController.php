<?php

namespace App\Http\Controllers\Admin;

use App\Models\Bank;
use App\Models\BankType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BankTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = BankType::orderBy('id', 'desc')->get();

        return view('admin.bank_type.index', compact('types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.bank_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request, 
        //     ['email' => 'email|unique:users']
        // );
        BankType::create($request->all());
        
        return redirect(route('admin.bank-type.index'))->with('message', 'تم حفظ المعلومات بنجاح!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BankType  $type
     * @return \Illuminate\Http\Response
     */
    public function show(BankType $type)
    {
        return view('admin.bank_type.show', compact('type'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BankType  $type
     * @return \Illuminate\Http\Response
     */
    public function edit(BankType $type)
    {
        return view('admin.bank_type.edit', compact('type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BankType  $type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BankType $type)
    {
        $type->update($request->all());

        return redirect(route('admin.bank-type.index'))->with('message', 'تم تحديث المعلومات بنجاح!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BankType  $type
     * @return \Illuminate\Http\Response
     */
    public function destroy(BankType $type)
    {
        $type->delete();

        return redirect(route('admin.bank-type.index'))->with('message', 'تم حذف المعلومات بنجاح!');
    }
}
