<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Order;
use App\Models\Package;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function list()
    {
        $users = User::latest()->get();

        return response()->json(['data' => $users]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::latest()->paginate(15);

        return view('admin.user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('display_name', 'id');
        $packages = Package::pluck('name', 'id');

        return view('admin.user.create', compact('roles', 'packages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, 
            ['email' => 'email|unique:users']
        );

        $user = User::create($request->except(['role_id']));
        $user->update(['policies' => \App\Models\Package::find($request->package_id)->policy_number]);
        
        $user->assignRole($request->role_id);
        
        return redirect(route('admin.user.index'))->with('message', 'تم حفظ المعلومات بنجاح!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $pendingOrders = Order::where('user_id', $user->id)->whereIn('order_status_id', [1,2])->count();
        $pendingTransfer = \App\Models\Balance::where('user_id', $user->id)->first()->balance;

        return view('admin.user.show', compact('user', 'pendingOrders','pendingTransfer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $roles = Role::pluck('display_name', 'id');
        $packages = Package::pluck('name', 'id');

        return view('admin.user.edit', compact('user', 'roles', 'packages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->validate($request, 
            ['email' => 'email|unique:users,email,'.$user->id]
        );

        $user->update($request->except(['role_id', 'password']));

        if(! is_null($user->password))
        {
            $user->password = bcrypt($request->password);
            $user->save();
        }

        if($request->email_verified_at == 0)
        {
            $user->email_verified_at = \Carbon\Carbon::now();
            $user->save();
        }
        else if($request->email_verified_at == 1)
        {
            $user->email_verified_at = null;
            $user->save();
        }
        
        $user->syncRoles($request->role_id);
        
        return redirect(route('admin.user.index'))->with('message', 'تم حفظ المعلومات بنجاح!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        
        return redirect(route('admin.user.index'))->with('message', 'تم حذف المعلومات بنجاح!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function orders(User $user)
    {
        return view('admin.user.order', compact('user'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function addresses(User $user)
    {
        return view('admin.user.address', compact('user'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function shipments(User $user)
    {
        return view('admin.user.shipment', compact('user'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function balances(User $user)
    {
        // قيمة بضاعة لم تسلم
        $pendingOrders = \App\Models\Shipment::whereHas('order', function($query) use ($user){
            return $query->whereIn('order_status_id', [1,2])->where('user_id', $user->id);
        })->get()->sum('item_cost');

        // مبالغ في انتظار التحويل
        $pendingTransfer = \App\Models\Balance::where('user_id', $user->id)->first()->balance;

        return view('admin.user.balance', compact('user', 'pendingOrders', 'pendingTransfer'));
    }
}
