<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Package;

class RedirectIfPackageIsNull
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->getRequestUri() != '/package' 
        && ! auth()->user()->hasRole('admin') && auth()->user()->policies == 0
        ||
        $request->getRequestUri() != '/package' 
        && ! auth()->user()->hasRole('admin') && is_null(auth()->user()->package_id))
        {
            $packages = Package::get();
            return response()->view('app.package.index', compact('packages'));
        }

        return $next($request);
    }
}
