<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Schema::defaultStringLength(191);

        $countries = \Cache::rememberForever('countries', function () {
            return \App\Models\Country::pluck('name', 'id');
        });

        $appSetting = \Cache::remember('setting', 1440, function () {
            return \App\Models\AppSetting::first();
        });

        \View::share('countries', $countries);
        \View::share('setting', $appSetting);

        \App\Models\Transfer::observe(\App\Observers\TransferObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
