<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailReport extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * This contains realted collectins ['shipment', 'transfers']
     *
     * @return collect
     */
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('email.report')->subject('التقارير - ' .config('app.name'))->with('data', $this->data);
    }
}
