<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Shipment extends Model
{
    use SoftDeletes;

    protected $with = ['provider_schedule', 'status', 'services'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = Carbon::parse($value)->format('d/m/Y');
    }

    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('Y-m-d');
    }

    // public function getDateAttribute($value)
    // {
    //     return \Carbon\Carbon::parse($value)->format('Y-m-d');
    // }
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    // get the last status onl to show as main one.
    public function status()
    {
        return $this->hasOne('App\Models\ShipmentStatus')->latest();
    }

    public function statuses()
    {
        return $this->hasMany('App\Models\ShipmentStatus');
    }

    public function order()
    {
        return $this->belongsTo('App\Models\Order', 'order_id');
    }

    public function money_receiver()
    {
        return $this->belongsTo('App\Models\ShipmentMoneyReceiver', 'shipment_money_receiver_id');
    }

    public function provider_schedule()
    {
        return $this->belongsTo('App\Models\ShipmentProviderSchedule', 'shipment_provider_schedule_id');
    }

    public function receiver()
    {
        return $this->belongsTo('App\Models\Address', 'receiver_address');
    }

    public function sender()
    {
        return $this->belongsTo('App\Models\Address', 'sender_address');
    }

    public function services()
    {
        return $this->belongsToMany('App\Models\ShipmentService', 'shipments_services', 'shipment_id', 'shipment_services_id');
    }

    public function method()
    {
        return $this->belongsTo('App\Models\ShipmentMethod', 'shipment_method_id');
    }

    public function getTotalPrice()
    {
        $total = 0;
        $totalWithoutItemCost = 0;

        foreach($this->services as $service)
        {
            $total += $service->cost;
            $totalWithoutItemCost += $service->cost;
        }

        if(! is_null($this->item_cost) && $this->shipment_method_id == 1)
        {
            $total += $this->item_cost;
            $total += \App\Models\AppSetting::first()->delivery_cost;
            return $total;

        }
        else if(is_null($this->item_cost) && $this->shipment_method_id == 2)
        {
            $totalWithoutItemCost += \App\Models\AppSetting::first()->delivery_cost;
            return $totalWithoutItemCost;
        }
    }
}
