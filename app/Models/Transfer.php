<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    protected $guarded = [];

    /**
     * The event map for the model.
     *
     * @var array
     */
    // protected $dispatchesEvents = [
    //     'created' => UserSaved::class,
    //     'updates' => UserDeleted::class,
    // ];

    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('Y-m-d');
    }
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function bank()
    {
        return $this->belongsTo('App\Models\Bank');
    }

    public function type()
    {
        return $this->belongsTo('App\Models\TransferType', 'transfer_type_id');
    }

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

    // public function activate()
    // {
    //     // dd($this->attributes['activate']);
    //     // if(isset($this->activate))
    //     // {
    //     //     if($this->activate == 1)
    //     //     {
    //     //         return 'مفعل';
    //     //     }
    //     //     else
    //     //     {
    //     //         return 'غير مفعل';
    //     //     }
    //     // }
    // }
}
