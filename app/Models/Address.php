<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $guarded = [];

    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    public function type()
    {
        return $this->belongsTo('App\Models\AddressType', 'address_type_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\user', 'user_id');
    }
}
