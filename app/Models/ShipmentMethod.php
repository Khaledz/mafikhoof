<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShipmentMethod extends Model
{
    protected $guarded = [];

    protected $table = 'shipment_methods';
}
