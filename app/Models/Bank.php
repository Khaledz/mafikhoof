<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $guarded = [];

    public function type()
    {
        return $this->belongsTo('App\Models\BankType', 'bank_type_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
