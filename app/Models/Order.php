<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function shipment()
    {
        return $this->hasOne('App\Models\Shipment');
    }

    public function status()
    {
        return $this->belongsTo('App\Models\OrderStatus', 'order_status_id');
    }

    public function transfer()
    {
        return $this->hasOne('App\Models\Transfer');
    }
}
