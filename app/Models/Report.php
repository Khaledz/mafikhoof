<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Report extends Model
{
    protected $guarded = [];

    public function setDateFromAttribute($value)
    {
        $this->attributes['date_from'] = Carbon::parse($value)->format('d/m/Y');
    }
    public function setDateToAttribute($value)
    {
        $this->attributes['date_to'] = Carbon::parse($value)->format('d/m/Y');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function functions()
    {
        return $this->belongsToMany('App\Models\ReportFunction', 'reports_functions', 'report_id', 'function_id');
    }
}
