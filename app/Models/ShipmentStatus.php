<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShipmentStatus extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $guarded = [];

    // public function statuses()
    // {
    //     return $this->hasMany('App\Models\ShipmentStatusExtra', 'shipment_status_id');
    // }

    public function shipment()
    {
        return $this->belongsTo('App\Models\shipment');
    }
}
