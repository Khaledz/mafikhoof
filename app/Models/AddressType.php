<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AddressType extends Model
{
    protected $guarded = [];

    protected $table = 'address_types';
}
