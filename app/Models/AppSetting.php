<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppSetting extends Model
{
    protected $guarded = [];

    protected $table = 'app_settings';


    // delivery_cost
    public function getDeliveryCostAttribute($value)
    {
        $delivery_cost = $this->attributes['delivery_cost'];

        // if user has coupon
        if(auth()->user()->coupons()->count() > 0)
        {
            $decimal_percentage = $delivery_cost / 100;
            $discount = $delivery_cost * $decimal_percentage;
            return  $this->attributes['delivery_cost'] - (int)$discount;
        }

        return $this->attributes['delivery_cost'];
    }

    public function getDeliveryCostForUser($user)
    {
        $delivery_cost = $this->attributes['delivery_cost'];

        // if user has coupon
        if($user->coupons()->count() > 0)
        {
            $decimal_percentage = $delivery_cost / 100;
            $discount = $delivery_cost * $decimal_percentage;
            return  $this->attributes['delivery_cost'] - (int)$discount;
        }
        return $this->attributes['delivery_cost'];
    }
}
