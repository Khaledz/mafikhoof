<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $guarded = [];

    public function slider()
    {
        if(! is_null($this->slider))
        {
            return asset('storage/'. $this->slider);
        }
    }
}
