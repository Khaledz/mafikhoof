<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransferType extends Model
{
    protected $guarded = [];

    public function type()
    {
        return $this->hasMany('App\Models\Transfer');
    }
}
