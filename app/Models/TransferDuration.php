<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransferDuration extends Model
{
    protected $guarded = [];

    protected $table = 'transfer_durations';

    public function users()
    {
        return $this->hasMany('App\Models\User');
    }
}
