<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class ShipmentProviderSchedule extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $guarded = [];

    public $timestamps = false;

    public function setFromAttribute($value)
    {
        $this->attributes['from'] = Carbon::parse($value)->format('H:i');
    }

    public function setToAttribute($value)
    {
        $this->attributes['to'] = Carbon::parse($value)->format('H:i');
    }

    public function getFromAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('H:i');
    }

    public function getToAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('H:i');
    }

    public function provider()
    {
        return $this->belongsTo('App\Models\ShipmentProvider', 'shipment_provider_id');
    }

    public function enable()
    {
        return $this->enable == 1 ? 'مفعل' : 'غير مفعل';
    }

    public function days()
    {
        return $this->belongsToMany('App\Models\Day', 'schedules_days', 'shipment_provider_schedule_id', 'day_id')->withPivot([
            'day_id',
        ]);
    }
}
