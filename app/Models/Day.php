<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Day extends Model
{
    protected $guarded = [];

    public function schedules()
    {
        return $this->belongsToMany('App\Models\ShipmentProviderSchedule', 'schedules_days', 'day_id', 'shipment_provider_schedule_id')->withPivot([
            'day_id',
        ]);
    }
}
