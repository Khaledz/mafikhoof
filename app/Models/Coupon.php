<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Coupon extends Model
{
    protected $guarded = [];
    
    public function setExpireOnAttribute($value)
    {
        $this->attributes['expire_on'] = \Carbon\Carbon::createFromFormat('d/m/Y', $value)->toDateString();
    }

    public function getExpireOnAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y');
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'users_coupons', 'coupon_id', 'user_id');
    }
}
