<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportFunction extends Model
{
    protected $guarded = [];

    public function reports()
    {
        return $this->belongsToMany('App\Models\Report', 'reports_functions', 'report_id', 'function_id');
    }
}
