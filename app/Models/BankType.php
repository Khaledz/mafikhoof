<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankType extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    public function banks()
    {
        return $this->hasMany('App\Models\BankType');
    }
}
