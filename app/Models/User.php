<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Collection;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['policies','avatar','company_name', 'name', 'email', 'password', 'package_id', 'transfer_duration_id', 'phone', 'website', 'address', 'city'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $with = ['roles'];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'name';
    }

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new \App\Notifications\VerifyEmail);
    }

    public function avatar()
    {
        if(is_null($this->avatar))
        {
            return asset('storage/no-avatar.png');
        }
        else
        {
            return asset('storage/'. $this->avatar);
        }
    }

    public function getRoleNames(): Collection
    {
        return $this->roles->pluck('display_name');
    }

    public function package()
    {
        return $this->belongsTo('App\Models\Package');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

    public function addresses()
    {
        return $this->hasMany('App\Models\Address');
    }

    public function balance()
    {
        return $this->hasOne('App\Models\Balance');
    }

    public function status()
    {
        return $this->email_verified_at != null ? 'مفعل' : 'غير مفعل';
    }

    public function duration()
    {
        return $this->belongsTo('App\Models\TransferDuration', 'transfer_duration_id');
    }

    public function coupons()
    {
        return $this->belongsToMany('App\Models\Coupon', 'users_coupons', 'user_id', 'coupon_id');
    }
}
