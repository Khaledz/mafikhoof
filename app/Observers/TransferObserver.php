<?php

namespace App\Observers;

use App\Models\Transfer;

class TransferObserver
{
    /**
     * Handle the transfer "created" event.
     *
     * @param  \App\Transfer  $transfer
     * @return void
     */
    public function created(Transfer $transfer)
    {
        $oldBalance = \App\Models\Balance::where('user_id', $transfer->user_id)->first();
        if($transfer->transfer_type_id == 1 && $transfer->activate != 0)
        {
            $oldBalance->update(['balance' => $oldBalance->balance - $transfer->amount]);
        }
        else if($transfer->transfer_type_id == 2 && $transfer->activate != 0)
        {
            $oldBalance->update(['balance' => $oldBalance->balance + $transfer->amount]);
        }
    }

    /**
     * Handle the transfer "updated" event.
     *
     * @param  \App\Transfer  $transfer
     * @return void
     */
    public function updated(Transfer $transfer)
    {
        $oldBalance = \App\Models\Balance::where('user_id', $transfer->user_id)->first();
        if($transfer->transfer_type_id == 1)
        {
            $oldBalance->update(['balance' => $oldBalance->balance - $transfer->amount]);
        }
        else if($transfer->transfer_type_id == 2)
        {
            $oldBalance->update(['balance' => $oldBalance->balance + $transfer->amount]);
        }
    }

    /**
     * Handle the transfer "deleted" event.
     *
     * @param  \App\Transfer  $transfer
     * @return void
     */
    public function deleted(Transfer $transfer)
    {
        //
    }

    /**
     * Handle the transfer "restored" event.
     *
     * @param  \App\Transfer  $transfer
     * @return void
     */
    public function restored(Transfer $transfer)
    {
        //
    }

    /**
     * Handle the transfer "force deleted" event.
     *
     * @param  \App\Transfer  $transfer
     * @return void
     */
    public function forceDeleted(Transfer $transfer)
    {
        //
    }
}
