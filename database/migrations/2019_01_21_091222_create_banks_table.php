<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
        });

        Schema::create('banks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_type_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('holder_name');
            $table->string('account_number');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('bank_type_id')->references('id')->on('bank_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banks');
        Schema::dropIfExists('bank_types');
    }
}
