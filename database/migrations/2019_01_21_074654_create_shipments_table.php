<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('days', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->string('name_en')->index();
        });

        // this table to save who is going to receive the money of the item (sender, receiver, added to my balanace)
        Schema::create('shipment_money_receivers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
        });        

        Schema::create('shipment_providers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->softDeletes();
        });

        Schema::create('shipment_provider_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('from');
            $table->string('to');
            $table->integer('delivery_cost')->nullable();
            $table->integer('shipment_cost')->nullable();
            $table->boolean('enable')->default(true);
            $table->integer('shipment_provider_id')->unsigned();
            $table->softDeletes();

            $table->foreign('shipment_provider_id')->references('id')->on('shipment_providers')->onDelete('cascade');
        });

        Schema::create('schedules_days', function (Blueprint $table) {
            $table->integer('shipment_provider_schedule_id')->unsigned();
            $table->integer('day_id')->unsigned();
            $table->softDeletes();

            $table->foreign('day_id')->references('id')->on('days')->onDelete('cascade');
            $table->foreign('shipment_provider_schedule_id')->references('id')->on('shipment_provider_schedules')->onDelete('cascade');
        });

        Schema::create('shipment_services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->integer('cost');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('shipment_methods', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->timestamps();
        });

        Schema::create('shipments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tracking_number')->index();
            $table->integer('order_id')->unsigned();
            $table->integer('sender_address')->unsigned();
            $table->integer('receiver_address')->unsigned();
            $table->integer('item_cost')->nullable();
            $table->string('date');
            $table->integer('shipment_money_receiver_id')->unsigned();
            $table->integer('shipment_provider_schedule_id')->unsigned();
            $table->integer('shipment_method_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('shipment_method_id')->references('id')->on('shipment_methods')->onDelete('cascade');
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->foreign('sender_address')->references('id')->on('addresses')->onDelete('cascade');
            $table->foreign('receiver_address')->references('id')->on('addresses')->onDelete('cascade');
            $table->foreign('shipment_money_receiver_id')->references('id')->on('shipment_money_receivers')->onDelete('cascade');
            $table->foreign('shipment_provider_schedule_id')->references('id')->on('shipment_provider_schedules')->onDelete('cascade');
        });

        Schema::create('shipments_services', function (Blueprint $table) {
            $table->integer('shipment_id')->unsigned();
            $table->integer('shipment_services_id')->unsigned();
            $table->softDeletes();

            $table->foreign('shipment_id')->references('id')->on('shipments')->onDelete('cascade');
            $table->foreign('shipment_services_id')->references('id')->on('shipment_services')->onDelete('cascade');
        });

        Schema::create('shipment_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->longText('description')->nullable();
            $table->integer('user_id')->nullable()->unsigned(); // this can be added by admin.
            $table->integer('shipment_id')->unsigned();
            $table->foreign('shipment_id')->references('id')->on('shipments')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });

        // this table will hold any additional information of the parent status
        // Schema::create('shipment_status_extra', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->string('name')->index();
        //     $table->longText('description')->nullable();
        //     $table->integer('user_id')->nullable()->unsigned(); // this can be added by admin.
        //     $table->integer('shipment_status_id')->unsigned();

        //     $table->foreign('shipment_status_id')->references('id')->on('shipment_statuses')->onDelete('cascade');
        //     $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipments');
        Schema::dropIfExists('shipment_statuses');
        Schema::dropIfExists('money_receivers');
        Schema::dropIfExists('shipment_providers');
    }
}
