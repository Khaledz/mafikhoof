<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->string('code_name')->index();
        });

        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id')->unsigned();
            $table->integer('address_type_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('name');
            $table->string('city');
            $table->string('area');
            $table->string('phone');
            $table->string('phone2')->nullable();
            $table->string('building_number')->nullable();
            $table->string('street')->nullable();
            $table->string('postcode')->nullable();
            $table->timestamps();

            $table->foreign('address_type_id')->references('id')->on('address_types')->onDelete('cascade');
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
        Schema::dropIfExists('address_types');
    }
}
