<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call('CountriesSeeder');
        $this->command->info('Seeded the countries!');
        $this->call('SetupSeeder');
        factory(App\Models\User::class, 10)->create();
        foreach(\DB::table('users')->get() as $user)
        {
            \DB::table('balances')->insert([
                'user_id' => $user->id,
            ]);
        }
    }
}
