<?php

use Illuminate\Database\Seeder;

class SetupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('transfer_durations')->insert([
            'name' => 'خلال يومين عمل',
        ]);

        \DB::table('transfer_durations')->insert([
            'name' => 'كل أسبوع',
        ]);

        \DB::table('transfer_durations')->insert([
            'name' => 'مرتين في الشهر',
        ]);

        \DB::table('transfer_durations')->insert([
            'name' => 'كل شهر',
        ]);

        \DB::table('packages')->insert([
            'name' => 'بوليصة شحن',
            'policy_number' => 35
        ]);

        \DB::table('roles')->insert([
            'name' => 'admin',
            'display_name' => 'مدير',
            'guard_name' => 'web'
        ]);

        \DB::table('roles')->insert([
            'display_name' => 'تاجر',
            'name' => 'trader',
            'guard_name' => 'web'
        ]);

        \DB::table('roles')->insert([
            'display_name' => 'عميل',
            'name' => 'customer',
            'guard_name' => 'web'
        ]);

        \DB::table('address_types')->insert([
            'name' => 'عنوان رئيسي',
            'code_name' => 'main'
        ]);

        \DB::table('address_types')->insert([
            'name' => 'عنوان مرسل',
            'code_name' => 'sender'
        ]);

        \DB::table('address_types')->insert([
            'name' => 'عنوان مستلم',
            'code_name' => 'receiver'
        ]);

        \DB::table('users')->insert([
            'name' => 'Khaled Bawazir',
            'email' => 'devkhaledz@gmail.com',
            'password' => bcrypt('123456'),
            'email_verified_at' => \Carbon\Carbon::now(),
        ]);

        \DB::table('users')->insert([
            'name' => 'Naif Osman',
            'email' => 'naif@gmail.com',
            'password' => bcrypt('123456'),
            'email_verified_at' => \Carbon\Carbon::now(),
        ]);

        \DB::table('bank_types')->insert([
            'name' => 'بنك الراجحي'
        ]);

        \DB::table('bank_types')->insert([
            'name' => 'بنك اﻷهلي'
        ]);
        
        \DB::table('shipment_money_receivers')->insert([
            'name' => 'المرسل'
        ]);
        
        \DB::table('shipment_money_receivers')->insert([
            'name' => 'المستلم'
        ]);

        \DB::table('shipment_money_receivers')->insert([
            'name' => 'على الحساب / يخصم لاحقاً من رصيدي'
        ]);
        
        \DB::table('order_statuses')->insert([
            'name' => 'تم إستلام الطلب',
            'status_order' => 1
        ]);

        \DB::table('order_statuses')->insert([
            'name' => 'في الشحن',
            'status_order' => 2,
            'shipment_status' => true
        ]);

        \DB::table('order_statuses')->insert([
            'name' => 'تم تنفيذ الطلب',
            'status_order' => 4,
        ]);

        \DB::table('order_statuses')->insert([
            'name' => 'تم الغاء الطلب',
            'status_order' => 5,
        ]);

        
        \DB::table('shipment_providers')->insert([
            'name' => 'أرامكس'
        ]);

        \DB::table('addresses')->insert([
            'country_id' => 4,
            'address_type_id' => '2',
            'city' => 'جدة',  
            'area' => 'حي مدائن الفهد', 
            'user_id' => 1,
            'name' => 'خالد باوزير',
            'phone' => 123456
        ]);

        \DB::table('addresses')->insert([
            'country_id' => 4,
            'address_type_id' => '3',
            'city' => 'جدة',  
            'area' => 'حي البوادي',   
            'user_id' => 2 ,
            'name' => 'نايف عثمان',
            'phone' => 123456
        ]);
        
        $days_ar = ['اﻹثنين','الثلاثاء','اﻷربعاء','الخميس','الجمعة', 'السبت','اﻷحد'];
        $days_en = ['monday','tuesday','wednesday','thursday','friday', 'saturday','sunday'];
        for($i = 0; $i < count($days_ar); $i++)
        {
            \DB::table('days')->insert([
                'name' => $days_ar[$i],
                'name_en' => $days_en[$i],
            ]);
        }


        // racking_number	order_id	sender_address	receiver_address	item_cost
        //shipment_money_receiver_id	shipment_provider_schedule_id	shipment_status_detail_id

        \DB::table('report_functions')->insert([
            'name' => 'تقرير الشحنات',
        ]);

        \DB::table('report_functions')->insert([
            'name' => ' تقرير الشحنات المفوترة',
        ]);

        \DB::table('report_functions')->insert([
            'name' => 'تقرير حركات الرصيد',
        ]);

        \DB::table('report_functions')->insert([
            'name' => 'تقرير الحوالات',
        ]);

        \DB::table('shipment_services')->insert([
            'name' => 'تغليف الشحنة داخل كرتون',
            'cost' => 5
        ]);

        \DB::table('shipment_services')->insert([
            'name' => 'إضافة ملصق قابل للكسر',
            'cost' => 0
        ]);
        
        \DB::table('shipment_methods')->insert([
            'name' => 'توصيل + تحصيل مبلغ البضاعة',
        ]);
        
        \DB::table('shipment_methods')->insert([
            'name' => 'توصيل',
        ]);


        
        \DB::table('app_settings')->insert([
            'delivery_cost' => 35,
            'shipment_cost' => 50,
            'phone' => '123456',
            'email' => 'admin@test.com'
        ]);
        
        \DB::table('transfer_types')->insert([
            'name' => 'سحب رصيد',
        ]);

        \DB::table('transfer_types')->insert([
            'name' => 'إضافة رصيد',
        ]);

        
        \App\Models\User::first()->assignRole(\Spatie\Permission\Models\Role::first());
        \App\Models\User::find(2)->assignRole(\Spatie\Permission\Models\Role::first());
    }
}
