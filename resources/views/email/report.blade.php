<div style="dir: rtl">
@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
           التقارير - {{ config('app.name') }}
        @endcomponent
    @endslot
{{-- Body --}}
#التقارير المطلوبة من تاريخ {{ $data->report->date_from }} إلى {{ $data->report->date_to }} 
<br>
التقارير التي طلبتها كالتالي:
<ul>
@foreach($data->report->functions as $function)
<li>{{ $function->name }}</li>
@endforeach
</ul>
<br>
@if(isset($data->shipments))
#تقرير الشحنات
@component('mail::table')
|   رقم التتبع   |    التاريخ    |   الحالة   |  المبلغ
|: ------------- |:-------------:|  |:-------------:| --------:|
@if(count($data->shipments) > 0)
@foreach($data->shipments as $shipment)
| {{$shipment->tracking_number}}     | {{ $shipment->date }}     | {{ $shipment->status->name }} | | {{ $shipment->getTotalPrice() }} ريال سعودي
@endforeach
@else
لا يوجد نتائج
@endif
@endcomponent
@endif
@if(isset($data->billed))
#تقرير الشحنات المفوترة
@component('mail::table')
|   رقم التتبع   |    التاريخ    |   الحالة   |  المبلغ
|: ------------- |:-------------:|  |:-------------:| --------:|
@if(count($data->billed) > 0)
@foreach($data->billed as $shipment)
| {{$shipment->tracking_number}}     | {{ $shipment->date }}     | {{ $shipment->status->name }} |  {{ $shipment->getTotalPrice()  }} ريال سعودي
@endforeach
@else
لا يوجد نتائج
@endif
@endcomponent
@endif
@if(isset($data->sumItemsNotDeliver) && isset($data->sumLastTwoTransfers))
#تقرير حركات الرصيد
@component('mail::table')
|   قيمة بضاعة لم تسلم   |    آخر حوالتين بنكية    
|:-------------:| --------:|
| {{$data->sumItemsNotDeliver}} ريال سعودي | {{ $data->sumLastTwoTransfers }}   ريال سعودي
@endcomponent
@endif
@if(isset($data->transfers))
#تقرير الحوالات
@component('mail::table')
|   نوع الحوالة    | المبلغ | التاريخ
|: ------------- |:-------------:|  |:-------------:| --------:|
@if(count($data->transfers) > 0)
@foreach($data->transfers as $transfer)
| {{$transfer->type->name }}    | {{ $transfer->amount }} ريال سعودي    | {{ $transfer->amount }} ريال سعودي| | {{ $transfer->created_at }}
@endforeach
@else
ﻻ يوجد نتائج
@endif 
@endcomponent
@endif

{{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset
{{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} {{ config('app.name') }}.
        @endcomponent
    @endslot
@endcomponent