<!DOCTYPE html>
<html lang="en"  direction="rtl" style="direction: rtl;" >

	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>@yield('title') - {{ config('app.name') }}</title>
		<meta name="description" content="Latest updates and statistic charts"> 
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
		<meta name="csrf-token" content="{{ csrf_token() }}">
				
		<!--begin::Global Theme Styles -->
		<link href="/assets/admin/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
		<link href="/assets/admin/vendors/base/vendors.bundle.rtl.css" rel="stylesheet" type="text/css" />
		<link href="/assets/admin/demo/default/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />
		<!--end::Global Theme Styles -->
		<link rel="shortcut icon" href="/assets/admin/demo/default/media/img/logo/favicon.ico" />
		<link href="https://fonts.googleapis.com/css?family=Cairo" rel="stylesheet">
		<link href="/assets/admin/custom.css" rel="stylesheet" type="text/css" />
	</head>
	<!-- end::Head -->

	<!-- begin::Body -->
    <body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
        <!-- begin::Page loader -->
        <div class="m-page-loader m-page-loader--base">
            <div class="m-blockui">
                <span>جاري التحميل ...</span>

                <span>
                    <div class="m-loader m-loader--brand"></div>
                </span>
            </div>
        </div>
        <!-- end::Page Loader -->

		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
            @include('partials.admin.header')
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
                @include('partials.admin.sidebar')
                <div class="m-grid__item m-grid__item--fluid m-wrapper">
                    @include('partials.admin.breadcrumbs')
                    <div class="m-content">
						@include('partials.admin.messages')
                        @yield('content')
                    </div>
                </div>
			</div>
			@include('partials.admin.footer')
        </div>

        <!-- begin::Scroll Top -->
		<div id="m_scroll_top" class="m-scroll-top">
			<i class="la la-arrow-up"></i>
		</div>
		<!-- end::Scroll Top -->

		
		<!--begin::Global Theme Bundle -->
		<!-- <script src="/js/app.js" type="text/javascript"></script> -->
		<script src="/assets/admin/vendors/base/vendors.bundle.js" type="text/javascript"></script>
		<script src="/assets/admin/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
		<script src="/assets/admin/app/js/dashboard.js" type="text/javascript"></script>
		<script src="/assets/admin/demo/default/custom/crud/metronic-datatable/base/html-table.js" type="text/javascript"></script>
		<script src="/assets/admin/demo/default/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
		<script src="/assets/admin/demo/default/custom/components/base/sweetalert2.js" type="text/javascript"></script>
		@yield('script')
		<!-- begin::Page Loader -->
		<script>
            $(window).on('load', function() {
                $('body').removeClass('m-page--loading');         
            });
        </script>
        <!-- end::Page Loader -->
    </body>
</html>