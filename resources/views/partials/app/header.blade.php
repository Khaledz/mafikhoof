<!--header-->
<header class="{{ \Request::path() == '/' ? 'home' : 'nothome' }}">
    <div class="{{ \Request::path() == '/' ? 'container2' : 'container' }}">
        <div class="uperbar row {{ \Request::path() == '/' ? '' : 'paddingbottom20' }}">
            <div class="col-3 rtl text-center">
                <a href="{{ url('/')}}">مافي خوف</a>
            </div>
            <div class="col-lg-3 rtl text-center hidein991">
                <i class="far fa-envelope color1 margin10 "></i>
                <span>support@domain.com</span>
            </div>
            <div class="col-lg-3 rtl text-center hidein991 ">
                <i class="far fa-clock color1 margin10"></i>
                    <span ><span class="numberfont ">08</span> صباحا - <span class="numberfont ">10</span> مساء</span>
            </div>
            <div class="col-lg-3 rtl  row text-center hidein991 ">
                <span  >
                    <p class="color1 ">الخط الساخن</p>
                    <p class="numberfont " >00123456789</p>
                </span>
                <span  >
                    <i class="fas fa-phone color1  "></i>
                </span>

            </div> 
        </div>
    </div>

      <nav class="navbar navbar-expand-lg navbar-light {{ \Request::path() == '/' ? '' : 'container' }}" >
          <button class="navbar-toggler "  data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <i class="fas fa-bars color1"></i>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav " >
                  @if(! auth::check())
                     <li class="nav-item ">
                           <a class="nav-link" href="{{ url('/register') }}">تسجيل جديد</a>
                     </li>
                     <li class="nav-item">
                         <a class="nav-link " href="{{ url('/login') }}">تسجيل الدخول</a>
                     </li>
                     @else

                     <li class="nav-item dropdown">
                     <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        ملفي الشخصي
                     </a>
                     <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        @if(auth()->user()->hasRole('admin'))
                        <a href="{{ route('admin.dashboard.index') }}"class="dropdown-item">
                            عرض لوحة تحكم اﻹدارة
                        </a>    
                        @endif
                        <a href="{{ route('app.user.show', auth()->user()) }}" class="dropdown-item">
                            عرض الملف الشخصي
                        </a>
                        <a href="{{ route('app.user.setting') }}" class="dropdown-item">
                            اعدادات الحساب
                        </a>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();" class="dropdown-item">
                            تسجيل خروج
                        </a>    
                        <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                     </li>
                     @endif

                     <li class="nav-item ">
                          <a class="nav-link" href="{{ route('app.home.index') }}">الرئيسية</a>
                     </li>
                     <li class="nav-item">
                           <a class="nav-link" href="{{ route('app.shipment.track') }}">تتبع الشحنة</a>
                     </li>
                     <li class="nav-item">
                          <a class="nav-link " href="{{ url('/tickets') }}">الدعم الفنى</a>
                     </li>
                     <li class="nav-item">
                          <a class="nav-link " href="{{ route('app.home.contact') }}">تواصل معنا</a>
                     </li>
                     <li class="nav-item is-active">
                          <a class="nav-link " href="{{ route('app.order.create') }}">طلب جديد</a>
                     </li>
                     <li class="nav-item contact margintopincoll ">
                         <i class="far fa-envelope color1  "></i>
                         <span>{{ $setting->email }}</span>
                    </li>
                    <li class="nav-item contact margintopincoll ">
                        <i class="far fa-clock color1 "></i>
                        <span ><span class="numberfont ">08</span> صباحا - <span class="numberfont ">10</span> مساء</span>
                    </li>
                    <li class="nav-item contact margintopincoll ">
                      <span  >
                          <p class="color1 ">الخط الساخن</p>
                          <p  class="numberfont ">{{ $setting->phone }}</p>
                      </span>
                      <span  >
                          <i class="fas fa-phone color1  "></i>
                      </span>
                    </li>
                </ul>
          </div>
          
      </nav>
  
</header>

