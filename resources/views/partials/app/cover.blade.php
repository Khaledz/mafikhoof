<!--front-->
<div class="front">
    <div class="container text-center color2">
        <!-- <h1>@yield('title')</h1> -->
        <div class="color3 text-left font-weight-bold">
            @if(auth()->guest())
            <span><a href="{{ url('/') }}" style="color:white;">الرئيسية</a></span>
            @elseif(\Request::path() == 'contact-us' || \Request::path() == 'shipment-track')
            <span><a href="{{ url('/') }}" style="color:white;">الرئيسية</a></span>
            @else
            <span><a href="{{ route('app.user.show', auth()->user()) }}" style="color:white;">حسابي</a></span>
            @endif
            <i class="fas fa-angle-left"></i>
            <span>@yield('title')</span>
        </div>
    </div>
</div>
