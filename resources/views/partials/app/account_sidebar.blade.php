<a href="{{ route('app.notification.index') }}"><div class="col-md-3">
<div class="notification row font-weight-bold">
        <i class="fas fa-bell col-2 color2"></i>
        <span class="col-8 color2">التنبيهات</span>
        <div class="col-2  color3 rounded-circle">
            <div class="background2 "><span class="numberfont">{{ auth()->user()->unreadNotifications->count() }}</span></div></div>
            
    </div></a>
    <div class="accontsett text-center">
        <img src="{{ auth()->user()->avatar() }}" class="rounded-circle">
        @if(auth()->check())
        <p class="font-weight-bold color2">{{ auth()->user()->name }}</p>
        @endif
        <div class=" text-center font-weight-bold color2">
            <i class="fas fa-cog"></i>
            <span><a href="{{ route('app.user.setting') }}">اعدادات الحساب</a></span>
        </div>
    </div>
    <div class="pannel font-weight-bold color2">
        <div >
            <i class="fas fa-bars"></i>
            <span>لوحة التحكم</span>
        </div>
        <div >
            <a href="{{ route('app.user.show', auth()->user()) }}" class="color2 now2">
                <i class="fas fa-user-alt rounded-circle"></i>
                <span>حسابى</span>
            </a>
        </div>
        <div >
                <a href="{{ route('app.home.contact') }}" class="color2">
                    <i class="fas fa-truck rounded-circle"></i>
                    <span>اتصل بنا</span>
                </a>
        </div>
        <div >
                <a href="{{ route('app.shipment.track') }}" class="color2">
                    <i class="fas fa-search rounded-circle"></i>
                    <span>تتبع برقم الشحنة</span>
                </a>
        </div>
        <div >
                <a href="{{ url('/tickets') }}" class="color2">
                    <i class="fas fa-comments rounded-circle"></i>
                    <span>الدعم الفني</span>
                </a>
        </div>
        <div >
        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();"class="color2">
        <i class="fas fa-power-off rounded-circle"></i>
                    <span>تسجيل الخروج</span>
        </a>    
        <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
        </div>
        
        
    </div>
</div>