<br>
<!--footer-->
<footer >
    <div class="container">
        <div class="row container_of_footer">
            <div class="col-md-2 "><a href="/">الرئيسة</a></div>
            <div class="col-md-2 "><a href="{{ route('app.shipment.track') }}">تتبع الشحنه</a></div>
            <div class="col-md-2 "><a href="{{ url('/tickets') }}">الدعم الفنى</a></div>
            <div class="col-md-2 "><a href="{{ route('app.home.about') }}">عن الموقع</a></div>
            <div class="col-md-2 "><a href="{{ route('app.home.privacy') }}">سياسه الخصوصيه</a></div>
            <div class="col-md-2 "><a href="{{ route('app.home.term') }}">الشروط والاحكام</a></div>
        </div>
        <div class="scroll">
                <i class="fa fa-angle-up"></i>
            </div>
    </div>
</footer>

<!--lowerbar-->
<div class="lowerbar text-center">
    <p class="color3 ">جميع الحقوق محفوظه لموقع مافي خوف لخدمات الشحن {{ date('Y') }}</p>
</div>