<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item">
                    <a href="{{ route('admin.dashboard.index') }}" class="m-nav__link">
                        <span class="m-nav__link-text">لوحة التحكم</span>
                    </a>
                </li>
                @if(\Request::segment(2) != null && \Request::segment(3) == null)
                <li class="m-nav__separator">></li>
                <li class="m-nav__item">
                    <a href="{{ route('admin.'.Request::segment(2).'.index') }}" class="m-nav__link">
                        <span class="m-nav__link-text">{{ __('general.'.Request::segment(2)) }}</span>
                    </a>
                </li>
                @endif
                @if(\Request::segment(3) != null)
				<li class="m-nav__separator">></li>
                <li class="m-nav__item">
                    <a href="{{ route('admin.'.Request::segment(2).'.index') }}" class="m-nav__link">
                        <span class="m-nav__link-text">{{ __('general.'.Request::segment(2)) }}</span>
                    </a>
                </li>
                @endif
                <li class="m-nav__separator">></li>
                <li class="m-nav__item">
                    <a href="#" class="m-nav__link">
                        <span class="m-nav__link-text--active">@yield('title')</span>
                    </a>
                </li>
			</ul>
        </div>
        <!-- <div>
            <span class="m-subheader__daterange" id="m_dashboard_daterangepicker">
                <span class="m-subheader__daterange-label">
                    <span class="m-subheader__daterange-title"></span>
                    <span class="m-subheader__daterange-date m--font-brand"></span>
                </span>
                <a href="#" class="btn btn-sm btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                    <i class="la la-angle-down"></i>
                </a>
            </span>
        </div> -->
    </div>
</div>