<!-- BEGIN: Left Aside -->
<button class="m-aside-left-close m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
<div id="m_aside_left" class="m-grid__item m-aside-left m-aside-left--skin-dark ">
    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark m-aside-menu--dropdown " m-menu-vertical="1" m-menu-dropdown="1" m-menu-scrollable="0" m-menu-dropdown-timeout="500" style="position: relative;">
        <ul class="m-menu__nav m-menu__nav--dropdown-submenu-arrow ">
            <li class="m-menu__item {{ \Request::segment(2) == null ? 'm-menu__item--active' : '' }}" aria-haspopup="true" >
                <a href="{{ route('admin.dashboard.index') }}" class="m-menu__link ">
                    <i class="m-menu__link-icon fa fa-tachometer-alt"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">الصفحة الرئيسية</span> 
                        </span>
                    </span>
                </a>
            </li>
            <li class="m-menu__item m-menu__item--submenu {{ \Request::segment(2) == 'user' ? 'm-menu__item--active' : '' }}" aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon fa fa-users"></i><span class="m-menu__link-text">المستخدمين</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item " aria-haspopup="true" >
                            <a href="{{ route('admin.user.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">عرض المستخدمين</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="m-menu__item m-menu__item--submenu {{ \Request::segment(2) == 'order' ? 'm-menu__item--active' : '' }}" aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon fa fa-copy"></i><span class="m-menu__link-text">الطلبات</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item " aria-haspopup="true" >
                            <a href="{{ route('admin.order.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">عرض الطلبات</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="m-menu__item m-menu__item--submenu {{ \Request::segment(2) == 'shipment' ? 'm-menu__item--active' : '' }}" aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon fa fa-truck"></i><span class="m-menu__link-text">الشحن</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item " aria-haspopup="true" >
                            <a href="{{ route('admin.shipment.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">عرض طلبات الشحن</span>
                            </a>
                        </li>
                        <!-- <li class="m-menu__item " aria-haspopup="true" >
                            <a href="" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">حالات الشحن</span>
                            </a>
                        </li> -->
                        <li class="m-menu__item " aria-haspopup="true" >
                            <a href="{{ route('admin.shipment-schedule.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">مواعيد الشحن</span>
                            </a>
                        </li>
                        <li class="m-menu__item " aria-haspopup="true" >
                            <a href="{{ route('admin.shipment-provider.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">مزودي خدمات الشحن</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="m-menu__item m-menu__item--submenu {{ \Request::segment(2) == 'transfer' ? 'm-menu__item--active' : '' }}" aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon fa fa-exchange-alt"></i><span class="m-menu__link-text">الحوالات</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item " aria-haspopup="true" >
                            <a href="{{ route('admin.transfer.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">عرض الحوالات</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="m-menu__item m-menu__item--submenu {{ \Request::segment(2) == 'bank' ? 'm-menu__item--active' : '' }}" aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon fa fa-money-check-alt"></i><span class="m-menu__link-text">البنوك</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item " aria-haspopup="true" >
                            <a href="{{ route('admin.bank.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">عرض البنوك</span>
                            </a>
                        </li>
                        <li class="m-menu__item " aria-haspopup="true" >
                            <a href="{{ route('admin.bank-type.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">مزودي الخدمات البنكية</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="m-menu__item m-menu__item--submenu {{ \Request::segment(2) == 'coupon' ? 'm-menu__item--active' : '' }}" aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon fa fa-minus"></i><span class="m-menu__link-text">الكوبونات</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item " aria-haspopup="true" >
                            <a href="{{ route('admin.coupon.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">عرض الكوبونات</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="m-menu__item m-menu__item--submenu {{ \Request::segment(2) == 'package' ? 'm-menu__item--active' : '' }}" aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon fa fa-cubes"></i><span class="m-menu__link-text">الباقات</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item " aria-haspopup="true" >
                            <a href="{{ route('admin.package.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">عرض الباقات</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="m-menu__item m-menu__item--submenu {{ \Request::segment(2) == 'report' ? 'm-menu__item--active' : '' }}" aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon fa fa-file-alt"></i><span class="m-menu__link-text">التقارير</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item " aria-haspopup="true" >
                            <a href="{{ route('admin.report.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">عرض التقارير</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="m-menu__item m-menu__item--submenu {{ \Request::segment(2) == 'slider' ? 'm-menu__item--active' : '' }}" aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon fa fa-address-card"></i><span class="m-menu__link-text">السلايدات</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item " aria-haspopup="true" >
                            <a href="{{ route('admin.slider.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">عرض السلايدات</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="m-menu__item m-menu__item--submenu {{ \Request::segment(2) == 'app-setting' ? 'm-menu__item--active' : '' }}" aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon fa fa-file-alt"></i><span class="m-menu__link-text">إعدادات النظام</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item " aria-haspopup="true" >
                            <a href="{{ route('admin.app-setting.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">عرض اﻹعدادات</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
    <!-- END: Aside Menu -->
</div>
<!-- END: Left Aside -->