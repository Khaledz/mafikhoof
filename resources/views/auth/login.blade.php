@extends('layouts.app')
@section('title')
تسجيل الدخول
@endsection
@section('content')
<div class="vendor_register text-center">
    <h3 class="color2">لتسجيل الدخول يرجي ملئ الحقول التالية</h3>
    <hr class="hr">
    <div class="data_for_register">
        {!! Form::open(['url' => 'login', 'files' => true]) !!}
            <div class="text-center ">
                <div class="row">
                    <div class="col-md-5 text-center">
                        {!! Form::text('email', null, ['class' => 'form-control boxofinput', 'placeholder' => 'البريد اﻹلكتروني']) !!}
                    </div>
                    <div class="col-md-2 displynone771 "></div>
                    <div class="col-md-5 text-center">
                        {!! Form::password('password', ['class' => 'form-control boxofinput', 'placeholder' => 'كلمة المرور']) !!}
                    </div>

                </div>
                <button type="submit" class="form-control btn btn-primary my-1 buttonbox">تسجيل دخول</button>
            </div>
        </form>

    </div>
    <p>نسيت كلمة المرور؟ <a href="/password/reset">إضغط هنا</a></p>

</div>
@endsection