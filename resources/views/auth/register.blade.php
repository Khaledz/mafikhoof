@extends('layouts.app')
@section('title')
نوع العضوية
@endsection
@section('content')
<div class=" new_register text-center">
    <h3 class="color2">يرجى اختيار نوع العضوية</h3>
    <hr class="hr">
    <div class="row font-weight-bold color2">
        <div class="col-sm-6">
            <a href="/register/customer">
            <i class="fas fa-truck rounded-circle background2 color3"></i>
            <p>عميل</p>
            </a>
        </div>
        <div class="col-sm-6">
        <a href="/register/trader">
            <i class="fas fa-map-marker-alt rounded-circle background2 color3"></i>
            <p>تاجر</p>
        </a>
        </div>
    </div>
</div>
@endsection
