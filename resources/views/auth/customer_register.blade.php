@extends('layouts.app')
@section('title')
تسجيل عميل جديد
@endsection
@section('content')
<div class="vendor_register text-center">
    <h3 class="color2">لتسجيل عميل جديد يرجي ملئ الحقول التالية</h3>
    <hr class="hr">
    <div class="data_for_register">
        {!! Form::open(['url' => 'register/customer', 'files' => true]) !!}
            <div class="text-center ">
                <div class="row">
                    <div class="col-md-5 text-center">
                        {!! Form::text('name', null, ['class' => 'form-control boxofinput', 'placeholder' => 'اسم العميل']) !!}
                        {!! Form::text('city', null, ['class' => 'form-control boxofinput', 'placeholder' => 'المدينة']) !!}
                        {!! Form::text('phone', null, ['class' => 'form-control boxofinput', 'placeholder' => 'رقم الهاتف']) !!}
                        {!! Form::text('area', null, ['class' => 'form-control boxofinput', 'placeholder' => 'العنوان يدوياً']) !!}
                    </div>
                    <div class="col-md-2 displynone771 "></div>
                    <div class="col-md-5 text-center">
                        {!! Form::text('email', null, ['class' => 'form-control boxofinput', 'placeholder' => 'البريد اﻹلكتروني']) !!}
                        {!! Form::text('email_confirmation', null, ['class' => 'form-control boxofinput', 'placeholder' => 'تأكيد البريد اﻹلكتروني']) !!}
                        {!! Form::password('password', ['class' => 'form-control boxofinput', 'placeholder' => 'كلمة المرور']) !!}
                        {!! Form::password('password_confirmation', ['class' => 'form-control boxofinput', 'placeholder' => 'تأكيد كلمة المرور']) !!}
                    </div>
                    <div class="col-md-12 " style="background-color: white;border-radius: 0;
    border: #D93B6F solid 1px;">
    <div class="row"><div class="col-md-2"><label style="margin-top: 25px;">رفع صورة للحساب</label></div>
            <div class="col-md-10"><input type="file" name="avatar" class="form-control"> </div></div>

</div>
                    </div>
                </div>
                <button type="submit" class="form-control btn btn-primary my-1 buttonbox">تسجيل</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('script')

@endsection