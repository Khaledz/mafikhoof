@extends('layouts.app')
@section('title')
الحساب غير مفعل
@endsection
@section('content')
<div class=" new_register text-center">
    <h3 class="color2">حسابك غير مفعل.</h3>
    <hr class="hr">
    <div class="row font-weight-bold color2">
        <div class="col-sm-12">
        @if (session('resent'))
            <div class="alert alert-success" role="alert">
                تم إرسال رسالة التفعيل على بريدك اﻹلكتروني {{ auth()->user()->email }}
            </div>
        @endif
            <p>تم إرسال رسالة تفعيل الحساب على بريدك اﻹلكتروني، ﻹرسال رسالة تفعيل جديدة <a href="{{ route('verification.resend') }}">إضغط هنا</a></p>
        </div>
    </div>
</div>
@endsection
