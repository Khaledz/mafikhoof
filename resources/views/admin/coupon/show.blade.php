@extends('layouts.admin')
@section('title')
عرض الشحنة لرقم التتبع {{ $shipment->tracking_number }}
@endsection
@section('content')
<div class="m-portlet m-portlet--mobile m-portlet--body-progress-">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					حالة الشحن
				</h3>
			</div>			
		</div>
	</div>
	<div class="m-portlet__body">
		<table class="table m-table m-table--head-bg-brand">
			<thead class="thead-inverse">
				<tr>
					<th>#</th>
					<th>حالة الشحن</th>
				</tr>
			</thead>
			<tbody>
				@php $i = 1 @endphp
				@foreach($shipment->statuses as $status)
				<tr>
					<th>{{ $i++ }}</th>
					<td>
						<strong>{{ $status->name }}</strong>
						@if($status->statuses()->count() > 0)
						<div class="m-section__content">
							<div class="m-list-timeline">
								<div class="m-list-timeline__items">
									@foreach($status->statuses as $extraStatus)
										<div class="m-list-timeline__item">
											<span class="m-list-timeline__badge m-list-timeline__badge--success"></span>
											<span class="m-list-timeline__text">{{ $extraStatus->name }}</span>
											<span class="m-list-timeline__time">{{ $extraStatus->created_at }}</span>
										</div>
									@endforeach
								</div>
							</div>
						</div>
						@endif
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

@endsection