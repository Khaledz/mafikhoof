@extends('layouts.admin')
@section('title')
إضافة كوبون جديد
@endsection
@section('content')
<div class="m-portlet">
	<div class="m-portlet__body">
        {!! Form::open(['route' => 'admin.coupon.store']) !!}
            <div class="form-group m-form__group">
                <label>إسم الكوبون*</label>
                {!! Form::text('name', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>

            <div class="form-group m-form__group">
                <label>كود الكوبون*</label>
                {!! Form::text('code', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>

            <div class="form-group m-form__group">
                <label>نسبة الخصم(%)*</label>
                {!! Form::text('discount_percentage', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>

            <div class="form-group m-form__group">
                <label>تاريخ إنتهاء الكوبون*</label>
                {!! Form::text('expire_on', null, ['class' => 'form-control m-input m-input--solid', 'id' => 'm_datepicker_1', 'readonly']) !!}
            </div>

            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-primary btn-sm m-btn m-btn--icon m-btn--wide">
                    <span>
                        <i class="la la-save"></i>
                        <span>حفظ</span>
                    </span>
                </button>
            </div>
        {!! Form::close() !!}
	</div>
</div>

@endsection

@section('script')
<script src="/assets/admin/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
<script>
$(function(){
    $('#m_datepicker_1').datepicker({format: 'yyyy-mm-dd'});

})
</script>
@endsection