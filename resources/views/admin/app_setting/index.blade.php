@extends('layouts.admin')
@section('title')
عرض إعدادت الموقع
@endsection
@section('content')
<div class="m-portlet">
	<div class="m-portlet__body">
        
        <div class="m-input-icon m-input-icon--left">
            <input type="text" class="form-control m-input" placeholder="البحث في الجدول ..." id="generalSearch">
            <span class="m-input-icon__icon m-input-icon__icon--left">
                <span><i class="la la-search"></i></span>
            </span>
        </div>
        <br>
        <table class="m-datatable" width="100%">
            <thead>
                <tr>
                    <th title="Field #1" data-field="رسوم التوصيل">رسوم التوصيل</th>
                    <th title="Field #3" data-field="هاتف التواصل">هاتف التواصل</th>
                    <th title="Field #4" data-field="البريد الرسمي">البريد الرسمي</th>
                    <th title="Field #5" data-field="لوحة التحكم">لوحة التحكم</th>
                </tr>
            </thead>
            <tbody>
                @foreach($settings as $setting)
                <tr>
                    <td>{{ $setting->delivery_cost }} ريال سعودي</td>
                    <td>{{ $setting->phone }}</td>
                    <td>{{ $setting->email }}</td>
                    <td>
                        <a href="{{ route('admin.app-setting.edit', $setting) }}" class="btn btn-warning m-btn m-btn--icon btn-sm m-btn--icon-only">
                            <i class="flaticon-edit"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
	</div>
</div>
@endsection
