@extends('layouts.admin')
@section('title')
تعديل إعدادات الموقع
@endsection
@section('content')
<div class="m-portlet">
	<div class="m-portlet__body">
        {!! Form::model($setting, ['route' => ['admin.app-setting.update', $setting]]) !!}
        {{ method_field('PATCH') }}
            <label for="name">رسوم التوصيل*</label>
            {!! Form::text('delivery_cost', null, ['class' => 'form-control m-input m-input--solid']) !!}
                    <div class="form-group m-form__group">
            </div>
            <div class="form-group m-form__group">
                <label for="name">البريد اﻹلكتروني*</label>
                {!! Form::text('email', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>
            <div class="form-group m-form__group">
                <label for="name">رقم الهاتف*</label>
                {!! Form::text('phone', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>
            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-primary btn-sm m-btn m-btn--icon m-btn--wide">
                    <span>
                        <i class="la la-save"></i>
                        <span>حفظ</span>
                    </span>
                </button>
            </div>
        {!! Form::close() !!}
	</div>
</div>

@endsection