@extends('layouts.admin')
@section('title')
إضافة باقة جديدة
@endsection
@section('content')
<div class="m-portlet">
	<div class="m-portlet__body">
        {!! Form::open(['route' => 'admin.package.store']) !!}
            <div class="form-group m-form__group">
                <label>إسم الباقة*</label>
                {!! Form::text('name', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>

            <div class="form-group m-form__group">
                <label>عدد البوليصات*</label>
                {!! Form::text('policy_number', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>

            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-primary btn-sm m-btn m-btn--icon m-btn--wide">
                    <span>
                        <i class="la la-save"></i>
                        <span>حفظ</span>
                    </span>
                </button>
            </div>
        {!! Form::close() !!}
	</div>
</div>

@endsection

@section('script')
<script src="/assets/admin/demo/default/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
<script src="/assets/admin/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
@endsection