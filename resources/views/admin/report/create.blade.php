@extends('layouts.admin')
@section('title')
إضافة تقرير جديد
@endsection
@section('content')
<div class="m-portlet">
	<div class="m-portlet__body">
        {!! Form::open(['route' => 'admin.report.store']) !!}
            <div class="form-group m-form__group">
                <label>المستخدم*</label>
                {!! Form::select('user_id', $users, null, ['class' => 'form-control m-select2 m-input m-input--solid']) !!}
            </div>

            <div class="form-group m-form__group">
                <label>الفترة من وإلى*</label>
                <div class="input-daterange input-group" id="m_datepicker_5">
                    {!! Form::text('date_from', null, ['class' => 'form-control m-input m-input--solid']) !!}
                    <div class="input-group-append">
                        <span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
                    </div>
                    {!! Form::text('date_to', null, ['class' => 'form-control m-input m-input--solid']) !!}
                </div>
            </div>

            <div class="form-group m-form__group">
                <label>المهام*</label>
                {!! Form::select('functions[]', $functions, null, ['class' => 'form-control m-select2 m-input m-input--solid', 'id' => 'm_select2_3', 'multiple' => 'multiple']) !!}
            </div>

            <div class="form-group m-form__group">
                <label>البريد اﻹلكتروني*</label>
                {!! Form::text('email', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>

            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-primary btn-sm m-btn m-btn--icon m-btn--wide">
                    <span>
                        <i class="la la-save"></i>
                        <span>حفظ</span>
                    </span>
                </button>
            </div>
        {!! Form::close() !!}
	</div>
</div>

@endsection

@section('script')
<script src="/assets/admin/demo/default/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
<script src="/assets/admin/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
@endsection