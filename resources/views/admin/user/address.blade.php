@extends('layouts.admin')
@section('title')
عرض عناوين {{ $user->name }}
@endsection
@section('content')
<div class="row">
	<div class="col-xl-3 col-lg-4">
		<div class="m-portlet m-portlet--full-height  ">
			<div class="m-portlet__body">
				<div class="m-card-profile">
					<div class="m-card-profile__title m--hide">
						Your Profile
					</div>
					<div class="m-card-profile__pic">
						<div class="m-card-profile__pic-wrapper">	
							<img src="{{ $user->avatar() }}" alt="">
						</div>
					</div>
					<div class="m-card-profile__details">
						<span class="m-card-profile__name">{{ $user->name }}</span>
						<a href="" class="m-card-profile__email m-link">{{ $user->email }}</a>
					</div>
				</div>	
				<ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
					<li class="m-nav__separator m-nav__separator--fit"></li>
					<li class="m-nav__section m--hide">
						<span class="m-nav__section-text">القسم</span>
					</li>
					<li class="m-nav__item">
						<a href="{{ route('admin.user.show', $user) }}" class="m-nav__link">
							<i class="m-nav__link-icon flaticon-profile-1"></i>
							<span class="m-nav__link-title">  
								<span class="m-nav__link-wrap">      
									<span class="m-nav__link-text">معلومات المستخدم</span>      
								</span>
							</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="{{ route('admin.user.order', $user) }}" class="m-nav__link">
						<i class="m-nav__link-icon flaticon-share"></i>
							<span class="m-nav__link-text">الطلبات</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="{{ route('admin.user.shipment', $user) }}" class="m-nav__link">
							<i class="m-nav__link-icon flaticon-chat-1"></i>
							<span class="m-nav__link-text">الشحن</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="{{ route('admin.user.balance', $user) }}" class="m-nav__link">
							<i class="m-nav__link-icon flaticon-graphic-2"></i>
							<span class="m-nav__link-text">الرصيد</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="{{ route('admin.user.address', $user) }}" class="m-nav__link">
							<i class="m-nav__link-icon flaticon-time-3"></i>
							<span class="m-nav__link-text">العناوين</span>
						</a>
					</li>
				</ul>

				<div class="m-portlet__body-separator"></div>

			</div>			
		</div>	
	</div>
	<div class="col-xl-9 col-lg-8">
		<div class="m-portlet m-portlet--full-height">
			<div class="m-portlet__body">
                <table class="m-datatable" width="100%">
                <thead>
                    <tr>
                        <th title="Field #1" data-field="رقم التواصل">رقم التواصل</th>
                        <th title="Field #2" data-field="الدولة">الدولة</th>
                        <th title="Field #3" data-field="المدينة">المدينة</th>
                        <th title="Field #4" data-field="الحي">الحي</th>
						<th title="Field #5" data-field="الشارع">الشارع</th>
                        <th title="Field #6" data-field="العنوان البريدي">العنوان البريدي</th>
						<th title="Field #7" data-field="رقم المبنى">رقم المبنى</th>
						<th title="Field #8" data-field="نوع العنوان">نوع العنوان</th>						
                    </tr>
                </thead>
                <tbody>
                    @foreach($user->addresses as $address)
                    <tr>
                        <td>{{ $address->phone }} {{ ($address->phone2) ?? $address->phone2 }}</td>
                        <td>{{ $address->country->name }}</td>
                        <td>{{ $address->city }}</td>
                        <td>{{ $address->area }}</td>
						<td>{{ $address->street }}</td>
						<td>{{ $address->postcode }}</td>
						<td>{{ $address->building_number }}</td>
						<td>{{ $address->type->name }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
			</div>
			<!-- end m-portlet__body -->
		</div>
	</div>
</div>

@endsection