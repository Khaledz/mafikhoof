@extends('layouts.admin')
@section('title')
عرض طلبات {{ $user->name }}
@endsection
@section('content')
<div class="row">
	<div class="col-xl-3 col-lg-4">
		<div class="m-portlet m-portlet--full-height  ">
			<div class="m-portlet__body">
				<div class="m-card-profile">
					<div class="m-card-profile__title m--hide">
						Your Profile
					</div>
					<div class="m-card-profile__pic">
						<div class="m-card-profile__pic-wrapper">	
							<img src="{{ $user->avatar() }}" alt="">
						</div>
					</div>
					<div class="m-card-profile__details">
						<span class="m-card-profile__name">{{ $user->name }}</span>
						<a href="" class="m-card-profile__email m-link">{{ $user->email }}</a>
					</div>
				</div>	
				<ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
					<li class="m-nav__separator m-nav__separator--fit"></li>
					<li class="m-nav__section m--hide">
						<span class="m-nav__section-text">القسم</span>
					</li>
					<li class="m-nav__item">
						<a href="{{ route('admin.user.show', $user) }}" class="m-nav__link">
							<i class="m-nav__link-icon flaticon-profile-1"></i>
							<span class="m-nav__link-title">  
								<span class="m-nav__link-wrap">      
									<span class="m-nav__link-text">معلومات المستخدم</span>      
								</span>
							</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="{{ route('admin.user.order', $user) }}" class="m-nav__link">
						<i class="m-nav__link-icon flaticon-share"></i>
							<span class="m-nav__link-text">الطلبات</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="{{ route('admin.user.shipment', $user) }}" class="m-nav__link">
							<i class="m-nav__link-icon flaticon-chat-1"></i>
							<span class="m-nav__link-text">الشحن</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="{{ route('admin.user.balance', $user) }}" class="m-nav__link">
							<i class="m-nav__link-icon flaticon-graphic-2"></i>
							<span class="m-nav__link-text">الرصيد</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="{{ route('admin.user.address', $user) }}" class="m-nav__link">
							<i class="m-nav__link-icon flaticon-time-3"></i>
							<span class="m-nav__link-text">العناوين</span>
						</a>
					</li>
				</ul>

				<div class="m-portlet__body-separator"></div>

			</div>			
		</div>	
	</div>
	<div class="col-xl-9 col-lg-8">
		<div class="m-portlet m-portlet--full-height">
			<div class="m-portlet__body">
			<div class="m-input-icon m-input-icon--left">
				<input type="text" class="form-control m-input" placeholder="البحث في الجدول ..." id="generalSearch">
				<span class="m-input-icon__icon m-input-icon__icon--left">
					<span><i class="la la-search"></i></span>
				</span>
			</div>
			<br>
                <table class="m-datatable" width="100%">
                <thead>
                    <tr>
						<th>#رقم الطلب</th>
						<th>مقدم الطلب</th>
						<th>مستلم الطلب</th>
						<th>حالة الطلب</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($user->orders as $order)
                    <tr>
						<td><a href="{{ route('admin.order.show', $order) }}">#{{ $order->id }}</a></td>
						<td>{{ $order->user->name }}</td>
						<td>{{ $order->shipment->receiver->name }}</td>
						<td>{{ $order->status->name }}</td>
					</tr>
                    @endforeach
                </tbody>
            </table>
			</div>
			<!-- end m-portlet__body -->
		</div>
	</div>
</div>

@endsection