@extends('layouts.admin')
@section('title')
{{ $user->name }}
@endsection
@section('content')
<div class="row">
	<div class="col-xl-3 col-lg-4">
		<div class="m-portlet m-portlet--full-height  ">
			<div class="m-portlet__body">
				<div class="m-card-profile">
					<div class="m-card-profile__title m--hide">
						Your Profile
					</div>
					<div class="m-card-profile__pic">
						<div class="m-card-profile__pic-wrapper">	
							<img src="{{ $user->avatar() }}" alt="">
						</div>
					</div>
					<div class="m-card-profile__details">
						<span class="m-card-profile__name">{{ $user->name }}</span>
						<a href="" class="m-card-profile__email m-link">{{ $user->email }}</a>
					</div>
				</div>	
				<ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
					<li class="m-nav__separator m-nav__separator--fit"></li>
					<li class="m-nav__section m--hide">
						<span class="m-nav__section-text">القسم</span>
					</li>
					<li class="m-nav__item">
						<a href="{{ route('admin.user.show', $user) }}" class="m-nav__link">
							<i class="m-nav__link-icon flaticon-profile-1"></i>
							<span class="m-nav__link-title">  
								<span class="m-nav__link-wrap">      
									<span class="m-nav__link-text">معلومات المستخدم</span>      
								</span>
							</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="{{ route('admin.user.order', $user) }}" class="m-nav__link">
						<i class="m-nav__link-icon flaticon-share"></i>
							<span class="m-nav__link-text">الطلبات</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="{{ route('admin.user.shipment', $user) }}" class="m-nav__link">
							<i class="m-nav__link-icon flaticon-chat-1"></i>
							<span class="m-nav__link-text">الشحن</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="{{ route('admin.user.balance', $user) }}" class="m-nav__link">
							<i class="m-nav__link-icon flaticon-graphic-2"></i>
							<span class="m-nav__link-text">الرصيد</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="{{ route('admin.user.address', $user) }}" class="m-nav__link">
							<i class="m-nav__link-icon flaticon-time-3"></i>
							<span class="m-nav__link-text">العناوين</span>
						</a>
					</li>
				</ul>

				<div class="m-portlet__body-separator"></div>

				<div class="m-widget1 m-widget1--paddingless">
					
				</div>
			</div>			
		</div>	
	</div>
	<div class="col-xl-9 col-lg-8">
		<div class="m-portlet m-portlet--full-height">
			<div class="m-portlet__body">
				<div class="row skin-dark m-row--no-padding m-row--col-separator-xl">
					<div class="col-md-12 col-lg-6 col-xl-4">
						<!--begin::Total Profit-->
						<div class="m-widget24 skin--dark">					 
							<div class="m-widget24__item">
								<h4 class="m-widget24__title">
									إجمالي الطلبات
								</h4><br><br>
								<span class="m-widget24__stats m--font-success">
									{{ $user->orders->count() }} 
								</span>		
								<div class="m--space-20"></div>	
							</div>				      
						</div>
						<!--end::Total Profit-->
					</div>
					<div class="col-md-12 col-lg-6 col-xl-4">
						<!--begin::New Feedbacks-->
						<div class="m-widget24">
							<div class="m-widget24__item">
								<h4 class="m-widget24__title">
								الطلبات قيد التنفيذ
								</h4><br><br>
								<span class="m-widget24__stats m--font-danger">
									{{ $pendingOrders }}
								</span>		
								<div class="m--space-20"></div>	
							</div>		
						</div>
						<!--end::New Feedbacks--> 
					</div>
					<div class="col-md-12 col-lg-6 col-xl-4">
						<!--begin::New Orders-->
						<div class="m-widget24">
							<div class="m-widget24__item">
								<h4 class="m-widget24__title">
									الرصيد غير المستلم
								</h4><br><br>
								<span class="m-widget24__stats m--font-info">
								{{ $pendingTransfer }}  ريال
								</span>		
								<div class="m--space-20"></div>
							</div>		
						</div>
						<!--end::New Orders--> 
					</div>
				</div>
				<hr>
				<table class="table m-table m-table--head-bg-brand">
					<thead class="thead-inverse">
						<tr>
							<th>#</th>
							<th>اﻹسم</th>
							<th>البريد اﻹلكتروني</th>
							<th>رقم الهاتف</th>
							<th>الباقة</th>
							<th>الحالة</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>{{ $user->id }}</td>
							<td>{{ $user->name }}</td>
							<td>{{ $user->email }}</td>
							<td>{{ $user->phone }}</td>
							<td>{{ ! is_null($user->package) ? $user->package->name : '-' }}</td>
							<td>{{ $user->status() }}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- end m-portlet__body -->
		</div>
	</div>
</div>

@endsection