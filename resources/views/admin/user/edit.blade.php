@extends('layouts.admin')
@section('title')
تعديل المستخدم {{ $user->name }}
@endsection
@section('content')
<div class="m-portlet">
	<div class="m-portlet__body">
        {!! Form::model($user, ['route' => ['admin.user.update', $user]]) !!}
        {{ method_field('PATCH') }}
            <div class="form-group m-form__group">
                <label for="username">حالة المستخدم*</label>
                {!! Form::select('email_verified_at', ['مفعل', 'غير مفعل'], $user->email_verified_at != null ? 0 : 1, ['class' => 'form-control m-select2 m-input m-input--solid', 'id' => 'm_select2_1_modal']) !!}
            </div>
            <div class="form-group m-form__group">
                <label for="username">الباقة*</label>
                {!! Form::select('package_id', $packages, null, ['class' => 'form-control m-select2 m-input m-input--solid', 'id' => 'm_select2_1_modal']) !!}
            </div>
            <div class="form-group m-form__group">
                <label for="username">نوع المستخدم*</label>
                {!! Form::select('role_id', $roles, $user->roles, ['class' => 'form-control m-select2 m-input m-input--solid', 'id' => 'm_select2_1_modal']) !!}
            </div>
            <div class="form-group m-form__group">
                <label for="name">اﻻسم*</label>
                {!! Form::text('name', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>
            <div class="form-group m-form__group">
                <label for="name">البريد اﻹلكتروني*</label>
                {!! Form::text('email', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>
            <div class="form-group m-form__group">
                <label for="name">كلمة المرور</label>
                <small>(أترك الحقل فارغ إذا كنت ﻻ تريد تعديل كلمة المرور)</small>
                {!! Form::password('password', ['class' => 'form-control m-input m-input--solid']) !!}
            </div>
            <div class="form-group m-form__group">
                <label for="name">رقم الهاتف</label>
                {!! Form::text('phone', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>
            <div class="form-group m-form__group">
                <label for="name">الموقع اﻹلكتروني</label>
                {!! Form::text('website', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>

            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-primary btn-sm m-btn m-btn--icon m-btn--wide">
                    <span>
                        <i class="la la-save"></i>
                        <span>حفظ</span>
                    </span>
                </button>
            </div>
        {!! Form::close() !!}
	</div>
</div>

@endsection