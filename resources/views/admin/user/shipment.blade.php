@extends('layouts.admin')
@section('title')
عرض طلبات الشحن {{ $user->name }}
@endsection
@section('content')
<div class="row">
	<div class="col-xl-3 col-lg-4">
		<div class="m-portlet m-portlet--full-height  ">
			<div class="m-portlet__body">
				<div class="m-card-profile">
					<div class="m-card-profile__title m--hide">
						Your Profile
					</div>
					<div class="m-card-profile__pic">
						<div class="m-card-profile__pic-wrapper">	
							<img src="{{ $user->avatar() }}" alt="">
						</div>
					</div>
					<div class="m-card-profile__details">
						<span class="m-card-profile__name">{{ $user->name }}</span>
						<a href="" class="m-card-profile__email m-link">{{ $user->email }}</a>
					</div>
				</div>	
				<ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
					<li class="m-nav__separator m-nav__separator--fit"></li>
					<li class="m-nav__section m--hide">
						<span class="m-nav__section-text">القسم</span>
					</li>
					<li class="m-nav__item">
						<a href="{{ route('admin.user.show', $user) }}" class="m-nav__link">
							<i class="m-nav__link-icon flaticon-profile-1"></i>
							<span class="m-nav__link-title">  
								<span class="m-nav__link-wrap">      
									<span class="m-nav__link-text">معلومات المستخدم</span>      
								</span>
							</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="{{ route('admin.user.order', $user) }}" class="m-nav__link">
						<i class="m-nav__link-icon flaticon-share"></i>
							<span class="m-nav__link-text">الطلبات</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="{{ route('admin.user.shipment', $user) }}" class="m-nav__link">
							<i class="m-nav__link-icon flaticon-chat-1"></i>
							<span class="m-nav__link-text">الشحن</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="{{ route('admin.user.balance', $user) }}" class="m-nav__link">
							<i class="m-nav__link-icon flaticon-graphic-2"></i>
							<span class="m-nav__link-text">الرصيد</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="{{ route('admin.user.address', $user) }}" class="m-nav__link">
							<i class="m-nav__link-icon flaticon-time-3"></i>
							<span class="m-nav__link-text">العناوين</span>
						</a>
					</li>
				</ul>

				<div class="m-portlet__body-separator"></div>

			</div>			
		</div>	
	</div>
	<div class="col-xl-9 col-lg-8">
		<div class="m-portlet m-portlet--full-height">
			<div class="m-portlet__body">
			<div class="m-input-icon m-input-icon--left">
				<input type="text" class="form-control m-input" placeholder="البحث في الجدول ..." id="generalSearch">
				<span class="m-input-icon__icon m-input-icon__icon--left">
					<span><i class="la la-search"></i></span>
				</span>
			</div>
			<br>
				<table class="m-datatable" width="100%">
				<thead class="thead-inverse">
					<tr>
						<th title="Field #1" data-field="رقم التتبع">رقم التتبع</th>
						<th title="Field #2" data-field="رقم الطلب">رقم الطلب</th>
						<th title="Field #3" data-field="موعد اﻹستلام">موعد اﻹستلام</th>
						<th title="Field #4" data-field="حالة الشحن">حالة الشحن</th>
						<th title="Field #5" data-field="متحمل قيمة التوصيل">متحمل قيمة التوصيل</th>
					</tr>
				</thead>
				<tbody>
					@foreach($user->orders as $order)
					<tr>
						<td><a href="{{ route('admin.shipment.show', $order->shipment) }}">#{{ $order->shipment->tracking_number }}</a></td>
						<td><a href="{{ route('admin.order.show', $order->shipment->order) }}">#{{ $order->shipment->order->id }}</a></td>
						<td>{{ $order->shipment->date }} - ({{ $order->shipment->provider_schedule->from }} - {{ $order->shipment->provider_schedule->to }})</td>
						<td><a href="#" data-toggle="modal" data-target="#m_modal_{{ $order->shipment->id }}">أضغط للتفاصيل</a></td>
                    <!--begin::Modal-->
                    <div class="modal fade" id="m_modal_{{ $order->shipment->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">تفاصيل الشحن</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                        <div class="m-demo">
                            <div class="m-demo__preview">
                                <div class="m-list-timeline">
                                    <div class="m-list-timeline__items">
                                        @if($order->shipment->statuses()->count() == 0)
                                        <p>لا يوجد تفاصيل في الوقت الحالي.</p>
                                        @else
                                        @foreach($order->shipment->statuses as $status)
                                        <div class="m-list-timeline__item">
                                            <span class="m-list-timeline__badge m-list-timeline__badge--success"></span>
                                            <span class="m-list-timeline__text">{{ $status->name }}
                                                <br>
                                                @if(! is_null($status->description))
                                                <p>{{ $status->description }}</p>
                                                @endif
                                            </span>
                                            <span class="m-list-timeline__time">{{ $status->created_at->diffForHumans() }}</span>
                                        </div>
                                        @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    <!--end::Modal-->
						<td>{{ $order->shipment->money_receiver->name }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			</div>
			<!-- end m-portlet__body -->
		</div>
	</div>
</div>

@endsection