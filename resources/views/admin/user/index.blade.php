@extends('layouts.admin')
@section('title')
عرض المستخدمين
@endsection
@section('content')
<div class="m-portlet">
	<div class="m-portlet__body">
        <div class="row">
            <div class="col-md-12 text-center">
                <a href="{{ route('admin.user.create') }}" class="btn btn-primary btn-sm m-btn m-btn--icon m-btn--wide">
                    <span>
                        <i class="flaticon-plus"></i>
                        <span>إضافة جديد</span>
                    </span>
                </a>
            </div>
        </div>
        <br>
        <div class="m-input-icon m-input-icon--left">
            <input type="text" class="form-control m-input" placeholder="البحث في الجدول ..." id="generalSearch">
            <span class="m-input-icon__icon m-input-icon__icon--left">
                <span><i class="la la-search"></i></span>
            </span>
        </div>
        <br>
        <table class="m-datatable" width="100%">
            <thead>
                <tr>
                    <th title="Field #1" data-field="رقم المستخدم">#</th>
                    <th title="Field #2" data-field="اﻹسم">اﻹسم</th>
                    <th title="Field #3" data-field="البريد اﻹلكتروني">البريد اﻹلكتروني</th>
                    <th title="Field #4" data-field="نوع المستخدم">نوع المستخدم</th>
                    <th title="Field #5" data-field="الرصيد">الرصيد</th>
                    <th title="Field #6" data-field="المزيد">أدوات التحكم</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                <tr>
                    <td>#{{ $user->id }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ implode(', ', $user->getRoleNames()->toArray()) }}</td>
                    <td>{{ ($user->balance) ? $user->balance->balance .' ريال سعودي' : '-' }} </td>
                    <td>
                        <a href="{{ route('admin.user.show', $user) }}" class="btn btn-info m-btn m-btn--icon btn-sm m-btn--icon-only">
                            <i class="flaticon-medical"></i>
                        </a>
                        <a href="{{ route('admin.user.edit', $user) }}" class="btn btn-warning m-btn m-btn--icon btn-sm m-btn--icon-only">
                            <i class="flaticon-edit"></i>
                        </a>
                        <form method="POST" action="{{ route('admin.user.destroy', $user) }}" style="display:none" id="removeForm">
                            <input type="submit" />
                            {{ method_field('DELETE') }}
                            {!! csrf_field() !!}
                        </form>
                        <a id="m_sweetalert_demo_9" onclick="javascript:document.getElementById('removeForm').submit();" href="#" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only">
                            <i class="flaticon-circle"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
	</div>
</div>
@endsection

@section('script')
<script src="/assets/admin/demo/default/custom/components/base/sweetalert2.js" type="text/javascript"></script>
@endsection