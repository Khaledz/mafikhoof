@extends('layouts.admin')
@section('title')
لوحة التحكم
@endsection
@section('content')
<div class="m-portlet ">
	<div class="m-portlet__body  m-portlet__body--no-padding">
		<div class="row m-row--no-padding m-row--col-separator-xl">
			<div class="col-md-12 col-lg-6 col-xl-3">
				<!--begin::Total Profit-->
				<div class="m-widget24">					 
				    <div class="m-widget24__item">
				        <h4 class="m-widget24__title">
				            عدد اﻷعضاء
				        </h4><br>
				        <span class="m-widget24__desc">
				            عدد اﻷعضاء المسجلين
				        </span>
				        <span class="m-widget24__stats m--font-success">
				            {{ $users_count }} 
				        </span>		
				        <div class="m--space-30"></div>	
				    </div>				      
				</div>
				<!--end::Total Profit-->
			</div>
			<div class="col-md-12 col-lg-6 col-xl-3">
				<!--begin::New Feedbacks-->
				<div class="m-widget24">
					 <div class="m-widget24__item">
				        <h4 class="m-widget24__title">
				            عدد الطلبات
				        </h4><br>
				        <span class="m-widget24__desc">
				            عدد الطلبات قيد التنفيذ
				        </span>
				        <span class="m-widget24__stats m--font-danger">
                            {{ $pendingOrders }}  
				        </span>		
				        <div class="m--space-30"></div>	
				    </div>		
				</div>
				<!--end::New Feedbacks--> 
			</div>
			<div class="col-md-12 col-lg-6 col-xl-3">
				<!--begin::New Orders-->
				<div class="m-widget24">
					<div class="m-widget24__item">
				        <h4 class="m-widget24__title">
                        المبالغ التي سيتم تسليمها
				        </h4><br>
				        <!-- <span class="m-widget24__desc">
				         
				        </span> -->
				        <span class="m-widget24__stats m--font-info">
				            {{$shipmentOrders}} ريال
				        </span>		
				        <div class="m--space-30"></div>
				    </div>		
				</div>
				<!--end::New Orders--> 
			</div>
			<div class="col-md-12 col-lg-6 col-xl-3">
				<!--begin::New Users-->
				<div class="m-widget24">
					 <div class="m-widget24__item">
				        <h4 class="m-widget24__title">
                        مجموع المبالغ المستحقة
				        </h4><br>
				        <!-- <span class="m-widget24__desc">
                        مجموع المبالغ المستحقة
				        </span> -->
				        <span class="m-widget24__stats m--font-primary">
				            {{ $pendingTransfer}} ريال
				        </span>		
				        <div class="m--space-30"></div>
				    </div>		
				</div>
				<!--end::New Users--> 
			</div>
		</div>
	</div>
</div>

<div class="row">
    <div class="col-xl-6">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="">
                            آخر الطلبات
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <table class="table m-table m-table--head-bg-brand">
                <thead class="thead-inverse">
                    <tr>
                        <th>#رقم الطلب</th>
                        <th>مقدم الطلب</th>
                        <th>مستلم الطلب</th>
                        <th>حالة الطلب</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($orders as $order)
                    <tr>
                        <td>{{ $order->id }}</td>
                        <td>{{ $order->user->name }}</td>
                        <td>{{ $order->shipment->receiver->name }}</td>
                        <td>{{ $order->status->name }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
	    </div>    
    </div>
    <div class="col-xl-6">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="">
                            آخر المستخدمين
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <table class="table m-table m-table--head-bg-brand">
                <thead class="thead-inverse">
                    <tr>
                        <th>#</th>
                        <th>اﻹسم</th>
                        <th>البريد اﻹلكتروني</th>
                        <th>حالة المستخدم</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <th>{{ $user->id }}</th>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->status() }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
	    </div>    
    </div>
    <div class="col-xl-6">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="">
                            آخر الشحنات
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <table class="table m-table m-table--head-bg-brand">
                <thead class="thead-inverse">
                    <tr>
                        <th>رقم التتبع</th>
                        <th>رقم الطلب</th>
                        <th>موعد اﻹستلام</th>
                        <th>حالة الشحن</th>
                    </tr>
                </thead>
                <tbody>
                @if($shipments->count() > 0)
                @foreach($shipments as $shipment)
                <tr>
                    <td>{{ $shipment->tracking_number }}</td>
                    <td>{{ $shipment->order->id }}</td>
                    <td>{{ $shipment->date }}</td>
                    <td><a href="#" data-toggle="modal" data-target="#m_modal_{{ $shipment->id }}">أضغط للتفاصيل</a></td>
                    <!--begin::Modal-->
                    <div class="modal fade" id="m_modal_{{ $shipment->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">تفاصيل الشحن</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                        <div class="m-demo">
                            <div class="m-demo__preview">
                                <div class="m-list-timeline">
                                    <div class="m-list-timeline__items">
                                        @if($shipment->statuses()->count() == 0)
                                        <p>لا يوجد تفاصيل في الوقت الحالي.</p>
                                        @else
                                        @foreach($shipment->statuses as $status)
                                        <div class="m-list-timeline__item">
                                            <span class="m-list-timeline__badge m-list-timeline__badge--success"></span>
                                            <span class="m-list-timeline__text">{{ $status->name }}
                                                <br>
                                                @if(! is_null($status->description))
                                                <p>{{ $status->description }}</p>
                                                @endif
                                            </span>
                                            <span class="m-list-timeline__time">{{ $status->created_at->diffForHumans() }}</span>
                                        </div>
                                        @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    <!--end::Modal-->

                </tr>
                @endforeach
                @endif
                </tbody>
            </table>
            </div>
	    </div>    
    </div>
    <div class="col-xl-6">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="">
                            آخر الحوالات
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <table class="table m-table m-table--head-bg-brand">
                <thead class="thead-inverse">
                    <tr>
                        <th>#</th>
                        <th>نوع الحوالة</th>
                        <th>المستخدم</th>
                        <th>المبلغ</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($transfers as $transfer)
                    <tr>
                        <th>{{ $transfer->id }}</th>
                        <td>{{ $transfer->type->name }}</td>
                        <td>{{ $transfer->user->name }}</td>
                        <td>{{ $transfer->amount }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
	    </div>    
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="m-portlet m--bg-info m-portlet--bordered-semi m-portlet--full-height ">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h2 class="m--font-light">
                            الباقات
                        </h2>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin::Widget 29-->
                <div class="m-widget29">
                    <div class="row">
                        @foreach($packages as $package)
                        <div class="col-xl-4">
                            <div class="text-center m-widget_content">
                                <h3>{{ $package->name }}</h3>
                                <div class="m-widget_content-items">
                                    <div class="m-widget_content-item">
                                        <p>عدد المشتركين</p>
                                        <h2 class="m--font-accent">{{ $package->users()->count() }}</h2>
                                    </div>	
                                </div>	
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <!--end::Widget 29--> 
            </div>
        </div>
    </div>
</div>

@endsection