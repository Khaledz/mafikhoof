@extends('layouts.admin')
@section('title')
تعديل الطلب رقم #{{ $order->id }}
@endsection
@section('content')
<div class="m-portlet">
	<div class="m-portlet__body">
        {!! Form::model($order, ['route' => ['admin.order.update', $order]]) !!}
        {{ method_field('PATCH') }}
            <div class="form-group m-form__group">
                <label for="username">حالة الطلب*</label>
                {!! Form::select('order_status_id', $statuses, $order->order_status_id, ['class' => 'form-control m-select2 m-input m-input--solid', 'id' => 'm_select2_1_modal']) !!}
            </div>

            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-primary btn-sm m-btn m-btn--icon m-btn--wide">
                    <span>
                        <i class="la la-save"></i>
                        <span>حفظ</span>
                    </span>
                </button>
            </div>
        {!! Form::close() !!}
	</div>
</div>

@endsection