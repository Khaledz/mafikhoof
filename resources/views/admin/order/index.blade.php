@extends('layouts.admin')
@section('title')
عرض الطلبات
@endsection
@section('content')
<div class="m-portlet">
	<div class="m-portlet__body">
        <div class="m-input-icon m-input-icon--left">
            <input type="text" class="form-control m-input" placeholder="البحث في الجدول ..." id="generalSearch">
            <span class="m-input-icon__icon m-input-icon__icon--left">
                <span><i class="la la-search"></i></span>
            </span>
        </div>
        <br>
        <table class="m-datatable" width="100%">
            <thead>
                <tr>
                    <th title="Field #1" data-field="#">#</th>
                    <th title="Field #2" data-field="حالة الطلب">حالة الطلب</th>
                    <th title="Field #3" data-field="مرسل الطلب">مرسل الطلب</th>
                    <th title="Field #3" data-field="مستلم الطلب">مستلم الطلب</th>
                    <th title="Field #4" data-field="المزيد">أدوات التحكم</th>
                </tr>
            </thead>
            <tbody>
                @foreach($orders as $order)
                <tr>
                    <td>{{ $order->id }}</td>
                    <td>{{ $order->status->name }}</td>
                    <td><a href="{{ route('admin.user.show', $order->user) }}">{{ $order->user->name }}</a></td>
                    <td>{{ $order->shipment->receiver->name }}</td>
                    <td>
                        <a href="{{ route('admin.order.show', $order) }}" class="btn btn-info m-btn m-btn--icon btn-sm m-btn--icon-only">
                            <i class="flaticon-medical"></i>
                        </a>
                        <a href="{{ route('admin.order.edit', $order) }}" class="btn btn-warning m-btn m-btn--icon btn-sm m-btn--icon-only">
                            <i class="flaticon-edit"></i>
                        </a>
                        <form method="POST" action="{{ route('admin.order.destroy', $order) }}" style="display:none" id="removeForm">
                            <input type="submit" />
                            {{ method_field('DELETE') }}
                            {!! csrf_field() !!}
                        </form>
                        <a id="m_sweetalert_demo_9" onclick="javascript:document.getElementById('removeForm').submit();" href="#" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only">
                            <i class="flaticon-circle"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
	</div>
</div>
@endsection
