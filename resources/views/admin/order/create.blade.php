@extends('layouts.admin')
@section('title')
إضافة طلب جديد
@endsection
@section('content')
<div class="m-portlet">
	<div class="m-portlet__body">
        {!! Form::open(['route' => 'admin.order.store']) !!}
            <h2>قريباً</h2>

            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-primary btn-sm m-btn m-btn--icon m-btn--wide">
                    <span>
                        <i class="la la-save"></i>
                        <span>حفظ</span>
                    </span>
                </button>
            </div>
        {!! Form::close() !!}
	</div>
</div>

@endsection