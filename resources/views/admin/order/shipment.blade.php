@extends('layouts.admin')
@section('title')
عرض طلبات {{ $user->name }}
@endsection
@section('content')
<div class="row">
	<div class="col-xl-3 col-lg-4">
		<div class="m-portlet m-portlet--full-height  ">
			<div class="m-portlet__body">
				<div class="m-card-profile">
					<div class="m-card-profile__title m--hide">
						Your Profile
					</div>
					<div class="m-card-profile__pic">
						<div class="m-card-profile__pic-wrapper">	
							<img src="{{ $user->avatar() }}" alt="">
						</div>
					</div>
					<div class="m-card-profile__details">
						<span class="m-card-profile__name">{{ $user->name }}</span>
						<a href="" class="m-card-profile__email m-link">{{ $user->email }}</a>
					</div>
				</div>	
				<ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
					<li class="m-nav__separator m-nav__separator--fit"></li>
					<li class="m-nav__section m--hide">
						<span class="m-nav__section-text">القسم</span>
					</li>
					<li class="m-nav__item">
						<a href="{{ route('admin.user.show', $user) }}" class="m-nav__link">
							<i class="m-nav__link-icon flaticon-profile-1"></i>
							<span class="m-nav__link-title">  
								<span class="m-nav__link-wrap">      
									<span class="m-nav__link-text">معلومات المستخدم</span>      
								</span>
							</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="{{ route('admin.user.order', $user) }}" class="m-nav__link">
						<i class="m-nav__link-icon flaticon-share"></i>
							<span class="m-nav__link-text">الطلبات</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="{{ route('admin.user.shipment', $user) }}" class="m-nav__link">
							<i class="m-nav__link-icon flaticon-chat-1"></i>
							<span class="m-nav__link-text">الشحن</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="{{ route('admin.user.balance', $user) }}" class="m-nav__link">
							<i class="m-nav__link-icon flaticon-graphic-2"></i>
							<span class="m-nav__link-text">الرصيد</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="{{ route('admin.user.address', $user) }}" class="m-nav__link">
							<i class="m-nav__link-icon flaticon-time-3"></i>
							<span class="m-nav__link-text">العناوين</span>
						</a>
					</li>
				</ul>

				<div class="m-portlet__body-separator"></div>

			</div>			
		</div>	
	</div>
	<div class="col-xl-9 col-lg-8">
		<div class="m-portlet m-portlet--full-height">
			<div class="m-portlet__body">
                <table class="m-datatable" width="100%">
                <thead>
                    <tr>
                        <th title="Field #1" data-field="رقم المستخدم">#</th>
                        <th title="Field #2" data-field="اﻹسم">اﻹسم</th>
                        <th title="Field #3" data-field="البريد اﻹلكتروني">البريد اﻹلكتروني</th>
                        <th title="Field #4" data-field="نوع المستخدم">نوع المستخدم</th>
                        <th title="Field #5" data-field="المزيد">أدوات التحكم</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>#{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ implode(', ', $user->getRoleNames()->toArray()) }}</td>
                        <td>
                            <a href="{{ route('admin.user.show', $user) }}" class="btn btn-info m-btn m-btn--icon btn-sm m-btn--icon-only">
                                <i class="flaticon-medical"></i>
                            </a>
                            <a href="{{ route('admin.user.edit', $user) }}" class="btn btn-warning m-btn m-btn--icon btn-sm m-btn--icon-only">
                                <i class="flaticon-edit"></i>
                            </a>
                            <a href="{{ route('admin.user.destroy', $user) }}" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only">
                                <i class="flaticon-circle"></i>
                            </a>
                    </tr>
                    @endforeach
                </tbody>
            </table>
			</div>
			<!-- end m-portlet__body -->
		</div>
	</div>
</div>

@endsection