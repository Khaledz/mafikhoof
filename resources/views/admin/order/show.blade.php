@extends('layouts.admin')
@section('title')
عرض الطلب رقم #{{ $order->id }}
@endsection
@section('content')
<div class="m-portlet m-portlet--mobile m-portlet--body-progress-">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					معلومات الطلب الرئيسية
				</h3>
			</div>
		</div>
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					<a href="{{ route('admin.order.edit', $order)}}">تعديل</a>
				</h3>
			</div>
		</div>
	</div>
	<div class="m-portlet__body">
		<table class="table m-table m-table--head-bg-brand">
			<thead class="thead-inverse">
				<tr>
					<th>#رقم الطلب</th>
					<th>مقدم الطلب</th>
					<th>مستلم الطلب</th>
					<th>حالة الطلب</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<th>{{ $order->id }}</th>
					<td>{{ $order->user->name }}</td>
					<td>{{ $order->shipment->receiver->name }}</td>
					<td>{{ $order->status->name }}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="m-portlet m-portlet--mobile m-portlet--body-progress-">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					معلومات الشحن
				</h3>
			</div>			
		</div>
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					<a href="{{ route('admin.shipment.edit', $order->shipment)}}">تعديل</a>
				</h3>
			</div>
		</div>
	</div>
	<div class="m-portlet__body">
		<table class="table m-table m-table--head-bg-brand">
			<thead class="thead-inverse">
				<tr>
					<th title="Field #1" data-field="رقم التتبع">رقم التتبع</th>
					<th title="Field #2" data-field="رقم الطلب">رقم الطلب</th>
					<th title="Field #3" data-field="موعد اﻹستلام">موعد اﻹستلام</th>
					<th title="Field #4" data-field="حالة الشحن">حالة الشحن</th>
					<th title="Field #5" data-field="متحمل قيمة التوصيل">متحمل قيمة التوصيل</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{{ $order->shipment->tracking_number }}</td>
					<td><a href="{{ route('admin.order.show', $order->shipment->order) }}">{{ $order->shipment->order->id }}#</a></td>
					<td>{{ $order->shipment->date }} - ({{ $order->shipment->provider_schedule->from }} - {{ $order->shipment->provider_schedule->to }})</td>
					<td><a href="#" data-toggle="modal" data-target="#m_modal_1">أضغط للتفاصيل</a></td>
					<td>{{ $order->shipment->money_receiver->name }}</td>
				</tr>
			</tbody>
		</table>
		<hr>
		<div class="row">
		<div class="col-md-6">
		<p>خدمات إضافية:</p>
		<ul>
			@if($order->shipment->services()->count() > 0)
			@foreach($order->shipment->services as $service)
			<li>{{ $service->name }} ({{ $service->cost }} ريال)</li>
			@endforeach
			@endif
		</ul>
		</div>
		<div class="col-md-6">
		<p>نوع الخدمة والتكلفة اﻹجمالية:</p>
		<ul>
			<li>{{ $order->shipment->method->name }}</li>
			<li>{{ $order->shipment->getTotalPrice() }} ريال سعودي</li>
		</ul>
		</div>
		</div>
	</div>
</div>

<div class="m-portlet m-portlet--mobile m-portlet--body-progress-">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					عنوان المرسل
				</h3>
			</div>			
		</div>
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					<a href="{{ route('admin.address.edit', $order->shipment->sender)}}">تعديل</a>
				</h3>
			</div>
		</div>
	</div>
	<div class="m-portlet__body">
		<table class="table m-table m-table--head-bg-brand">
			<thead class="thead-inverse">
				<tr>
					<th>إسم المرسل</th>
					<th>رقم التواصل</th>
					<th>الدولة</th>
					<th>المدينة</th>
					<th>الحي</th>
					<th>الشارع</th>
					<th>العنوان البريدي</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{{ $order->shipment->sender->name }}</td>
					<td>{{ $order->shipment->sender->phone }} {{ is_null($order->shipment->sender->phone2) ? '' : ' - ' . $order->shipment->sender->phone2 }}</td>
					<td>{{ $order->shipment->sender->country->name }}</td>
					<td>{{ $order->shipment->city }}</td>
					<td>{{ $order->shipment->area }}</td>
					<td>{{ $order->shipment->street }}</td>
					<td>{{ $order->shipment->postcode }}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="m-portlet m-portlet--mobile m-portlet--body-progress-">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					عنوان المستلم
				</h3>
			</div>			
		</div>
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					<a href="{{ route('admin.address.edit', $order->shipment->receiver)}}">تعديل</a>
				</h3>
			</div>
		</div>
	</div>
	<div class="m-portlet__body">
		<table class="table m-table m-table--head-bg-brand">
		<thead class="thead-inverse">
				<tr>
					<th>إسم المرسل</th>
					<th>رقم التواصل</th>
					<th>الدولة</th>
					<th>المدينة</th>
					<th>الحي</th>
					<th>الشارع</th>
					<th>العنوان البريدي</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{{ $order->shipment->receiver->name }}</td>
					<td>{{ $order->shipment->receiver->phone }} {{ is_null($order->shipment->receiver->phone2) ? '' : ' - ' . $order->shipment->receiver->phone2 }}</td>
					<td>{{ $order->shipment->receiver->country->name }}</td>
					<td>{{ $order->shipment->city }}</td>
					<td>{{ $order->shipment->area }}</td>
					<td>{{ $order->shipment->street }}</td>
					<td>{{ $order->shipment->postcode }}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">تفاصيل الشحن</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<div class="m-demo">
			<div class="m-demo__preview">
				<div class="m-list-timeline">
					<div class="m-list-timeline__items">
						@if($order->shipment->statuses()->count() == 0)
						<p>لا يوجد تفاصيل في الوقت الحالي.</p>
						@else
						@foreach($order->shipment->statuses as $status)
						<div class="m-list-timeline__item">
							<span class="m-list-timeline__badge m-list-timeline__badge--success"></span>
							<span class="m-list-timeline__text">{{ $status->name }}
                                <br>
                                @if(! is_null($status->description))
                                <p>{{ $status->description }}</p>
                                @endif
							</span>
							<span class="m-list-timeline__time">{{ $status->created_at->diffForHumans() }}</span>
						</div>
						@endforeach
						@endif
					</div>
				</div>
			</div>
		</div>
      </div>
    </div>
  </div>
</div>
<!--end::Modal-->
@endsection