@extends('layouts.admin')
@section('title')
إضافة سلايد جديد
@endsection
@section('content')
<div class="m-portlet">
	<div class="m-portlet__body">
        {!! Form::open(['route' => 'admin.slider.store', 'files' => true]) !!}
            <div class="form-group m-form__group">
                <label>اﻹسم*</label>
                {!! Form::text('name', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>
            <div class="form-group m-form__group">
                <label>صورة السلايد*</label>
                <div></div>
                <div class="custom-file">
                    {!! Form::file('slider', ['class' => 'custom-file-input m-input m-input--solid']) !!}
                    <label class="custom-file-label" for="customFile">إختر ملف</label>
                    <span>يفضل أن يقوم مقاس السلايد 720 *1080 كحد أدني</span>
                </div>
            </div>

            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-primary btn-sm m-btn m-btn--icon m-btn--wide">
                    <span>
                        <i class="la la-save"></i>
                        <span>حفظ</span>
                    </span>
                </button>
            </div>
        {!! Form::close() !!}
	</div>
</div>
@endsection
