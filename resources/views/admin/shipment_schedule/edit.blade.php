@extends('layouts.admin')
@section('title')
تعديل خيار توصيل
@endsection
@section('content')
<div class="m-portlet">
	<div class="m-portlet__body">
        {!! Form::model($schedule, ['route' => ['admin.shipment-schedule.update', $schedule]]) !!}
        {{ method_field('PATCH') }}
            <div class="form-group m-form__group">
                <label>الحالة*</label>
                {!! Form::select('enable', ['غير مفعل','مفعل'], 1 , ['class' => 'form-control m-select2 m-input m-input--solid']) !!}
            </div>
            <div class="form-group m-form__group">
                <label>مزود الخدمة*</label>
                {!! Form::select('shipment_provider_id', $providers, null, ['class' => 'form-control m-select2 m-input m-input--solid', 'id' => 'm_select2_1_modal']) !!}
            </div>
            <div class="form-group m-form__group">
                <label>اﻷيام*</label>
                {!! Form::select('days[]', \DB::table('days')->pluck('name', 'id'), null, ['class' => 'form-control m-select2 m-input m-input--solid', 'id' => 'm_select2_3', 'multiple' => 'multiple']) !!}
            </div>
            <div class="form-group m-form__group">
                <label>الوقت من*</label>
                {!! Form::text('from', null, ['class' => 'form-control m-input m-input--solid', 'id' => 'm_timepicker_1', 'readonly']) !!}
            </div>
            <div class="form-group m-form__group">
                <label>الوقت إلى*</label>
                {!! Form::text('to', null, ['class' => 'form-control m-input m-input--solid', 'id' => 'm_timepicker_1', 'readonly']) !!}
            </div>
            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-primary btn-sm m-btn m-btn--icon m-btn--wide">
                    <span>
                        <i class="la la-save"></i>
                        <span>حفظ</span>
                    </span>
                </button>
            </div>
        {!! Form::close() !!}
	</div>
</div>

@endsection

@section('script')
<script src="/assets/admin/demo/default/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
<script src="/assets/admin/demo/default/custom/crud/forms/widgets/bootstrap-timepicker.js" type="text/javascript"></script>
@endsection