@extends('layouts.admin')
@section('title')
عرض أوقات وأسعار الشحن
@endsection
@section('content')
<div class="m-portlet">
	<div class="m-portlet__body">
        <div class="row">
            <div class="col-md-12 text-center">
                <a href="{{ route('admin.shipment-schedule.create') }}" class="btn btn-primary btn-sm m-btn m-btn--icon m-btn--wide">
                    <span>
                        <i class="flaticon-plus"></i>
                        <span>إضافة جديد</span>
                    </span>
                </a>
            </div>
        </div>
        <br>
        <div class="m-input-icon m-input-icon--left">
            <input type="text" class="form-control m-input" placeholder="البحث في الجدول ..." id="generalSearch">
            <span class="m-input-icon__icon m-input-icon__icon--left">
                <span><i class="la la-search"></i></span>
            </span>
        </div>
        <br>
        <table class="m-datatable" width="100%">
            <thead>
                <tr>
                    <th title="Field #1" data-field="اﻷيام">اﻷيام</th>
                    <th title="Field #2" data-field="الوقت من">الوقت من</th>
                    <th title="Field #3" data-field="الوقت إلى">الوقت إلى</th>
                    <th title="Field #6" data-field="الحالة">الحالة</th>
                    <th title="Field #7" data-field="مزود الخدمة">مزود الخدمة</th>
                    <th title="Field #8" data-field="أدوات التحكم">أدوات التحكم</th>
                </tr>
            </thead>
            <tbody>
                @foreach($schedules as $schedule)
                <tr>
                    <td>
                    @foreach($schedule->days as $day)
                    {{ $day->name }}, 
                    @endforeach
                    </td>
                    <td>{{ $schedule->from }}</td>
                    <td>{{ $schedule->to }}</td>
                    <td>{{ $schedule->enable() }}</td>
                    <td>{{ $schedule->provider->name }}</td>
                    <td>
                        <a href="{{ route('admin.shipment-schedule.edit', $schedule) }}" class="btn btn-warning m-btn m-btn--icon btn-sm m-btn--icon-only">
                            <i class="flaticon-edit"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
	</div>
</div>
@endsection
