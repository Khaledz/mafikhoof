@extends('layouts.admin')
@section('title')
تعديل العنوان رقم {{ $address->tracking_number }}
@endsection
@section('content')
<div class="m-portlet">
	<div class="m-portlet__body">
        {!! Form::model($address, ['route' => ['admin.address.update', $address]]) !!}
        {{ method_field('PATCH') }}
            <div class="form-group m-form__group">
                <label for="username">نوع العنوان*</label>
                {!! Form::select('address_type_id', $types, $address->type->id, ['class' => 'form-control m-select2 m-input m-input--solid', 'id' => 'm_select2_1_modal']) !!}
            </div>
            <div class="form-group m-form__group">
                <label for="name">الدولة*</label>
                {!! Form::select('country_id', $countries, $address->country->id, ['class' => 'form-control m-select2 m-input m-input--solid', 'id' => 'm_select2_1_modal']) !!}
            </div>
            <div class="form-group m-form__group">
                <label for="name">اﻹسم*</label>
                {!! Form::text('name', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>
            <div class="form-group m-form__group">
                <label for="name">المدينة*</label>
                {!! Form::text('city', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>
            <div class="form-group m-form__group">
                <label for="name">الحي*</label>
                {!! Form::text('area', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>
            <div class="form-group m-form__group">
                <label for="name">رقم الهاتف*</label>
                {!! Form::text('phone', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>
            <div class="form-group m-form__group">
                <label for="name">رقم الهاتف اﻹضافي</label>
                {!! Form::text('phone2', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>
            <div class="form-group m-form__group">
                <label for="name">رقم المبنى</label>
                {!! Form::text('building_number', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>
            <div class="form-group m-form__group">
                <label for="name">الشارع</label>
                {!! Form::text('street', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>
            <div class="form-group m-form__group">
                <label for="name">الكود البريدي</label>
                {!! Form::text('postcode', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>
            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-primary btn-sm m-btn m-btn--icon m-btn--wide">
                    <span>
                        <i class="la la-save"></i>
                        <span>حفظ</span>
                    </span>
                </button>
            </div>
        {!! Form::close() !!}
	</div>
</div>

@endsection