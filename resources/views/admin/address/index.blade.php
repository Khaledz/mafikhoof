@extends('layouts.admin')
@section('title')
عرض العناوين
@endsection
@section('content')
<div class="m-portlet">
	<div class="m-portlet__body">
        
        <div class="m-input-icon m-input-icon--left">
            <input type="text" class="form-control m-input" placeholder="البحث في الجدول ..." id="generalSearch">
            <span class="m-input-icon__icon m-input-icon__icon--left">
                <span><i class="la la-search"></i></span>
            </span>
        </div>
        <br>
        <table class="m-datatable" width="100%">
            <thead>
                <tr>
                    <th title="Field #1" data-field="#">#</th>
                    <th title="Field #2" data-field="نوع العنوان">نوع العنوان</th>
                    <th title="Field #3" data-field="اﻹسم">اﻹسم</th>
                    <th title="Field #4" data-field="رقم الهاتف">رقم الهاتف</th>
                    <th title="Field #5" data-field="الدولة">الدولة</th>
                    <th title="Field #6" data-field="المدينة">المدينة</th>
                    <th title="Field #7" data-field="الحي">الحي</th>
                    <th title="Field #8" data-field="الشارع">الشارع</th>
                    <th title="Field #9" data-field="رقم المبنى">رقم المبنى</th>
                    <th title="Field #10" data-field="الرمز البريدي">الرمز البريدي</th>
                    <th title="Field #11" data-field="لوحة التحكم">لوحة التحكم</th>
                </tr>
            </thead>
            <tbody>
                @foreach($addresses as $address)
                <tr>
                    <td>{{ $address->id }}</td>
                    <td>{{ $address->type->name }}</td>
                    <td>{{ $address->name }}</td>
                    <td>{{ $address->phone }} {{ is_null($address->phone2) ? '' : ' - ' . $address->phone2 }}</td>
                    <td>{{ $address->country->name }}</td>
                    <td>{{ $address->city }}</td>
                    <td>{{ $address->area }}</td>
                    <td>{{ $address->street }}</td>
                    <td>{{ $address->building_number }}</td>
                    <td>{{ $address->postcode }}</td>
                    <td>
                        <a href="{{ route('admin.address.edit', $address) }}" class="btn btn-warning m-btn m-btn--icon btn-sm m-btn--icon-only">
                            <i class="flaticon-edit"></i>
                        </a>
                        <form method="POST" action="{{ route('admin.address.destroy', $address) }}" style="display:none" id="removeForm">
                            <input type="submit" />
                            {{ method_field('DELETE') }}
                            {!! csrf_field() !!}
                        </form>
                        <a id="m_sweetalert_demo_9" onclick="javascript:document.getElementById('removeForm').submit();" href="#" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only">
                            <i class="flaticon-circle"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
	</div>
</div>
@endsection
