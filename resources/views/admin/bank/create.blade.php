@extends('layouts.admin')
@section('title')
إضافة حساب بنكي جديد
@endsection
@section('content')
<div class="m-portlet">
	<div class="m-portlet__body">
        {!! Form::open(['route' => 'admin.bank.store']) !!}
            <div class="form-group m-form__group">
                <label>البنك*</label>
                {!! Form::select('bank_type_id', $types, null, ['class' => 'form-control m-select2 m-input m-input--solid']) !!}
            </div>

            <div class="form-group m-form__group">
                <label>المستخدم*</label>
                {!! Form::select('user_id', $users, null, ['class' => 'form-control m-select2 m-input m-input--solid']) !!}
            </div>

            <div class="form-group m-form__group">
                <label>إسم حامل الحساب</label>
                {!! Form::text('holder_name', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>

            <div class="form-group m-form__group">
                <label>رقم الحساب*</label>
                {!! Form::text('account_number', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>

            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-primary btn-sm m-btn m-btn--icon m-btn--wide">
                    <span>
                        <i class="la la-save"></i>
                        <span>حفظ</span>
                    </span>
                </button>
            </div>
        {!! Form::close() !!}
	</div>
</div>

@endsection