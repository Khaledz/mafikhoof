@extends('layouts.admin')
@section('title')
عرض البنوك
@endsection
@section('content')
<div class="m-portlet">
	<div class="m-portlet__body">
        <div class="row">
            <div class="col-md-12 text-center">
                <a href="{{ route('admin.bank.create') }}" class="btn btn-primary btn-sm m-btn m-btn--icon m-btn--wide">
                    <span>
                        <i class="flaticon-plus"></i>
                        <span>إضافة جديد</span>
                    </span>
                </a>
            </div>
        </div>
        <br>
        <div class="m-input-icon m-input-icon--left">
            <input type="text" class="form-control m-input" placeholder="البحث في الجدول ..." id="generalSearch">
            <span class="m-input-icon__icon m-input-icon__icon--left">
                <span><i class="la la-search"></i></span>
            </span>
        </div>
        <br>
        <table class="m-datatable" width="100%">
            <thead>
                <tr>
                    <th title="Field #1" data-field="#">#</th>
                    <th title="Field #2" data-field="إسم البنك">إسم البنك</th>
                    <th title="Field #3" data-field="إسم المستخدم">إسم المستخدم</th>
                    <th title="Field #4" data-field="إسم حامل الحساب">إسم حامل الحساب</th>
                    <th title="Field #5" data-field="رقم الحساب">رقم الحساب</th>
                    <th title="Field #6" data-field="لوحة التحكم">لوحة التحكم</th>
                </tr>
            </thead>
            <tbody>
                @foreach($banks as $bank)
                <tr>
                    <td>{{ $bank->id }}</td>
                    <td>{{ $bank->type->name }}</td>
                    <td>{{ $bank->user->name }}</td>
                    <td>{{ $bank->holder_name }}</td>
                    <td>{{ $bank->account_number }}</td>
                    <td>
                        <a href="{{ route('admin.bank.edit', $bank) }}" class="btn btn-warning m-btn m-btn--icon btn-sm m-btn--icon-only">
                            <i class="flaticon-edit"></i>
                        </a>
                        <form method="POST" action="{{ route('admin.bank.destroy', $bank) }}" style="display:none" id="removeForm">
                            <input type="submit" />
                            {{ method_field('DELETE') }}
                            {!! csrf_field() !!}
                        </form>
                        <a id="m_sweetalert_demo_9" onclick="javascript:document.getElementById('removeForm').submit();" href="#" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only">
                            <i class="flaticon-circle"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
	</div>
</div>
@endsection
