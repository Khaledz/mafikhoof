@extends('layouts.admin')
@section('title')
إضافة طلب جديد
@endsection
@section('content')
<div class="m-portlet">
	<div class="m-portlet__body">
        {!! Form::open(['route' => 'admin.order.store']) !!}
            <div class="form-group m-form__group">
                <label>نوع المستخدم*</label>
                {!! Form::select('role_id', $roles, null, ['class' => 'form-control m-select2 m-input m-input--solid', 'id' => 'm_select2_1_modal']) !!}
            </div>
            <div class="form-group m-form__group">
                <label>اﻻسم*</label>
                {!! Form::text('name', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>
            <div class="form-group m-form__group">
                <label>البريد اﻹلكتروني*</label>
                {!! Form::text('email', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>
            <div class="form-group m-form__group">
                <label>كلمة المرور*</label>
                {!! Form::password('password', ['class' => 'form-control m-input m-input--solid']) !!}
            </div>
            <div class="form-group m-form__group">
                <label>رقم الهاتف</label>
                {!! Form::text('phone', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>
            <div class="form-group m-form__group">
                <label>الموقع اﻹلكتروني</label>
                {!! Form::text('website', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>

            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-primary btn-sm m-btn m-btn--icon m-btn--wide">
                    <span>
                        <i class="la la-save"></i>
                        <span>حفظ</span>
                    </span>
                </button>
            </div>
        {!! Form::close() !!}
	</div>
</div>

@endsection