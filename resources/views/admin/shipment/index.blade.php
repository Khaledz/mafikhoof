@extends('layouts.admin')
@section('title')
عرض الشحن
@endsection
@section('content')
<div class="m-portlet">
	<div class="m-portlet__body">
        <div class="m-input-icon m-input-icon--left">
            <input type="text" class="form-control m-input" placeholder="البحث في الجدول ..." id="generalSearch">
            <span class="m-input-icon__icon m-input-icon__icon--left">
                <span><i class="la la-search"></i></span>
            </span>
        </div>
        <br>
        <table class="m-datatable" width="100%">
            <thead>
                <tr>
                    <th title="Field #1" data-field="رقم التتبع">رقم التتبع</th>
                    <th title="Field #2" data-field="رقم الطلب">رقم الطلب</th>
                    <th title="Field #3" data-field="موعد اﻹستلام">موعد اﻹستلام</th>
                    <th title="Field #4" data-field="رسوم البضاعة">رسوم البضاعة</th>
                    <th title="Field #6" data-field="حالة الشحن">حالة الشحن</th>
                    <th title="Field #7" data-field="متحمل قيمة التوصيل">متحمل قيمة التوصيل</th>
                    <th title="Field #8" data-field="المزيد">أدوات التحكم</th>
                </tr>
            </thead>
            <tbody>
                @foreach($shipments as $shipment)
                <tr>
                    <td>{{ $shipment->tracking_number }}</td>
                    <td><a href="{{ route('admin.order.show', $shipment->order) }}">#{{ $shipment->order->id }}</a></td>
                    <td>{{ $shipment->date }}</td>
                    <td>
                        @if(! is_null($shipment->item_cost))
                            {{ $shipment->item_cost }} ريال
                        @else
                    -
                        @endif
                    </td>
                    <td><a href="#" data-toggle="modal" data-target="#m_modal_{{$shipment->id}}">أضغط للتفاصيل</a></td>
                    <td>{{ $shipment->money_receiver->name }}</td>
                    <td>
                        <a href="{{ route('admin.shipment.show', $shipment) }}" class="btn btn-info m-btn m-btn--icon btn-sm m-btn--icon-only">
                            <i class="flaticon-medical"></i>
                        </a>
                        <a href="{{ route('admin.shipment.edit', $shipment) }}" class="btn btn-warning m-btn m-btn--icon btn-sm m-btn--icon-only">
                            <i class="flaticon-edit"></i>
                        </a>
                        <!-- <form method="POST" action="{{ route('admin.shipment.destroy', $shipment) }}" style="display:none" id="removeForm">
                            <input type="submit" />
                            {{ method_field('DELETE') }}
                            {!! csrf_field() !!}
                        </form>
                        <a id="m_sweetalert_demo_9" onclick="javascript:document.getElementById('removeForm').submit();" href="#" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only">
                            <i class="flaticon-circle"></i>
                        </a> -->
                    </td>
                </tr>
                <!--begin::Modal-->
<div class="modal fade" id="m_modal_{{$shipment->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">تفاصيل الشحن</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="m-demo">
                <div class="m-demo__preview">
                    <div class="m-list-timeline">
                        <div class="m-list-timeline__items">
                            @if($shipment->statuses()->count() == 0)
                            <p>لا يوجد تفاصيل في الوقت الحالي.</p>
                            @else
                            @foreach($shipment->statuses as $status)
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--success"></span>
                                <span class="m-list-timeline__text">{{ $status->name }}
                                    <br>
                                    @if(! is_null($status->description))
                                    <p>{{ $status->description }}</p>
                                    @endif
                                </span>
                                <span class="m-list-timeline__time">{{ $status->created_at->diffForHumans() }}</span>
                            </div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
      </div>
    </div>
  </div>
</div>
<!--end::Modal-->
        @endforeach
            </tbody>
        </table>
	</div>
</div>


@endsection
