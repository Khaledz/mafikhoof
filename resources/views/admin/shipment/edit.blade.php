@extends('layouts.admin')
@section('title')
تعديل الشحن رقم {{ $shipment->tracking_number }}
@endsection
@section('content')

<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3>معلومات الشحنة اﻷساسية</h3>
            </div>
        </div>
    </div>
	<div class="m-portlet__body">
        {!! Form::model($shipment, ['route' => ['admin.shipment.update', $shipment]]) !!}
        {{ method_field('PATCH') }}
            <div class="form-group m-form__group">
                <label for="username">عنوان المرسل*</label>
                {!! Form::select('sender_address', $addresses, $shipment->sender->id, ['class' => 'form-control m-select2 m-input m-input--solid', 'id' => 'm_select2_1_modal']) !!}
            </div>
            <div class="form-group m-form__group">
                <label for="name">عنوان المستلم*</label>
                {!! Form::select('receiver_address', $addresses, $shipment->receiver->id, ['class' => 'form-control m-select2 m-input m-input--solid', 'id' => 'm_select2_1_modal']) !!}
            </div>
            <!-- <div class="form-group m-form__group">
                <label for="name">تكلفة البضاعة*</label>
                {!! Form::text('item_cost', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>
            <div class="form-group m-form__group">
                <label for="name">مستلم رسوم البضاعة</label>
                {!! Form::select('shipment_money_receiver_id', $money_receivers, $shipment->money_receiver->id, ['class' => 'form-control m-select2 m-input m-input--solid', 'id' => 'm_select2_1_modal']) !!}
            </div> -->
            <div class="form-group m-form__group">
                <label for="name">تاريخ التوصيل</label>
                {!! Form::text('date', $shipment->date, ['class' => 'form-control m-input m-input--solid', 'id' => 'm_datepicker_1', 'readonly']) !!}
            </div>
            <div class="form-group m-form__group">
                <label for="name">موعد التوصيل</label>
                {!! Form::select('shipment_provider_schedule_id', $shipment_provider_schedules, $shipment->provider_schedule->id, ['class' => 'form-control m-select2 m-input m-input--solid', 'id' => 'm_select2_1_modal']) !!}
            </div>
            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-primary btn-sm m-btn m-btn--icon m-btn--wide">
                    <span>
                        <i class="la la-save"></i>
                        <span>حفظ</span>
                    </span>
                </button>
            </div>
        {!! Form::close() !!}
	</div>
</div>

<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3>حالة الشحنة</h3>
            </div>
        </div>
    </div>
	<div class="m-portlet__body">
        {!! Form::open(['route' => ['admin.shipment.status.store', $shipment]]) !!}
            <div class="m-demo">
                <div class="m-demo__preview">
                    <div class="m-list-timeline">
                        <div class="m-list-timeline__items">
                            @if($shipment->statuses()->count() == 0)
                            <p>لا يوجد تفاصيل في الوقت الحالي.</p>
                            @else
                            @foreach($shipment->statuses as $status)
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--success"></span>
                                <span class="m-list-timeline__text">{{ $status->name }}
                                <br>
                                @if(! is_null($status->description))
                                <p>{{ $status->description }}</p>
                                @endif
                                </span>
                                <span class="m-list-timeline__time">{{ $status->created_at->diffForHumans() }}</span>
                            </div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group m-form__group">
                <label for="name">إضافة حالة جديدة للشحنة*</label>
                {!! Form::text('name', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>

            <div class="form-group m-form__group">
                <label for="name">إضافة وصف لحالةالشحنة</label>
                {!! Form::textarea('description', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>
            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-primary btn-sm m-btn m-btn--icon m-btn--wide">
                    <span>
                        <i class="la la-save"></i>
                        <span>حفظ</span>
                    </span>
                </button>
            </div>
        {!! Form::close() !!}
	</div>
</div>

<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3>الخدمات اﻹضافية</h3>
            </div>
        </div>
    </div>
	<div class="m-portlet__body">
        {!! Form::model($shipment->services, ['route' => ['admin.shipment.service.store', $shipment]]) !!}
            <div class="m-form__group form-group">
                <div class="m-checkbox-list">
                @foreach($services as $service)
                        <label class="m-checkbox">
                            <input type="checkbox" value="{{ $service->id }}" name="id[]" class="form-control m-input m-input--solid" {{ ($shipment->services->contains($service) ? 'checked' : '') }} />
                        {{ $service->name }} ({{ $service->cost }} ريال)<span></span>
                        </label>
                @endforeach
                </div>
            </div>
            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-primary btn-sm m-btn m-btn--icon m-btn--wide">
                    <span>
                        <i class="la la-save"></i>
                        <span>حفظ</span>
                    </span>
                </button>
            </div>
        {!! Form::close() !!}
	</div>
</div>
@endsection

@section('script')
<script src="/assets/admin/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>

<script>
$(function(){
    $('#m_datepicker_1').datepicker({ dateFormat: 'yy-mm-dd' });
})
</script>
@endsection