@extends('layouts.admin')
@section('title')
عرض الشحنة لرقم التتبع {{ $shipment->tracking_number }}
@endsection
@section('content')
<div class="m-portlet m-portlet--mobile m-portlet--body-progress-">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					معلومات الطلب
				</h3>
			</div>			
		</div>
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					<a href="{{ route('admin.order.edit', $shipment->order)}}">تعديل</a>
				</h3>
			</div>
		</div>
	</div>
	<div class="m-portlet__body">
		<table class="table m-table m-table--head-bg-brand">
			<thead class="thead-inverse">
				<tr>
					<th>#رقم الطلب</th>
					<th>مقدم الطلب</th>
					<th>مستلم الطلب</th>
					<th>حالة الطلب</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<th>{{ $shipment->order->id }}</th>
					<td>{{ $shipment->order->user->name }}</td>
					<td>{{ $shipment->order->shipment->receiver->name }}</td>
					<td>{{ $shipment->order->status->name }}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="m-portlet m-portlet--mobile m-portlet--body-progress-">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					معلومات الشحن
				</h3>
			</div>			
		</div>
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					<a href="{{ route('admin.shipment.edit', $shipment)}}">تعديل</a>
				</h3>
			</div>
		</div>
	</div>
	<div class="m-portlet__body">
		<table class="table m-table m-table--head-bg-brand">
			<thead class="thead-inverse">
				<tr>
					<th title="Field #1" data-field="رقم التتبع">رقم التتبع</th>
					<th title="Field #2" data-field="رقم الطلب">رقم الطلب</th>
					<th title="Field #3" data-field="موعد اﻹستلام">موعد اﻹستلام</th>
					<th title="Field #4" data-field="حالة الشحن">حالة الشحن</th>
					<th title="Field #5" data-field="متحمل قيمة التوصيل">متحمل قيمة التوصيل</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{{ $shipment->tracking_number }}</td>
					<td><a href="{{ route('admin.order.show', $shipment->order) }}">{{ $shipment->order->id }}#</a></td>
					<td>{{ $shipment->date }} - ({{ $shipment->provider_schedule->from }} - {{ $shipment->provider_schedule->to }})</td>
					<td><a href="#" data-toggle="modal" data-target="#m_modal_1">أضغط للتفاصيل</a></td>
					<td>{{ $shipment->money_receiver->name }}</td>
				</tr>
			</tbody>
		</table>
		<hr>
		<p>خدمات إضافية:</p>
		<ul>
			@if($shipment->services()->count() > 0)
			@foreach($shipment->services as $service)
			<li>{{ $service->name }} ({{ $service->cost }} ريال)</li>
			@endforeach
			@endif
		</ul>
	</div>
</div>

<div class="m-portlet m-portlet--mobile m-portlet--body-progress-">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					عنوان المرسل
				</h3>
			</div>			
		</div>
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					<a href="{{ route('admin.address.edit', $shipment->sender)}}">تعديل</a>
				</h3>
			</div>
		</div>
	</div>
	<div class="m-portlet__body">
		<table class="table m-table m-table--head-bg-brand">
			<thead class="thead-inverse">
				<tr>
					<th>إسم المرسل</th>
					<th>رقم التواصل</th>
					<th>الدولة</th>
					<th>المدينة</th>
					<th>الحي</th>
					<th>الشارع</th>
					<th>العنوان البريدي</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{{ $shipment->sender->name }}</td>
					<td>{{ $shipment->sender->phone }} {{ is_null($shipment->sender->phone2) ? '' : ' - ' . $shipment->sender->phone2 }}</td>
					<td>{{ $shipment->sender->country->name }}</td>
					<td>{{ $shipment->city }}</td>
					<td>{{ $shipment->area }}</td>
					<td>{{ $shipment->street }}</td>
					<td>{{ $shipment->postcode }}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="m-portlet m-portlet--mobile m-portlet--body-progress-">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					عنوان المستلم
				</h3>
			</div>			
		</div>
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					<a href="{{ route('admin.address.edit', $shipment->receiver)}}">تعديل</a>
				</h3>
			</div>
		</div>
	</div>
	<div class="m-portlet__body">
		<table class="table m-table m-table--head-bg-brand">
		<thead class="thead-inverse">
				<tr>
					<th>إسم المرسل</th>
					<th>رقم التواصل</th>
					<th>الدولة</th>
					<th>المدينة</th>
					<th>الحي</th>
					<th>الشارع</th>
					<th>العنوان البريدي</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{{ $shipment->receiver->name }}</td>
					<td>{{ $shipment->receiver->phone }} {{ is_null($shipment->receiver->phone2) ? '' : ' - ' . $shipment->receiver->phone2 }}</td>
					<td>{{ $shipment->receiver->country->name }}</td>
					<td>{{ $shipment->city }}</td>
					<td>{{ $shipment->area }}</td>
					<td>{{ $shipment->street }}</td>
					<td>{{ $shipment->postcode }}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">تفاصيل الشحن</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<div class="m-demo">
			<div class="m-demo__preview">
				<div class="m-list-timeline">
					<div class="m-list-timeline__items">
						@if($shipment->statuses()->count() == 0)
						<p>لا يوجد تفاصيل في الوقت الحالي.</p>
						@else
						@foreach($shipment->statuses as $status)
						<div class="m-list-timeline__item">
							<span class="m-list-timeline__badge m-list-timeline__badge--success"></span>
							<span class="m-list-timeline__text">{{ $status->name }}
                                <br>
                                @if(! is_null($status->description))
                                <p>{{ $status->description }}</p>
                                @endif
							</span>
							<span class="m-list-timeline__time">{{ $status->created_at->diffForHumans() }}</span>
						</div>
						@endforeach
						@endif
					</div>
				</div>
			</div>
		</div>
      </div>
    </div>
  </div>
</div>
<!--end::Modal-->

@endsection