@extends('layouts.admin')
@section('title')
تعديل حوالة
@endsection
@section('content')
<div class="m-portlet">
	<div class="m-portlet__body">
        {!! Form::model($transfer, ['route' => ['admin.transfer.update', $transfer]]) !!}
        {{ method_field('PATCH') }}

            <div class="form-group m-form__group">
                <label>حالة الحوالة*</label>
                {!! Form::select('activate', ['غير مفعل','مفعل'], null, ['class' => 'form-control m-input m-input--solid', 'id' => 'users']) !!}
            </div>

            <div class="form-group m-form__group">
                <label>نوع الحوالة*</label>
                {!! Form::select('transfer_type_id', $types, null, ['class' => 'form-control m-input m-input--solid', 'id' => 'users']) !!}
            </div>
            <div class="form-group m-form__group">
                <label>المستخدم*</label>
                {!! Form::select('user_id', $users, null, ['class' => 'form-control m-input m-input--solid', 'id' => 'users']) !!}
            </div>

            <div class="form-group m-form__group">
                <label>المبلغ*</label>
                {!! Form::text('amount', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>

            <div class="form-group m-form__group">
                <label>الطلبات</label>
                {!! Form::select('order_id', [], null, ['class' => 'form-control m-input m-input--solid', 'id' => 'orders']) !!}
                <small>هذا الحقل إختياري و يستخدم لمعلومة إضافية عن الحوالة</small>
            </div>

            <div class="form-group m-form__group">
                <label>الحساب البنكي</label>
                {!! Form::select('bank_id', [], null, ['class' => 'form-control m-input m-input--solid', 'id' => 'banks']) !!}
                <small>هذا الحقل إختياري و يستخدم لمعلومة إضافية عن الحوالة</small>
            </div>

            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-primary btn-sm m-btn m-btn--icon m-btn--wide">
                    <span>
                        <i class="la la-save"></i>
                        <span>حفظ</span>
                    </span>
                </button>
            </div>
        {!! Form::close() !!}
	</div>
</div>
@endsection

@section('script')
<script>
$(function(){
    // get orders
    let user_id = $("#users option:first").attr('selected','selected').val();
    $.ajax({
    url: "{{ route('admin.order.user') }}",
    method: 'POST',
    data: {"_token": "{{ csrf_token() }}", 'user_id': user_id},
    success: function(result){
        for(let i = 0; i < result.length; i++)
        {
            var option = new Option('#' + result[i].id + ' (' + result[i].status.name + ')', result[i].id);
            $('#orders').html(option);
        }
    }});

    // get banks
    $.ajax({
    url: "{{ route('admin.bank.user') }}",
    method: 'POST',
    data: {"_token": "{{ csrf_token() }}", 'user_id': user_id},
    success: function(result){
        for(let i = 0; i < result.length; i++)
        {
            var option = new Option(result[i].holder_name + ' ('+ result[i].type.name +': ' + result[i].account_number +')', result[i].id);
            $('#banks').html(option);
        }
    }});

    $("#users").change(function(){
        let user_id = $(this).val();
        $.ajax({
        url: "{{ route('admin.order.user') }}",
        method: 'POST',
        data: {"_token": "{{ csrf_token() }}", 'user_id': user_id},
        success: function(result){
            document.getElementById("orders").options.length = 0;
            for(let i = 0; i < result.length; i++)
            {
                var option = new Option(result[i].id, result[i].id);
                $('#orders').html(option);
            }
        }});

        $.ajax({
        url: "{{ route('admin.bank.user') }}",
        method: 'POST',
        data: {"_token": "{{ csrf_token() }}", 'user_id': user_id},
        success: function(result){
            document.getElementById("banks").options.length = 0;
            for(let i = 0; i < result.length; i++)
            {
                var option = new Option(result[i].holder_name + ' ('+ result[i].type.name +': ' + result[i].account_number +')', result[i].id);
                $('#banks').html(option);
            }
        }});

    });
});
    
</script>
<script src="/assets/admin/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
@endsection