@extends('layouts.admin')
@section('title')
عرض حالات الشحن
@endsection
@section('content')
<div class="m-portlet">
	<div class="m-portlet__body">
        <div class="row">
            <div class="col-md-12 text-center">
                <a href="{{ route('admin.shipment-status.create') }}" class="btn btn-primary btn-sm m-btn m-btn--icon m-btn--wide">
                    <span>
                        <i class="flaticon-plus"></i>
                        <span>إضافة جديد</span>
                    </span>
                </a>
            </div>
        </div>
        <br>
        <div class="m-input-icon m-input-icon--left">
            <input type="text" class="form-control m-input" placeholder="البحث في الجدول ..." id="generalSearch">
            <span class="m-input-icon__icon m-input-icon__icon--left">
                <span><i class="la la-search"></i></span>
            </span>
        </div>
        <br>
        <table class="m-datatable" width="100%">
            <thead>
                <tr>
                    <th title="Field #1" data-field="رقم التتبع">رقم التتبع</th>
                    <th title="Field #2" data-field="رقم الطلب">رقم الطلب</th>
                    <th title="Field #3" data-field="حالة الشحن">حالة الشحن</th>
                    <th title="Field #4" data-field="المزيد">أدوات التحكم</th>
                </tr>
            </thead>
            <tbody>
                @foreach($shipments as $shipment)
                <tr>
                    <td>{{ $shipment->tracking_number }}</td>
                    <td>{{ $shipment->order->id }}</td>
                    <td>{{ $shipment->statuses()->orderBy('id', 'desc')->first()->name }}</td>
                    <td>
                        <a href="{{ route('admin.shipment-status.show', $shipment) }}" class="btn btn-info m-btn m-btn--icon btn-sm m-btn--icon-only">
                            <i class="flaticon-medical"></i>
                        </a>
                        <a href="{{ route('admin.shipment-status.edit', $shipment) }}" class="btn btn-warning m-btn m-btn--icon btn-sm m-btn--icon-only">
                            <i class="flaticon-edit"></i>
                        </a>
                        <form method="POST" action="{{ route('admin.shipment-status.destroy', $shipment) }}" style="display:none" id="removeForm">
                            <input type="submit" />
                            {{ method_field('DELETE') }}
                            {!! csrf_field() !!}
                        </form>
                        <a id="m_sweetalert_demo_9" onclick="javascript:document.getElementById('removeForm').submit();" href="#" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only">
                            <i class="flaticon-circle"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
	</div>
</div>
@endsection
