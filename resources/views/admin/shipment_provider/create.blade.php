@extends('layouts.admin')
@section('title')
إضافة مزود جديد
@endsection
@section('content')
<div class="m-portlet">
	<div class="m-portlet__body">
        {!! Form::open(['route' => 'admin.shipment-provider.store']) !!}
            <div class="form-group m-form__group">
                <label>إسم مزود الخدمة*</label>
                {!! Form::text('name', null, ['class' => 'form-control m-input m-input--solid']) !!}
            </div>

            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-primary btn-sm m-btn m-btn--icon m-btn--wide">
                    <span>
                        <i class="la la-save"></i>
                        <span>حفظ</span>
                    </span>
                </button>
            </div>
        {!! Form::close() !!}
	</div>
</div>

@endsection