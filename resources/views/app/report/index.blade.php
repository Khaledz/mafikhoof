@extends('layouts.app')
@section('title')
التقارير
@endsection
@section('content')
<div class="account ">
    <div class="container ">
        <div class="row">
            @include('partials.app.account_sidebar')

			<div class="col-md-9  text-center">
            {!! Form::open(['route' => 'app.report.store']) !!}
                <div class="reports">
                    <h3 class="color2">التقارير</h3>
                    <hr class="hr">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 color2 row date1">
                            <span class="col-4 text-left">من تاريخ</span>

                            <input name="date_from" type="text" class="form-control" id="from" autocomplete="off">

                            </div>
                            <div class="col-lg-6 color2 row date1 text-left">
                                <span class="col-4 text-left">إلى تاريخ</span>
                                <input name="date_to" type="text" class="form-control" id="to" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <br>
                        <div class="form-group form-check row">
                                <div class=" col-1 text-left">
                                        <input name="functions[]" value="1"  type="checkbox" class="form-check-input" id="exampleCheck1" checked="">
                                </div>
                                <label class="form-check-label col-11" for="exampleCheck1">تقرير الشحنات</label>
                        </div>
                        <div class="form-group form-check row">
                                <div class=" col-1 text-left">
                                        <input name="functions[]" value="2"  type="checkbox" class="form-check-input" id="exampleCheck1">
                                </div>
                                <label class="form-check-label col-11" for="exampleCheck1">تقرير الشحنات المفوترة</label>
                        </div>
                        <div class="form-group form-check row">
                                <div class=" col-1 text-left">
                                        <input name="functions[]" value="3" type="checkbox" class="form-check-input" id="exampleCheck1">
                                </div>
                                <label class="form-check-label col-11" for="exampleCheck1">تقرير حركات الرصيد</label>
                        </div>
                        <div class="form-group form-check row">
                                <div class=" col-1 text-left">
                                        <input name="functions[]" value="4" type="checkbox" class="form-check-input" id="exampleCheck1">
                                </div>
                                <label class="form-check-label col-11" for="exampleCheck1">تقرير الحوالات</label>
                        </div>
                    </form>
                    <div class="text-center send ">
                        <h5 class="color2">إرسال إلى البريد الإلكتروني</h5>
                        <input type="text" name="email" class="form-control boxofinput">
                        <button type="submit" class="form-control btn btn-primary my-1 buttonbox maxwidth200px">إرسال</button>
                    </div>
                
                </div>
            </div>
		</div>
	</div>
</div>
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script>
$(function(){
    $('#from').datepicker({format: 'yyyy-mm-dd'});

$('#to').datepicker({format: 'yyyy-mm-dd'});
})
</script>
@endsection