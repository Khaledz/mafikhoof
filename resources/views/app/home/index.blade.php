@extends('layouts.app')
@section('title')
الصفحة الرئيسية
@endsection
@section('content')
<!--استلم شحنتك -->
@if($sliders->count() > 0)
<div class="container">
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel" style="margin-top: 12px;">
  <div class="carousel-inner">
    @foreach($sliders as $slider)
    <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
      <img class="d-block w-100" src="{{ $slider->slider() }}" alt="{{ $slider->name }}">
    </div>
    @endforeach
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>
@endif
<div class="charge text-center container">
    <!-- check tracking number --> 
    <h3 class="color1 ">ابتكار جديد لأسرع وأسهل أعمال الشحن </h3>
    {!! Form::open(['route' => 'app.shipment.track']) !!}
    {!! Form::text('tracking_number', null, ['class' => 'col-md-6', 'placeholder' => 'أدخل رقم تببع الشحنة ..']) !!}
        <button class="col-md-1 btn btn-primary "  type="submit">إبـحـث</button>
    </form>
</div>





        <!--ارسال وتتبع الشحنات -->
        <div class="recive">
            <div class="background">
            <div class="container ">
                <ul >
                    <li>ارسال وتتبع جميع الشحنات من جوالك.</li>
                    <li>امكانيه ارسال عنوانك عبر الاقمار الصناعية.</li>
                    <li> حفظ العناوين لاستخدامها فى الشحنات القادمة.</li>
                    <li>امكانيه طلب تغليف الشحن قبل ارسالها.</li>
                    <li>رسائل تنبيهية لجميع الشحنات عبر التطبيق والايميل والرسائل النصية والمتابعه الهاتفيه. </li>
                    <li>اشعار بقرب قدوم السائق اليك والقت المتبي للوصول.</li>
                    <li>ثبت تطبيق مافي خوف واستمتع باسهل طريقة للشحن</li>
                </ul>
            </div>
            </div></div>
        </div>



        <!--الخطوات فى صور-->
        <div class="step container text-center">
            <h2 class="color1">اطلب خدماتنا سريعاً</h2>
            <div class="grid1">
                <div class="container">
                    <div class="row">
                    <div class="col-md-6 col-lg-3">
                        <i class="fas fa-box rounded-circle"></i>
                        <p class="color1" ><span class="color2" >1 -</span> اختيار التاجر </p>
                    </div>
                    <div class="col-md-6 col-lg-3">
                            <i class="fas fa-home rounded-circle"></i>
                            <p class="color1" ><span class="color2" >2 -</span> تحديد عنوانك </p>
                    </div>
                    <div class="col-md-6 col-lg-3">
                                <i class="fas fa-search rounded-circle"></i>
                                <p class="color1" ><span class="color2" >3 -</span> متابعة الشحنة </p>
                    </div>
                    <div class="col-md-6 col-lg-3">
                            <i class="fas fa-handshake rounded-circle"></i>
                            <p class="color1" ><span class="color2" >4 -</span> استلام الشحنة </p>
                    </div>
                        
                </div>

                </div>

            </div>

        </div>



        <!--هل ما زلت ترغب فى المزيد-->
        <div class="more">
            <div class="row">
                <div class="col-lg-6 text-center">
                    <img src="/assets/app/img/asd.png">
                </div>
                <div class="col-lg-6  txt  " >
                    <h3 class="color1">هل ما زلت ترغب فى المزيد من المزايا؟</h3>
                    <p class="color3">سلم واستلم من مدينتك من الباب الى الباب</p>
                    <p class="color3">لكل ايام السنة حتى فى  <span>الاجازات</span> الموسمية</p>
                    <p class="color3"><span>من الثامنة صباحا و حتى العاشرة مساء</span></p>
                    <a href="{{ url('/register') }}"><button class="btn btn-primary buttonbox ">انضم الينا الان</button></a>
                </div>
            </div>

        </div>
       


        <!--هل تواجه مشكله-->
        <div class="problem">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 row">
                        <i class="far fa-envelope col-2"></i><div class="col-10">
                        <h4 class="color1 ">هل تواجه اى مشكله وتحتاج الى مساعده ؟</h4>
                        <p class="color2 ">يمكنك التواصل معنا الان وسيتم التواصل معك والرد على جميع استفساراتك وحل اى مشكله تواجهك .</p></div>
                    </div>
                    <div class="col-lg-3 ">
                    <a href="{{ route('app.home.contact') }}"><button class="btn btn-primary  ">تواصل معنا</button></a>
                    </div>
                </div>
            </div>
        </div>
@endsection