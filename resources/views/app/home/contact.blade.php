@extends('layouts.app')
@section('title')
تواصل معنا
@endsection
@section('content')
<!--استلم شحنتك -->
<div class="charge text-center container">
    <!-- check tracking number --> 
    <h3 class="color1 ">تواصل معنا </h3>
    <h5 class="color2">نسعد كثيراً بخدمتكم ولتسهيل التواصل تستطيع التحدث معنا مباشرة عبر الواتساب <a target="_blank" href="https://api.WhatsApp.com/send?phone={{ $setting->phone }}">بالضغط هنا</a> </h5>
</div>

@endsection