@extends('layouts.app')
@section('title')
عرض طلباتي
@endsection
@section('content')
<div class="account ">
    <div class="container ">
        <div class="row">
			@include('partials.app.account_sidebar')
			<div class="col-md-9  text-center font-weight-bold">
                <div class="address_recived text-center">
                    <h3 class="color2">
                   عرض طلباتي
</h3>

                    <hr class="hr">
                    <div class="row">
                    @if($orders->count() == 0)
                        لا يوجد طلبات في الوقت الحالي.
                    @else
                    @foreach($orders as $order)
                    <div class="col-12 color2 box ">
                        <div class="float-left">
                            رقم الطلب
                            <span>#{{ $order->id }}</span>
                        </div>
                        <div class=" float-right">
                            <span>{{ $order->status->name }}</span>
                        </div>
                    </div>
                    @endforeach
                    @endif
                    </div>
                    <br>
                </div></div>

            </div>
		</div>
	</div>
</div>
@endsection