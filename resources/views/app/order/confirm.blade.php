@extends('layouts.app')
@section('title')
طلب جديد
@endsection
@section('content')
<div class="account ">
    <div class="container ">
        <div class="row">
			@include('partials.app.account_sidebar')
			<div class="col-md-9  text-center">
            <div class="new_order_confirm">
                <h3 class="color2">تفاصيل الطلب</h3>
                <hr class="hr">
                <div class="container">
                        <div class="row">
                            <div class="confirm col-lg-6">
                                <div class="confirm_detail">{{ $sender_address->name }}</div>
                                <div class="confirm_detail">{{ $sender_address->area }}</div>
                                <div class="confirm_detail">{{ $sender_address->city }}</div>
                                <div class="confirm_detail">{{ $sender_address->country->name }}</div>
                                <div class="confirm_detail numberfont">{{ $sender_address->phone }}</div>
                                <div class="arrow">
                                        <i class="fas fa-arrow-left"></i>
                                </div>
                            </div>
                            <div class="confirm col-lg-6">
                                <div class="confirm_detail">{{ $receiver_address->name }}</div>
                                <div class="confirm_detail">{{ $receiver_address->area }}</div>
                                <div class="confirm_detail">{{ $receiver_address->city }}</div>
                                <div class="confirm_detail">{{ $receiver_address->country->name }}</div>
                                <div class="confirm_detail numberfont">{{ $receiver_address->phone }}</div>
                                    <div class="confirm_detail_tail">
                                        <div class="plumb">توصيل  <span class="numberfont">{{ $setting->delivery_cost }}</span></div>
                                        @if(! is_null($shipment->item_cost))
                                        <div class="bankup">تحصيل <span class="numberfont">{{ $shipment->item_cost }}</span></div>
                                        @endif
                                    </div>
                            </div>

                        </div>
                </div>
                @if(! is_null($shipment->item_cost))
                <div class="container message">
                        سيتم إضافة <span class="numberfont">{{ $shipment->item_cost}}</span> ريال لرصيدك إذا تم التسليم بنجاح  
                </div>
                @endif
                <h3 class="color2">تفاصيل الرسوم </h3>
                <hr class="hr">
                {!! Form::open(['route' => 'app.order.confirm']) !!}
                    @php $total = 0; $totalWithoutItemCost = 0 @endphp
                    <ul>
                        @if(! is_null($shipment->item_cost))
                        <li>رسوم البضاعة: {{ $shipment->item_cost }} ريال سعودي</li>
                        @php $total += $shipment->item_cost @endphp
                        @endif
                        <li>رسوم التوصيل: {{ $setting->delivery_cost }} ريال سعودي</li>
                        @php $total += $setting->delivery_cost; $totalWithoutItemCost += $setting->delivery_cost @endphp
                        @if($shipment->services->count() > 0)
                        @foreach($shipment->services as $service)
                        <li>{{ $service->name }}: {{ $service->cost}} ريال سعودي</li>
                        @php $total += $service->cost; $totalWithoutItemCost += $service->cost @endphp
                        @endforeach
                        @endif
                    </ul>
                    المجموع: {{ $total }} ريال سعودي
                    {{ session()->put('total',$total)}}

                </form>

                <h3 class="color2">من سيدفع قيمة التوصيل ( <span class="numberfont">
                @if(isset($totalWithoutItemCost))
                {{ $totalWithoutItemCost }}
                {{ session()->put('totalWithoutItemCost',$totalWithoutItemCost)}}
                @else
    {{$setting->delivery_cost}}
                @endif
                 </span> )</h3>
                <hr class="hr">
                {!! Form::open(['route' => 'app.order.confirm']) !!}
                    @foreach($money_receivers as $money_receiver)
                        <div class="row form-check">
                                <input type="radio" name="money_receiver" value="{{ $money_receiver->id }}" class="col-1">
                                <span class="col-11 ">{{ $money_receiver->name }}</span>
                                @if($money_receiver->id == 3)
                                <small>رصيدك الحالي هو: {{ $balance->balance }} ريال سعودي</small>
                                @endif
                        </div>
                    @endforeach
                    
                <div class="twobtt">
                    <button type="submit" class="form-control btn btn-primary my-1 buttonbox maxwidth200px">حفظ</button>
                </div>
                </form>

                

            </div>
            
            </div>
		</div>
	</div>
</div>
@endsection