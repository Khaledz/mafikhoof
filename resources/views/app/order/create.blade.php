@extends('layouts.app')
@section('title')
طلب جديد
@endsection
@section('content')
<div class="account ">
    <div class="container ">
        <div class="row">
			@include('partials.app.account_sidebar')
			<div class="col-md-9 text-center">
                        <div class="new_order  ">
                            <div class="fee " >
                                <div class="delivery ">
                                    <span class="float-left text-left">سعر التوصيل</span>
                                    <span class="float-right text-right numberfont">{{ $setting->delivery_cost }}</span>
                                </div>
                            </div>
                            {!! Form::open(['route' => 'app.order.store']) !!}
                            <div class="new_order_menue ">
                                <h3 class="color2">عنوان المرسل</h3>
                                <hr class="hr">
                                <ul class="row row1 row11 nav nav-tabs" id="myTab" role="tablist" >
                                    <li class="nav-item"><a class=" text-center   background2 color3 nav-link active"  id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">عنوانى</a></li>
                                    <li class="nav-item"><a class=" text-center   background2 color3 nav-link "  id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">عنوان جديد </a> </li>
                                    <li class="nav-item"><a class=" text-center  background2 color3 nav-link "  id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">إختيار عنوان</a></li>
                                </ul>
                                <div id="myTabContent" class="tab-content">
                                <div id="home" role="tabpanel" aria-labelledby="home-tab" class="tab-pane fade show active" >
                                    @if($mains->count() == 0)
                                        <p class="alert alert-warning">لا يوجد عناوين رئيسية في الوقت الحالي .. قم بإضافة <a href="{{ route('app.address.create') }}">عنوان جديد</a></p>
                                    @else
                                    @foreach($mains as $main)
                                    <div class="row form-check">
                                        <input type="radio" name="sender_address_id" value="{{ $main->id }}" class="col-1">
                                        <span class="col-2 ">{{ $main->name }}</span>
                                        <span class="col-2 ">{{ $main->city }}</span>
                                        <span class="col-2 ">({{ $main->phone }} {{ $main->phone2 }})</span>
                                    </div>
                                    @endforeach
                                    @endif
                                </div>
                                <div id="profile" role="tabpanel" aria-labelledby="profile-tab" class="tab-pane fade ">
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-4 col-lg-2 col-sm-3 col-form-label">الاسم</label>
                                    <div class="col-7 col-lg-9 col-sm-8">
                                    {!! Form::text('sender_name', null, ['class' => 'form-control boxofinput']) !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputState" class="col-4 col-lg-2 col-sm-3  col-form-label ">الدولة</label>
                                    <div class="col-7 col-lg-9 col-sm-8">
                                        {!! Form::select('sender_country_id', $countries, null, ['class' => 'form-control boxofinput']) !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputState" class="col-4 col-lg-2 col-sm-3  col-form-label">المدينة</label>
                                    <div class="col-7 col-lg-9 col-sm-8">
                                        {!! Form::text('sender_city', null, ['class' => 'form-control boxofinput']) !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                        <label for="inputPassword" class="col-4 col-lg-2 col-sm-3  col-form-label">الحى</label>
                                        <div class="col-7 col-lg-9 col-sm-8">
                                        {!! Form::text('sender_area', null, ['class' => 'form-control boxofinput']) !!}
                                        </div>
                                </div>
                                <div class="form-group row">
                                        <label for="inputPassword" class="col-4 col-lg-2 col-sm-3  col-form-label">الموبايل</label>
                                        <div class="col-7 col-lg-9 col-sm-8">
                                        {!! Form::text('sender_phone', null, ['class' => 'form-control boxofinput']) !!}
                                        </div>
                                </div>
                                    <div class="form-group form-check row">
                                            <div class=" col-1 text-left">
                                                    <input type="checkbox" class="form-check-input" id="exampleCheck1" name="saveSenderAddress" >
                                            </div>
                                            <label class="form-check-label col-11" for="exampleCheck1">حفظ العنوان لإستخدامه بالمستقبل</label>
                                    </div>
                                </div>
                                <!-- select address -->
                                <div id="contact" role="tabpanel" aria-labelledby="contact-tab" class="tab-pane fade">
                                    @if($senders->count() == 0)
                                        <p class="alert alert-warning">لا يوجد عناوين مرسلة في الوقت الحالي .. قم بإضافة <a href="{{ route('app.address.create') }}">عنوان جديد</a></p>
                                    @else
                                    @foreach($senders as $sender)
                                    <div class="row form-check">
                                        <input type="radio" name="sender_address_id" value="{{ $sender->id }}" class="col-1">
                                        <span class="col-2 ">{{ $sender->name }}</span>
                                        <span class="col-2 ">{{ $sender->city }}</span>
                                        <span class="col-2 ">({{ $sender->phone }} {{ $sender->phone2 }})</span>
                                    </div>
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                                <h3 class="color2">الوقت المفضل للاستلام من المرسل</h3>
                                <hr class="hr">
                                <ul class="row row1 row11 nav nav-tabs" id="myTab2" role="tablist" >
                                    <li class="nav-item"><a class=" text-center   background2 color3 nav-link "  id="home-tab2" data-toggle="tab" href="#home2" role="tab" aria-controls="home" aria-selected="true">اقرب وقت</a></li>
                                    <li class="nav-item"><a class=" text-center   background2 color3 nav-link  active"  id="profile-tab2" data-toggle="tab" href="#profile2" role="tab" aria-controls="profile" aria-selected="false">اليوم</a> </li>
                                    <li class="nav-item"><a class=" text-center  background2 color3 nav-link "  id="contact-tab2" data-toggle="tab" href="#contact2" role="tab" aria-controls="contact" aria-selected="false">غدا</a></li>
                                </ul>
                                <div class="tab-content" id="myTabContent2">
                                <div class="form-form-check tab-pane fade" id="home2" role="tabpanel" aria-labelledby="home-tab">
                                    @if($schedulesForToday->count() == 0)
                                        <p class="alert alert-warning">لا يوجد مواعيد في الوقت الحالي.</p>
                                    @else
                                    @foreach($schedulesForToday as $schedule)
                                    <div class="row form-check">
                                        <input type="radio" name="schedule_id" value="{{ $schedule->id }}" class="col-1">
                                        <span class="col-2 ">يوم {{ $schedule->days->first()->name }}</span>
                                        <span class="col-4" >من <span class="numberfont">{{ $schedule->from }}</span>
                                        <span class="col-4 ">الى <span class="numberfont">{{ $schedule->to }}</span> <br>
                                    </div>
                                    @endforeach
                                    @endif
                                </div>
                                <div class="form-form-check tab-pane fade show active " id="profile2" role="tabpanel" aria-labelledby="profile-tab">
                                    @if($schedulesForToday->count() == 0)
                                        <p class="alert alert-warning">لا يوجد مواعيد في الوقت الحالي.</p>
                                    @else
                                    @foreach($schedulesForToday as $schedule)
                                    <div class="row form-check">
                                        <input type="radio" name="schedule_id" value="{{ $schedule->id }}" class="col-1">
                                        <span class="col-2 ">يوم {{ $schedule->days->first()->name }}</span>
                                        <span class="col-4" >من <span class="numberfont">{{ $schedule->from }}</span>
                                        <span class="col-4 ">الى <span class="numberfont">{{ $schedule->to }}</span> <br>
                                    </div>
                                    @endforeach
                                    @endif
                                </div>
                                <div class="form-form-check tab-pane fade " id="contact2" role="tabpanel" aria-labelledby="contact-tab">
                                    @if($schedulesForTomorrow->count() == 0)
                                        <p class="alert alert-warning">لا يوجد مواعيد في الوقت الحالي.</p>
                                    @else
                                    @foreach($schedulesForTomorrow as $schedule)
                                    <div class="row form-check">
                                        <input type="radio" name="schedule_id" value="{{ $schedule->id }}" class="col-1">
                                        <span class="col-2 ">يوم {{ $schedule->days->first()->name }}</span>
                                        <span class="col-4" >من <span class="numberfont">{{ $schedule->from }}</span>
                                        <span class="col-4 ">الى <span class="numberfont">{{ $schedule->to }}</span> <br>
                                    </div>
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                                <h3 class="color2">عنوان المستلم</h3>
                                <hr class="hr">
                                <ul class="row row1 row11 nav nav-tabs" id="myTab3" role="tablist" >
                                    <li class="nav-item"><a class=" text-center   background2 color3 nav-link active"  id="home-tab" data-toggle="tab" href="#home3" role="tab" aria-controls="home" aria-selected="true">عنوانى</a></li>
                                    <li class="nav-item"><a class=" text-center   background2 color3 nav-link "  id="profile-tab" data-toggle="tab" href="#profile3" role="tab" aria-controls="profile" aria-selected="false">عنوان جديد </a> </li>
                                    <li class="nav-item"><a class=" text-center  background2 color3 nav-link "  id="contact-tab" data-toggle="tab" href="#contact3" role="tab" aria-controls="contact" aria-selected="false">إختيار عنوان</a></li>
                                </ul>
                                <div class="tab-content" id="myTabContent3">
                                    <div id="home3" role="tabpanel" aria-labelledby="home-tab" class="tab-pane fade show active">
                                        @if($mains->count() == 0)
                                            <p class="alert alert-warning">لا يوجد عناوين رئيسية في الوقت الحالي .. قم بإضافة <a href="{{ route('app.address.create') }}">عنوان جديد</a></p>
                                        @else
                                        @foreach($mains as $main)
                                        <div class="row form-check">
                                            <input type="radio" name="receiver_address_id" value="{{ $main->id }}" class="col-1">
                                            <span class="col-2 ">{{ $main->name }}</span>
                                            <span class="col-2 ">{{ $main->city }}</span>
                                            <span class="col-2 ">({{ $main->phone }} {{ $main->phone2 }})</span>
                                        </div>
                                        @endforeach
                                        @endif
                                    </div>
                                    <div id="profile3" role="tabpanel" aria-labelledby="profile-tab" class="tab-pane fade ">
                                    <div class="form-group row">
                                        <label for="inputPassword" class="col-4 col-lg-2 col-sm-3 col-form-label">الاسم</label>
                                        <div class="col-7 col-lg-9 col-sm-8">
                                        {!! Form::text('receiver_name', null, ['class' => 'form-control boxofinput']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputState" class="col-4 col-lg-2 col-sm-3  col-form-label ">الدولة</label>
                                        <div class="col-7 col-lg-9 col-sm-8">
                                            {!! Form::select('receiver_country_id', $countries, null, ['class' => 'form-control boxofinput']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputState" class="col-4 col-lg-2 col-sm-3  col-form-label">المدينة</label>
                                        <div class="col-7 col-lg-9 col-sm-8">
                                            {!! Form::text('receiver_city', null, ['class' => 'form-control boxofinput']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                            <label for="inputPassword" class="col-4 col-lg-2 col-sm-3  col-form-label">الحى</label>
                                            <div class="col-7 col-lg-9 col-sm-8">
                                            {!! Form::text('receiver_area', null, ['class' => 'form-control boxofinput']) !!}
                                            </div>
                                    </div>
                                    <div class="form-group row">
                                            <label for="inputPassword" class="col-4 col-lg-2 col-sm-3  col-form-label">الموبايل</label>
                                            <div class="col-7 col-lg-9 col-sm-8">
                                            {!! Form::text('receiver_phone', null, ['class' => 'form-control boxofinput']) !!}
                                            </div>
                                    </div>
                                    <div class="form-group form-check row">
                                            <div class=" col-1 text-left">
                                                    <input type="checkbox" class="form-check-input" id="exampleCheck1" name="saveReceiverAddress" >
                                            </div>
                                            <label class="form-check-label col-11" for="exampleCheck1">حفظ العنوان لإستخدامه بالمستقبل</label>
                                    </div>
                                </div>
                            <div id="contact3" role="tabpanel" aria-labelledby="contact-tab" class="tab-pane fade ">
                                @if($receivers->count() == 0)
                                        <p class="alert alert-warning">لا يوجد عناوين مستلمة في الوقت الحالي .. قم بإضافة <a href="{{ route('app.address.create') }}">عنوان جديد</a></p>
                                    @else
                                    @foreach($receivers as $receiver)
                                    <div class="row form-check">
                                        <input type="radio" name="receiver_address_id" value="{{ $receiver->id }}" class="col-1">
                                        <span class="col-2 ">{{ $receiver->name }}</span>
                                        <span class="col-2 ">{{ $receiver->city }}</span>
                                        <span class="col-2 ">({{ $receiver->phone }} {{ $receiver->phone2 }})</span>
                                    </div>
                                    @endforeach
                                    @endif
                            </div>
                            </div>
                                <h3 class="color2">تحصيل قيمة البضاعة من المستلم</h3>
                                <hr class="hr">
                                <div class="tab-pane fade show active">

                                <div class="form-group row">
                                                <label for="inputPassword" class="col-4 col-lg-3 col-form-label">نوع الخدمة</label>
                                                <div class="col-8 col-lg-9 col-sm-8">
                                                {!! Form::select('shipment_method_id', $methods, null, ['class' => 'form-control fontaral boxofinput', 'id' => 'method']) !!}
                                                </div>
                                    </div>

                                    <div class="form-group row" id="item_cost">
                                                <label for="inputPassword" class="col-4 col-lg-3 col-form-label">قيمة البضاعة</label>
                                                <div class="col-8 col-lg-9 col-sm-8">
                                                    <input type="text" class="form-control fontaral boxofinput" name="item_cost">
                                                </div>
                                                <p class="text-left precive">- هذا هو المبلغ الذي سيتم إرجاعه لك بعد إستلامة .</p>
                                    <p class="text-left precive">- يرجى عدم إضافة قيمة التوصيل على قيمة البضاعة ما عدى في حال الدفع على الحساب .</p>

                                    </div>
                                </div>
                                <h3 class="color2">معلومات إضافية ( غير إلزامية )</h3>
                                <hr class="hr">
                                <div class="tab-pane fade show active">
                                    <div class="form-group row">
                                            <label for="inputPassword" class="col-4 col-lg-3 col-form-label">رقم الطلب</label>
                                            <div class="col-8 col-lg-9 col-sm-8">
                                                <input type="text" class="form-control boxofinput" placeholder="اختيارى">
                                            </div>
                                    </div>
                                    @foreach($services as $service)
                                    <div class="form-group form-check row">
                                            <div class=" col-1 text-left">
                                                    <input type="checkbox" class="form-check-input" id="exampleCheck1" name="services[]" value="{{ $service->id }}" >
                                            </div>
                                            <label class="form-check-label col-11" for="exampleCheck1">{{ $service->name }}</label>
                                    </div>
                                    @endforeach
                                </div>


                                <button type="submit" class="form-control btn btn-primary my-1 buttonbox maxwidth200px">التالى</button>
                            </div>
                        </div>
                      </div>
            </form>
		</div>
	</div>
</div>
@endsection

@section('script')
<script>
$('#method').change(function(){
    let method_id = $(this).val();

    if(method_id == 1)
    {
        $('#item_cost').show();
    }
    else if(method_id == 2)
    {
        $('#item_cost').hide();

    }
    
});
</script>
@endsection