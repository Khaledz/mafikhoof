@extends('layouts.app')
@section('title')
التنبيهات
@endsection
@section('content')
<div class="account ">
    <div class="container ">
        <div class="row">
			@include('partials.app.account_sidebar')
			<div class="col-md-9  text-center font-weight-bold">
                <div class="address_recived text-center">
                    <h3 class="color2">
                   التنبيهات
                </h3>

                    <hr class="hr">
                    <div class="row">
                    @if($notifications->count() == 0)
                        لا يوجد تنبيهات في الوقت الحالي.
                    @else
                    @foreach($notifications as $notification)
                    <div class="col-12 color2 box ">
                        <div class="float-left">
                            <span>{{ $notification->data['title'] }}</span>
                        </div>
                        <div class=" float-right">
                            <span>{{ $notification->data['text'] }}</span>
                        </div>
                    </div>
                    @endforeach
                    @endif
                    </div>
                    <br>
                </div></div>

            </div>
		</div>
	</div>
</div>
@endsection