@extends('layouts.app')
@section('title')
ملفي الشخصي
@endsection
@section('content')
<div class="account ">
    <div class="container ">
        <div class="row">
			@include('partials.app.account_sidebar')
			<div class="col-md-9  text-center">
				<div class="my_account">
					<div class="row font-weight-bold color2">
						<div class="col-md-4 col-6">
						<a href="{{ route('app.order.create') }}">
							<i class="fas fa-truck rounded-circle"></i>
							<p>طلب جديد</p>
						</a>
						</div>

						<div class="col-md-4 col-6">
						<a href="{{ route('app.order.index') }}">
								<i class="fas fa-truck rounded-circle"></i>
								<p>طلباتي</p>
						</a>
						</div>
						<div class="col-md-4 col-6">
						<a href="{{ route('app.address.index') }}">
								<i class="fas fa-map-marker-alt rounded-circle"></i>
								<p>عناوينى</p>
						</a>
						</div>
						<div class="col-md-4 col-6">
							<a href="{{ route('app.bank.index') }}">
								<i class="fas fa-coins rounded-circle"></i>
								<p>حسابات الحولات البنكية</p>
							</a>
						</div>
						@if(auth()->user()->hasAnyRole('trader'))
						<div class="col-md-4 col-6">
						<a href="{{ route('app.user.duration') }}">
								<i class="far fa-credit-card rounded-circle"></i>
								<p>فتره التحويل البنكى</p>
						</a>
						</div>
						<div class="col-md-4 col-6">
							<a href="{{ route('app.transfer.index') }}">
								<i class="fas fa-chart-line rounded-circle"></i>
								<p>حركات الرصيد</p>
							</a>
						</div>
						<div class="col-md-4 col-6">
						<a href="{{ route('app.transfer.create') }}">
								<i class="far fa-credit-card rounded-circle"></i>
								<p>إيداع رصيد</p>
						</a>
						</div>
						@endif
						<div class="col-md-4 col-6">
						<a href="{{ route('app.coupon.create') }}">
								<i class="fas fa-gift rounded-circle"></i>
								<p>تفعيل قسيمة</p>
						</a>
						</div>
						<div class="col-md-4 col-6">
						<a href="{{ route('app.report.index') }}">
								<i class="fas fa-file rounded-circle"></i>
								<p>التقارير</p>
						</a>
						</div>
						<div class="col-md-4 col-6">
						<a href="{{ route('app.shipment.index') }}">

								<i class="fas fa-cube rounded-circle"></i>
								<p>شحناتى</p>
</a>
						</div>
						<div class="col-md-4 col-12" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
								<i class="fas fa-power-off rounded-circle"></i>
								<p>تسجيل خروج</p>
						</div>
        <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection