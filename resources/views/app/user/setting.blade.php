@extends('layouts.app')
@section('title')
إعدادات الحساب
@endsection
@section('content')
<div class="account ">
    <div class="container ">
        <div class="row">
			@include('partials.app.account_sidebar')
			<div class="col-md-9  text-center"><div class="addadress">
            <h3 class="color2">إعدادات الحساب</h3>
            <hr class="hr">

            {!! Form::model($user, ['route' => ['app.user.update', $user]]) !!}
            {{ method_field('PATCH') }}
                <div class="form-group row">
                    <label for="inputPassword" class="col-5 col-lg-2 col-sm-3 col-form-label">البريد اﻹلكتروني</label>
                    <div class="col-7 col-lg-9 col-sm-8">
                    {!! Form::text('email', null, ['class' => 'form-control boxofinput']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword" class="col-4 col-lg-2 col-sm-3 col-form-label">الاسم</label>
                    <div class="col-7 col-lg-9 col-sm-8">
                    {!! Form::text('name', null, ['class' => 'form-control boxofinput']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword" class="col-4 col-lg-2 col-sm-3 col-form-label">رقم الجوال</label>
                    <div class="col-7 col-lg-9 col-sm-8">
                    {!! Form::text('phone', null, ['class' => 'form-control boxofinput']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword" class="col-4 col-lg-2 col-sm-3 col-form-label">المدينة</label>
                    <div class="col-7 col-lg-9 col-sm-8">
                    {!! Form::text('city', null, ['class' => 'form-control boxofinput']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword" class="col-4 col-lg-2 col-sm-3 col-form-label">العنوان</label>
                    <div class="col-7 col-lg-9 col-sm-8">
                    {!! Form::text('address', null, ['class' => 'form-control boxofinput']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword" class="col-4 col-lg-2 col-sm-3 col-form-label">كلمة المرور</label>
                    <div class="col-7 col-lg-9 col-sm-8">
                    <small>أترك الحقل فارغاًإذا كنت ﻻ تريد تغييره.</small>
                    {!! Form::password('password', ['class' => 'form-control boxofinput']) !!}
                    </div>
                </div>
                @if(auth()->user()->hasRole('trader'))
                <div class="form-group row">
                    <label for="inputPassword" class="col-4 col-lg-2 col-sm-3 col-form-label">إسم الشركة</label>
                    <div class="col-7 col-lg-9 col-sm-8">
                    {!! Form::text('company_name', null, ['class' => 'form-control boxofinput']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword" class="col-4 col-lg-2 col-sm-3 col-form-label">الموقع اﻹلكتروني</label>
                    <div class="col-7 col-lg-9 col-sm-8">
                    {!! Form::text('website', null, ['class' => 'form-control boxofinput']) !!}
                    </div>
                </div>
                @endif
                <div class="form-group row">
                        <div class=" col-4 col-lg-2 col-sm-3 col-form-label "></div>
                        <div class="col-7 col-lg-9 col-sm-8 row">
                            <div class="col-md-12">
                                <button type="submit" class="form-control btn btn-primary my-1 buttonbox">حفظ المعلومات</button>
                            </div>
                        </div>
                </div>

        </form>


            
        </div>

    </div>
		</div>
	</div>
</div>
@endsection