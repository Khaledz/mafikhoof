@extends('layouts.app')
@section('title')
فترة التحويل البنكي
@endsection
@section('content')
<div class="account ">
    <div class="container ">
        <div class="row">
			@include('partials.app.account_sidebar')
			<div class="col-md-9  text-center">
                <div class="transfer_bank_period">
                    <h3 class="color2">فترة التحويل البنكي</h3>
                    <hr class="hr">
                    {!! Form::open(['route' => 'app.user.duration']) !!}
                    {!! Form::select('transfer_duration_id', $durations, null, ['class' => 'form-control boxofinput']) !!}
                    <button type="submit" class="form-control btn btn-primary my-1 buttonbox maxwidth200px">حفظ</button>
                    </div>
                    </form>
                </div>
		</div>
	</div>
</div>
@endsection