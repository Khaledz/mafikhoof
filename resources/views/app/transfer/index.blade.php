@extends('layouts.app')
@section('title')
حركات الرصيد
@endsection
@section('content')
<div class="account ">
    <div class="container ">
        <div class="row">
			@include('partials.app.account_sidebar')
			<div class="col-md-9  text-center font-weight-bold">
                <div class="blance_history text-center">
                        <h3 class="color2">حركات الرصيد والمعاملات المالية</h3>
                        <hr class="hr">
                        <span class="row ">
                            <div class="col-sm-5 color3 background2">
                                <p>قيمة بضاعة لم تسلم</p>
                                <p class="numberfont">{{ $sumItemsNotDeliver }} ريال سعودي</p>
                            </div>
                            <div class="col-sm-5 color3 background2">
                                    <p>اخر حوالتين بنكية</p>
                                    <p class="numberfont">{{ $sumLastTwoTransfers }} ريال سعودي</p>
                            </div>
                            <div class="col-sm-11 color2 finalbox">
                                @if($transfersForYesterday->count() == 0)
                                    لا يوجد لديك حركات مالية ليوم أمس
                                @else
                                @foreach($transfersForYesterday as $transfer)
                                {{ $transfer->type->name }}
                                {{ $transfer->amount }} ريال سعودي
                                <hr>
                                @endforeach
                                @endif
                            </div>
                        </span>
                </div>
            </div>
		</div>
	</div>
</div>
@endsection