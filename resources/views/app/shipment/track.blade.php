@extends('layouts.app')
@section('title')
تتبع الشحنة
@endsection
@section('content')
<div class="account ">
    <div class="container ">
        <div class="row">
			@include('partials.app.account_sidebar')
			<div class="col-md-9  text-center"><div class="addadress">
            <h3 class="color2">تتبع الشحنة</h3>
            <hr class="hr">

            {!! Form::open(['route' => 'app.shipment.track']) !!}
                <div class="form-group row">
                    <label for="inputPassword" class="col-4 col-lg-2 col-sm-3 col-form-label">أدخل رقم التتبع</label>
                    <div class="col-7 col-lg-9 col-sm-8">
                    {!! Form::text('tracking_number', null, ['class' => 'form-control boxofinput']) !!}
                    </div>
                </div>
                <div class="form-group row">
                        <div class=" col-4 col-lg-2 col-sm-3 col-form-label "></div>
                        <div class="col-7 col-lg-9 col-sm-8 row">
                            <div class="col-md-6">
                                <button type="submit" class="form-control btn btn-primary my-1 buttonbox">إبحث</button>
                            </div>
                        </div>
                </div>

        </form>
        </div>

    </div>
		</div>
	</div>
</div>
@endsection