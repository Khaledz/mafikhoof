@extends('layouts.app')
@section('title')
شحناتي
@endsection
@section('content')
<div class="account ">
    <div class="container ">
        <div class="row">
            @include('partials.app.account_sidebar')

			<div class="col-md-9  text-center font-weight-bold area1" style="height: auto;">
                <div class="container text-center">
                    <div class="row row1">
                        <div class="type text-center  now background2 color3" id="all">الكل</div>
                        <div class="type text-center   background2 color3">انا المرسل</div>
                        <div class="type text-center  background2 color3">انا المستلم</div>
                        <div class=" text-center  background2 color3">انا الوسيط</div>
                        <div class=" text-center  background2 color3">بحث</div>
                    </div>
                    <p class="color2  firstp">انا المرسل</p>
                    <hr class="hr">
                    <ul class="nav nav-tabs  row2">
                            <li class="nav-item text-center  background2 color3 ">
                                <a class="nav-link active show" id="home-tab4" data-toggle="tab" href="#home4" role="tab" aria-controls="home" aria-selected="false">جديدة</a>
                            </li>
                            <li class="nav-item text-center  background2 color3">
                                <a class="nav-link" id="profile-tab4" data-toggle="tab" href="#profile4" role="tab" aria-controls="profile" aria-selected="false">لم تسلم</a>
                            </li>
                            <li class="nav-item text-center  background2 color3">
                                <a class="nav-link " id="contact-tab4" data-toggle="tab" href="#contact4" role="tab" aria-controls="contact" aria-selected="true">منتهية</a>
                            </li>
                            </ul>
                            <div class="text-center">
                                <p class="no-result" style="display:none;">لا يوجد شحنات في الوقت المحدد.</p>
                            <table class="table" style="display:none;">
  <thead class="background2 color3">
    <tr>
      <th scope="col">رقم التتبع</th>
      <th scope="col">تاريخ اﻹستلام</th>
      <th scope="col">الوقت</th>
      <th scope="col">الحالة</th>
    </tr>
  </thead>
  <tbody class="result">
    
  </tbody>
</table>

</div>

                </div>
            </div>
		</div>
	</div>
</div>
@endsection

@section('script')
<script>
    var addressType;
    var status;
    
$(function(){
    

    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({url: "/shipment/filter/all/new", success: function(result){
            if(result.length > 0)
            {
                $('.table').css('display', 'table');
                $('.no-result').css('display', 'none');

                for(let i = 0; i < result.length; i++)
                {
                    $('.result').html("<tr><th>"+result[i].tracking_number+"</th><td>"+result[i].date+"</td><td>"+result[i].provider_schedule.from+" - "+result[i].provider_schedule.to+"</td><td>"+result[i].status.name+"</td></tr>");
                }
            }
            else if(result.length == 0)
            {
                $('.table').css('display', 'none');
                $('.no-result').css('display', 'block');

                $('.result').html("");
            }
            
        }});

    $('.type').click(function(){
        var types = {
            all: "الكل",
            sender: "انا المرسل",
            receiver: "انا المستلم"
        };
        
        addressType = getKeyByValue(types, $(this).text());

        // also click on .nav-link to send request
        $('.nav-link').each( function(){
            var $this = $(this);
            // console.log($this.attr('class'));
            if ($this.hasClass('nav-link active show')) {
                $(this).click();
            }
        });
    });

    $('.nav-link').click(function(){
        var statuses = {
            new: "جديدة",
            not_delivered: "لم تسلم",
            expired: "منتهية"
        };
        status = getKeyByValue(statuses, $(this).text());
        if(addressType === undefined)
        {
            addressType = 'all';
        }
        $.ajax({url: "/shipment/filter/" + addressType + "/" + status, success: function(result){
            if(result.length > 0)
            {
                $('.table').css('display', 'table');
                $('.no-result').css('display', 'none');

                for(let i = 0; i < result.length; i++)
                {
                    $('.result').html("<tr><th>"+result[i].tracking_number+"</th><td>"+result[i].date+"</td><td>"+result[i].provider_schedule.from+" - "+result[i].provider_schedule.to+"</td><td>"+result[i].status.name+"</td></tr>");
                }
            }
            else if(result.length == 0)
            {
                $('.table').css('display', 'none');
                $('.no-result').css('display', 'block');

                $('.result').html("");
            }
            
        }});

    });

});

function getKeyByValue(object, value) {
  return Object.keys(object).find(key => object[key] === value);
}
</script>
@endsection