@extends('layouts.app')
@section('title')
تفعيل قسيمة
@endsection
@section('content')
<div class="account ">
    <div class="container ">
        <div class="row">
			@include('partials.app.account_sidebar')
			<div class="col-md-9  text-center"><div class="address_recived text-center">
            <h3 class="color2">تفعيل قسيمة</h3>
            <hr class="hr">

            {!! Form::open(['route' => 'app.coupon.store']) !!}
                <div class="form-group row">
                    <label for="inputPassword" class="col-4 col-lg-2 col-sm-3 col-form-label">أدخل القسيمة</label>
                    <div class="col-7 col-lg-9 col-sm-8">
                    {!! Form::text('code', null, ['class' => 'form-control boxofinput']) !!}
                    </div>
                </div>
                <div class="form-group row">
                        <div class=" col-4 col-lg-2 col-sm-3 col-form-label "></div>
                        <div class="col-7 col-lg-9 col-sm-8 row">
                            <div class="col-md-6">
                                <button type="submit" class="form-control btn btn-primary my-1 buttonbox">فعل القسيمة</button>
                            </div>
                        </div>
                </div>

        </form>
        <hr>
        <br>
        @if($coupons->count()  == 0)
        <h3 class="color2">ﻻ يوجد لديك قسائم مفعلة.</h3>
        @else
        <h3 class="color2">القسائم المفعلة</h3>
            <hr class="hr">
        @foreach($coupons as $coupon)
        <div class="col-12 color2 box ">
            <div class="float-left">
                <span>{{ $coupon->name }} - {{ $coupon->code }}({{ $coupon->discount_percentage }}%) ينتهي في <span>{{ $coupon->expire_on }}</span></span>
            </div>
            <div class=" float-right">
                <form method="POST" action="{{ route('app.coupon.destroy', $coupon) }}" style="display:none" id="removeForm">
                    <input type="submit" />
                    {{ method_field('DELETE') }}
                    {!! csrf_field() !!}
                </form>
                <a id="m_sweetalert_demo_9" onclick="javascript:document.getElementById('removeForm').submit();" href="#" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only">
                    <i class="flaticon-circle"></i> حذف
                </a>
            </div>
        </div>
        @endforeach
        @endif
            
        </div>

    </div>
		</div>
	</div>
</div>
@endsection