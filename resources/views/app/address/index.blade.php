@extends('layouts.app')
@section('title')
عناويني
@endsection
@section('content')
<div class="account ">
    <div class="container ">
        <div class="row">
			@include('partials.app.account_sidebar')
			<div class="col-md-9  text-center">
                <div class="address text-center">
                    <a href="{{ route('app.address.list', \App\Models\AddressType::first()) }}" class="form-control btn btn-primary my-1 buttonbox col-12">تعديل عنوانى الاساسى</a>    
                    <a href="{{ route('app.address.list', \App\Models\AddressType::find(2)) }}" class="form-control btn btn-primary my-1 buttonbox col-12">عرض عناوين المرسل</a>    
                    <a href="{{ route('app.address.list', \App\Models\AddressType::find(3)) }}" class="form-control btn btn-primary my-1 buttonbox col-12">عرض عناوين المستلم</a>    
                </div>
            </div>
		</div>
	</div>
</div>
@endsection