@extends('layouts.app')
@section('title')
@if(! is_null($addresses->first()))
    {{ $addresses->first()->type->name }}
@else
    عناويني
@endif
@endsection
@section('content')
<div class="account ">
    <div class="container ">
        <div class="row">
			@include('partials.app.account_sidebar')
			<div class="col-md-9  text-center font-weight-bold">
                <div class="address_recived text-center">
                    <h3 class="color2">
                    @if(! is_null($addresses->first()))
    {{ $addresses->first()->type->name }}
@else
    عناويني
@endif
</h3>

                    <hr class="hr">
                    <div class="row">
                    @if($addresses->count() == 0)
                        لا يوجد عناوين في الوقت الحالي.
                    @else
                    @foreach($addresses as $address)
                    <div class="col-12 color2 box ">
                        <div class="float-left">
                            <i class="fas fa-user-alt"></i>
                            <span>{{ $address->name }}</span>
                        </div>
                        <div class=" float-right">
                            <span>{{ $address->city }} - {{ $address->area }} - <span class="numberfont">{{ $address->phone}}</span> </span>
                        </div>
                    </div>
                    @endforeach
                    @endif
                    </div>
                    <br>
                    <a href="{{ route('app.address.create') }}" class="form-control btn btn-primary my-1 buttonbox col-6 ">إضافة عنوان</a>    
                </div></div>

            </div>
		</div>
	</div>
</div>
@endsection