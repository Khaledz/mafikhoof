@extends('layouts.app')
@section('title')
إضافة عنوان جديد
@endsection
@section('content')
<div class="account ">
    <div class="container ">
        <div class="row">
			@include('partials.app.account_sidebar')
			<div class="col-md-9  text-center"><div class="addadress">
            <h3 class="color2">البيانات الاساسية (يجب كتابتها)</h3>
            <hr class="hr">

            {!! Form::open(['route' => 'app.address.store']) !!}
                <div class="form-group row">
                    <label for="inputState" class="col-4 col-lg-2 col-sm-3  col-form-label ">نوع العنوان</label>
                    <div class="col-7 col-lg-9 col-sm-8">
                        {!! Form::select('address_type_id', $types, null, ['class' => 'form-control boxofinput']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword" class="col-4 col-lg-2 col-sm-3 col-form-label">الاسم</label>
                    <div class="col-7 col-lg-9 col-sm-8">
                    {!! Form::text('name', null, ['class' => 'form-control boxofinput']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputState" class="col-4 col-lg-2 col-sm-3  col-form-label ">الدولة</label>
                    <div class="col-7 col-lg-9 col-sm-8">
                        {!! Form::select('country_id', $countries, null, ['class' => 'form-control boxofinput']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputState" class="col-4 col-lg-2 col-sm-3  col-form-label">المدينة</label>
                    <div class="col-7 col-lg-9 col-sm-8">
                        {!! Form::text('city', null, ['class' => 'form-control boxofinput']) !!}
                    </div>
                </div>
                <div class="form-group row">
                        <label for="inputPassword" class="col-4 col-lg-2 col-sm-3  col-form-label">الحى</label>
                        <div class="col-7 col-lg-9 col-sm-8">
                        {!! Form::text('area', null, ['class' => 'form-control boxofinput']) !!}
                        </div>
                </div>
                <div class="form-group row">
                        <label for="inputPassword" class="col-4 col-lg-2 col-sm-3  col-form-label">الموبايل</label>
                        <div class="col-7 col-lg-9 col-sm-8">
                        {!! Form::text('phone', null, ['class' => 'form-control boxofinput']) !!}
                        </div>
                </div>
            <h3 class="color2">البيانات الإضافية (إختيارية)</h3>
            <hr class="hr">
                <div class="form-group row">
                        <label for="inputPassword" class="col-4 col-lg-2 col-sm-3  col-form-label">الموبايل <span class="numberfont">2</span></label>
                        <div class="col-7 col-lg-9 col-sm-8">
                        {!! Form::text('phone2', null, ['class' => 'form-control boxofinput']) !!}
                        </div>
                </div>
                <div class="form-group row">
                        <label for="inputPassword" class="col-4 col-lg-2 col-sm-3  col-form-label">رقم المبنى</label>
                        <div class="col-7 col-lg-9 col-sm-8">
                        {!! Form::text('building_number', null, ['class' => 'form-control boxofinput']) !!}
                        </div>
                </div>
                <div class="form-group row">
                        <label for="inputPassword" class="col-4 col-lg-2 col-sm-3  col-form-label">الشارع</label>
                        <div class="col-7 col-lg-9 col-sm-8">
                        {!! Form::text('street', null, ['class' => 'form-control boxofinput']) !!}
                        </div>
                </div>
                <div class="form-group row ">
                        <label for="inputPassword" class="col-4 col-lg-2 col-sm-3  col-form-label">الرمز البريدى</label>
                        <div class="col-7 col-lg-9 col-sm-8">
                            <input type="text" class="form-control boxofinput problem1">
                        </div>
                </div>
                <div class="form-group row">
                        <label for="inputPassword" class="col-4 col-lg-2 col-sm-3  col-form-label">المحافظة</label>
                        <div class="col-7 col-lg-9 col-sm-8">
                        {!! Form::text('postcode', null, ['class' => 'form-control boxofinput']) !!}
                        </div>
                </div>
                <div class="form-group row">
                        <div class=" col-4 col-lg-2 col-sm-3 col-form-label "></div>
                        <div class="col-7 col-lg-9 col-sm-8 row">
                            <div class="col-md-6">
                                <button type="submit" class="form-control btn btn-primary my-1 buttonbox">حفظ العنوان</button>
                            </div>
                            <div class="col-md-5 ">
                                <button type="submit" class="form-control btn btn-primary my-1 buttonbox">الغاء</button>
                            </div>
                        </div>
                </div>

        </form>


            
        </div>

    </div>
		</div>
	</div>
</div>
@endsection