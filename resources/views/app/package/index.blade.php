@extends('layouts.app')
@section('title')
اختيار باقة
@endsection
@section('content')
<div class="vendor_confirm_register">
    <div class="container text-center">
            <div class="message">
                    شكرا لانضمامك لنا ونتمنى ان تحوز خدماتنا على رضاكم
            </div>
            <h3 class="color2">يرجي اختيار الباقة المناسبة</h3>
            <hr class="hr">
            {!! Form::open(['route' => 'app.package.store'], ['class' => 'form-form-check  text-center']) !!}
                @if($packages->count() > 0)
                    @foreach($packages as $package)
                    <div class="form-check ">
                        <span class="float-left"><span class="numberfont">{{ $package->policy_number }}</span> {{ $package->name }}</span>
                        <input type="radio" name="package_id" value="{{ $package->id }}"  class="col-1  float-right">
                    </div>
                    @endforeach
                @endif
                <button type="submit" class="form-control btn btn-primary my-1 buttonbox maxwidth200px">استمرار</button>
            </form>
    </div>
</div>
@endsection