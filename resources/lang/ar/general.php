<?php

return [
    'create' => 'إضافة جديد',
    'edit' => 'تعدل',
    'index' => 'لوحة التحكم',
    'user'=> 'المستخدمين',
    'address' => 'العناوين',
    'shipment' => 'الشحن',
    'bank' => 'البنوك',
    'coupon' => 'الكوبونات',
    'order' => 'الطلبات',
    'package' => 'الباقات',
    'report' => 'التقارير',
    'shipment-provider' => 'مزودي خدمات الشحن',
    'shipment-status' => 'حالات الشحن',
    'shipment-schedule' => 'أوقات وأسعار الشحن',
    'bank-type' => 'مزود الخدمة البنكية',
    'transfer' => 'الحوالات',
    'avatar' => 'صورة الحساب',
    'app-setting' => 'إعدادات الموقع',
    'slider' => 'السلايدات'
];